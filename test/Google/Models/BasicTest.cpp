#include "BasicTest.hpp"

namespace MyTime { namespace Test { namespace Models {

using namespace std;
using namespace MyTime;
using namespace MyTime::Models;

BasicTest::BasicTest()
{
    sqlExecutor()->setConnectionString(":memory:");
    tagDbBackend_ = make_shared<Database::Backend::Editor::Tag>(sqlExecutor());
    tags_ = make_shared<QTagModel>(tagDbBackend_);
}

std::shared_ptr<Database::Backend::Editor::Tag> BasicTest::tagDbBackend() const
{
    return tagDbBackend_;
}

std::shared_ptr<Models::QTagModel> BasicTest::tags() const
{
    return tags_;
}

}}}
