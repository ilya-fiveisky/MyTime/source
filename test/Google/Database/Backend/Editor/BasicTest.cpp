#include "gtest/gtest.h"

#include <algorithm>
#include <exception>
#include <set>
#include <vector>

#include <QSqlRecord>

#include "Database/db_exception.hpp"
#include "Database/SqlExecutor.hpp"
#include "Database/Backend/Editor/Basic.hpp"

#include "../../BasicTest.hpp"

using namespace std;

using namespace MyTime::Database;
using namespace MyTime::Database::Backend::Editor;

typedef MyTime::Database::Backend::Editor::Basic BasicEditorBackend;

class PreActivityBackend : public BasicEditorBackend {
protected:
    using BasicEditorBackend::Basic;

    std::string mainName() const override
    {
        return "activity";
    }
    std::string tagName() const override
    {
        return "tag";
    }
};

class PreTagBackend : public BasicEditorBackend {
protected:
    using BasicEditorBackend::Basic;

    std::string mainName() const override
    {
        return "tag";
    }
    std::string tagName() const override
    {
        return "super_tag";
    }
};

namespace MyTime { namespace Test { namespace Database { namespace Backend { namespace Editor {

typedef BasicTest DatabaseBackendEditorBasicTest;

TEST_F(DatabaseBackendEditorBasicTest, constructor) {
    EXPECT_THROW(PreActivityBackend b(nullptr), invalid_argument);
    EXPECT_NO_THROW(PreActivityBackend b(sqlExecutor()));
}

TEST_F(DatabaseBackendEditorBasicTest, create) {
    PreActivityBackend activityBackend(sqlExecutor());
    EXPECT_THROW(activityBackend.create(), db_exception);
    sqlExecutor()->setConnectionString(":memory:");
    int id = -1;
    auto result = activityBackend.select("SELECT * FROM activity");
    vector<QSqlRecord> records{begin(result), end(result)};
    ASSERT_TRUE(records.empty());
    ASSERT_NO_THROW(id = activityBackend.create());
    EXPECT_EQ(id, 1);
    ASSERT_NO_THROW(id = activityBackend.create());
    EXPECT_EQ(id, 2);
    result = activityBackend.select("SELECT * FROM activity");
    records.assign(begin(result), end(result));
    ASSERT_EQ(records.size(), 2);
}

TEST_F(DatabaseBackendEditorBasicTest, remove) {
    PreActivityBackend activityBackend(sqlExecutor());
    EXPECT_THROW(activityBackend.remove(0), db_exception);
    sqlExecutor()->setConnectionString(":memory:");
    activityBackend.create();
    auto result = activityBackend.select("SELECT * FROM activity");
    vector<QSqlRecord> records{begin(result), end(result)};
    ASSERT_EQ(records.size(), 1);
    ASSERT_NO_THROW(activityBackend.remove(1));
    result = activityBackend.select("SELECT * FROM activity");
    records.assign(begin(result), end(result));
    ASSERT_TRUE(records.empty());
}

TEST_F(DatabaseBackendEditorBasicTest, updateTags) {
    PreActivityBackend activityBackend(sqlExecutor());
    PreTagBackend tagBackend(sqlExecutor());
    EXPECT_THROW(activityBackend.updateTags(0, {}), db_exception);
    sqlExecutor()->setConnectionString(":memory:");
    int activityId = activityBackend.create();
    auto result = activityBackend.select("SELECT * FROM activity");
    vector<QSqlRecord> records{begin(result), end(result)};
    ASSERT_EQ(records.size(), 1);
    int tagId = tagBackend.create();
    result = activityBackend.select("SELECT * FROM tag");
    records.assign(begin(result), end(result));
    ASSERT_EQ(records.size(), 1);
    result = activityBackend.select("SELECT * FROM activity_tag");
    records.assign(begin(result), end(result));
    ASSERT_EQ(records.size(), 0);
    ASSERT_NO_THROW(activityBackend.updateTags(activityId, {tagId}));
    result = activityBackend.select(
                QString::fromStdString(
                    "SELECT * FROM activity_tag WHERE activity_id = " + to_string(activityId)));
    records.assign(begin(result), end(result));
    ASSERT_EQ(records.size(), 1);
}

}}}}}
