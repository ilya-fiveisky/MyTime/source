#include "QAnalyticsContext.hpp"

#include <string>

#include <boost/date_time.hpp>

#include <QDebug>

#include "check_ptr_arg.hpp"
#include "Models/QDurationChartModel.hpp"
#include "Models/QTagIdFilterProxyModel.hpp"
#include "Models/QTagModel.hpp"
#include "Models/QTagNameFilterProxyModel.hpp"
#include "Database/Backend/Basic.hpp"

using namespace std;
using namespace boost::posix_time;
using namespace MyTime::Models;

namespace MyTime {

const QString QAnalyticsContext::AGREGATE_QUERY =
        "SELECT MIN(start) AS start, MAX(end) AS end, SUM(duration) AS duration "
        "FROM "
        "("
        "       SELECT "
        "           description, "
        "           MIN(start) AS start, "
        "           MAX(end) AS end, "
        "           SUM(end-start) AS duration "
        "       FROM "
        "       ("
        +           Database::Backend::Basic::FILTERED_JOIN_PLACEHOLDER() +
        "       ) "
        "       GROUP BY activity_id"
        ")";

QAnalyticsContext::QAnalyticsContext(
        const shared_ptr<QTagModel> &allTagsModel,
        const shared_ptr<Database::Backend::Basic> &dbBackend,
        QObject *parent):
    QObject(parent), dbBackend_{dbBackend}
{
    check_ptr_arg(allTagsModel, "all tags model");
    check_ptr_arg(dbBackend, "db backend");

    m_newTagSelectorModel = new QTagNameFilterProxyModel(1, this);
    dynamic_cast<QSortFilterProxyModel*>(m_newTagSelectorModel)->setSourceModel(allTagsModel.get());

    m_tagSelectorModel = new QTagIdFilterProxyModel(0, this);
    dynamic_cast<QSortFilterProxyModel*>(m_tagSelectorModel)->setSourceModel(allTagsModel.get());

    m_chartModel = new QDurationChartModel(dbBackend, this);

    dbBackend_->dbChanged.connect([this](){ initialize(); });

    initialize();
}

void QAnalyticsContext::update(const QString &keywords, const QList<int> &tags, const QDateTime &from, const QDateTime &to)
{
    QString agregateQueryStr = QString(AGREGATE_QUERY).
            replace(Database::Backend::Basic::FILTERED_JOIN_PLACEHOLDER(),
                    dbBackend_->constructFilteredJoinString(keywords, tags, from, to));
    auto it = dbBackend_->select(agregateQueryStr);
    auto agregateRecord = *begin(it);

    double duration = agregateRecord.value("duration").toDouble();
    double start = agregateRecord.value("start").toDouble();
    double end = agregateRecord.value("end").toDouble();

    update_average(averageDurationStr(duration, start, end, 24*3600));
    update_averageFilter(averageDurationStr(duration, from.toTime_t(), to.toTime_t(), 24*3600));
    update_durationSum(durationStr(duration));
    update_end(QDateTime::fromTime_t(end).toLocalTime());
    update_filterInterval(durationStr(to.toTime_t() - from.toTime_t()));
    update_start(QDateTime::fromTime_t(start).toLocalTime());
    update_startEndInterval(durationStr(end-start));

    dynamic_cast<QDurationChartModel*>(m_chartModel)->refresh(keywords, tags, from, to);
}

QString QAnalyticsContext::averageDurationStr(const double &duration, const double &start, const double &end, const double &mult) const
{
    return QString::fromStdString(to_simple_string(seconds(mult*duration/(end-start))));
}

QString QAnalyticsContext::durationStr(const double &duration) const
{
    return averageDurationStr(duration, 0, 1, 1);
}

void QAnalyticsContext::initialize()
{
    update("", {},
           QDateTime::currentDateTime().addMonths(-1),
           QDateTime::currentDateTime().addDays(1));
}

}
