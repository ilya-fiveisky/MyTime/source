#ifndef GUARD_5EFBFFF71ABC456A81B300CD9AEF8CA1
#define GUARD_5EFBFFF71ABC456A81B300CD9AEF8CA1

#include <vector>

#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QSqlTableModel>

namespace MyTime { namespace Models {

template<class BaseT> class QSqlRolesModel : public BaseT
{
public:
    QSqlRolesModel();
    explicit QSqlRolesModel(QSqlDatabase db);
    
    int columnCount(const QModelIndex &) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

    std::string name() const;
    void setName(const std::string &name);
    void setUserRoleNames(const std::vector<QString> &roleNames);

protected:
    std::vector<QString> userRoleNames() const;

private:
    std::vector<QString> userRoleNames_;
    std::string name_;
};

template<class BaseT> QSqlRolesModel<BaseT>::QSqlRolesModel() :
    BaseT(nullptr){}

template<class BaseT> QSqlRolesModel<BaseT>::QSqlRolesModel(QSqlDatabase db) :
    BaseT(nullptr, db){}

template<class BaseT> int QSqlRolesModel<BaseT>::columnCount(const QModelIndex &) const
{
    return userRoleNames().size();
}

template<class BaseT> QHash<int, QByteArray> QSqlRolesModel<BaseT>::roleNames() const {
    QHash<int, QByteArray> roleNames = BaseT::roleNames();
    for(size_t i = 0; i < userRoleNames_.size(); i++){
        roleNames[Qt::UserRole + i + 1] = userRoleNames_[i].toUtf8();
    }
    return roleNames;
}

template<class BaseT> QVariant QSqlRolesModel<BaseT>::data(const QModelIndex &index, int role) const {
    QVariant value = BaseT::data(index, role);
    if(role >= Qt::UserRole){
        int columnIdx = role - Qt::UserRole - 1;
        QModelIndex modelIndex = this->index(index.row(), columnIdx);
        value = BaseT::data(modelIndex, Qt::DisplayRole);
    }
    return value;
}

template<class BaseT> void QSqlRolesModel<BaseT>::setUserRoleNames(const std::vector<QString> &roleNames)
{
    userRoleNames_ = roleNames;
}

template<class BaseT> std::vector<QString> QSqlRolesModel<BaseT>::userRoleNames() const
{
    return userRoleNames_;
}

template<class BaseT> std::string QSqlRolesModel<BaseT>::name() const
{
    return name_;
}

template<class BaseT> void QSqlRolesModel<BaseT>::setName(const std::string &name)
{
    name_ = name;
}

}}

#endif // GUARD_5EFBFFF71ABC456A81B300CD9AEF8CA1
