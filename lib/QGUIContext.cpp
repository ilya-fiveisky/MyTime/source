#include "QGUIContext.hpp"

#include "check_ptr_arg.hpp"

#include "Database/Store.hpp"
#include "QGUIContextImpl.hpp"

using namespace std;
using namespace MyTime::Models;

namespace MyTime {

QGUIContext::QGUIContext(const std::shared_ptr<Database::Store> &store,
                         const std::shared_ptr<QActivityModel> &activityViewModel,
                         const std::shared_ptr<QAnalyticsContext> &analyticsConext,
                         const std::shared_ptr<QTagModel> &tagViewModel,
                         QObject *parent) :
    QObject(parent),
    m_activityViewModel{activityViewModel}, m_analytics{analyticsConext},
    m_tagViewModel{tagViewModel}, m_tagSelectorModel{nullptr},
    impl_{new QGUIContextImpl{store}}
{
    check_ptr_arg(store, "store");
    check_ptr_arg(activityViewModel, "activityViewModel");
    check_ptr_arg(analyticsConext, "analyticsConext");
    check_ptr_arg(tagViewModel, "tagViewModel");
    m_tagSelectorModel = make_shared<QTagNameFilterProxyModel>(QTagModel::RoleIndex::name);
    m_tagSelectorModel->setSourceModel(m_tagViewModel.get());

    update_dbPath(impl_->store_->path());
}

QGUIContext::~QGUIContext() = default;

QString QGUIContext::newDb(const QUrl &fileUrl) noexcept
{
    return impl_->doActionWithStringResult([this, fileUrl](){ impl_->store_->newDb(fileUrl); }, this);
}

QString QGUIContext::openDb(const QUrl &fileUrl) noexcept
{
    return impl_->doActionWithStringResult([this, fileUrl](){ impl_->store_->open(fileUrl); }, this);
}

QString QGUIContext::saveDbAs(const QUrl &fileUrl) noexcept
{
    return impl_->doActionWithStringResult([this, fileUrl](){ impl_->store_->saveAs(fileUrl); }, this);
}

}
