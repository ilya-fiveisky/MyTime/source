#ifndef GUARD_739EDB49E45F4B0DBEFA938FEEE19626
#define GUARD_739EDB49E45F4B0DBEFA938FEEE19626

#include <memory>

#include <QString>
#include <QTimer>
#include <QUrl>

class QSettings;

namespace MyTime { namespace Database {

class SqlExecutor;

struct StoreImpl
{
    explicit StoreImpl(const std::shared_ptr<SqlExecutor> &sqlExecutor);
    StoreImpl(const std::shared_ptr<SqlExecutor> &sqlExecutor,
              const std::shared_ptr<QSettings> &settings,
              const std::string &tempDirPath);
    StoreImpl(const std::shared_ptr<SqlExecutor> &sqlExecutor,
              const std::shared_ptr<QSettings> &settings,
              const std::string &tempDirPath,
              const int &backupTimerInterval);
    virtual ~StoreImpl();

    void backup();
    void startBackup();

    std::unique_ptr<QTimer>         backupTimer_;
    std::shared_ptr<QSettings>      settings_;
    std::shared_ptr<SqlExecutor>    sqlExecutor_;

    static  const std::string    BACKUP_FILE_EXTENSION;
            const int            BACKUP_TIMER_INTERVAL;
    static  const int            BACKUP_TIMER_INTERVAL_DEFAULT;
    static  const QString        LAST_DB_PATH_SETTING_NAME;
    static  const std::string    NEW_DB_NAME_DEFAULT;
            const QUrl           NEW_DB_PATH_DEFAULT;
};

}}

#endif // GUARD_739EDB49E45F4B0DBEFA938FEEE19626
