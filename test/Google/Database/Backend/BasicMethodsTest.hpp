#ifndef GUARD_545724D0852742C78620C905B356BFD2
#define GUARD_545724D0852742C78620C905B356BFD2

#include <memory>

#include "../BasicTest.hpp"

namespace MyTime { namespace Test { namespace Database { namespace Backend {

template<class BackendT> class BasicMethodsTest : public BasicTest {
protected:
    BasicMethodsTest():
        backend_{std::make_shared<BackendT>(sqlExecutor())}
    {
    }

    std::shared_ptr<BackendT> backend() const
    {
        return backend_;
    }

private:
    const std::shared_ptr<BackendT> backend_;
};

}}}}

#endif // GUARD_545724D0852742C78620C905B356BFD2
