## Description
Time tracker with editable tags and basic analytics.

## Installers
* [Windows 64 bit online](https://ilya-fiveisky.gitlab.io/my-time-site/deployment/windows/MyTimeOnlineInstaller.exe) (recommended)
* [Windows 64 bit offline](https://ilya-fiveisky.gitlab.io/my-time-site/deployment/windows/MyTimeOfflineInstaller.exe)

## License
GPLv3 license.

## Build instructions
__MSVC__: BOOST_ROOT environment variable should be defined and point to Boost directory root.


__Linux__: BOOST_ROOT can be used to point to your Boost build istead of pre-installed.


__Android__: ANDROID_NDK_ROOT environment variable should be defined and point to [CrystaX NDK](https://www.crystax.net/en/android/ndk) directory root.