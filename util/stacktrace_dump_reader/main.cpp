#include <iostream>

#include <boost/filesystem.hpp>
#include <boost/stacktrace.hpp>

using namespace std;

using namespace boost::filesystem;
using namespace boost::stacktrace;

int main(int argc, char *argv[])
{
    if(argc < 2){
        std::cout << "No dump file in program args." << std::endl;
    }
    path inPath{argv[1]};
    boost::filesystem::ifstream ifs(inPath);
    path outPath{inPath};
    outPath.replace_extension("txt");
    boost::filesystem::ofstream ofs(outPath);

    boost::stacktrace::stacktrace st = boost::stacktrace::stacktrace::from_dump(ifs);
    cout << st << endl;
    ofs << st;

    return 0;
}
