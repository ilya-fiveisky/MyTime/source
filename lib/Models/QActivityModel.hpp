#ifndef GUARD_70AB16A4DE504271A0E353B56BC12234
#define GUARD_70AB16A4DE504271A0E353B56BC12234

#include <memory>

#include <QAbstractTableModel>
#include <QString>

#include "mylib_global.h"

#include "QModelBase.hpp"

namespace MyTime {

namespace Database { namespace Backend { namespace Editor {
    class Activity;
}}}

namespace Models {

struct QActivityModelImpl;

class MYLIB_EXPORT QActivityModel: public QModelBase
{
    Q_OBJECT

    typedef QModelBase BaseT;

public:
    explicit QActivityModel(
            const std::shared_ptr<Models::QModelBase> &tagModel,
            const std::shared_ptr<Database::Backend::Editor::Activity> &dbBackend,
            QObject *parent = Q_NULLPTR);

    virtual ~QActivityModel();

    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

public slots:
    QString start(const int &row);
    QString stop(const int &row);

protected:
    std::shared_ptr<QSqlRecord>
                createRecord() const override;

    std::shared_ptr<QSqlRecord>
                createRecord(const QSqlRecord &record) override;

    std::shared_ptr<Database::Backend::Editor::Basic>
                dbBackend() const override;

    QModelBase *tagModel() override;

    void        timerEvent(QTimerEvent *event) override;

private:
    std::unique_ptr<QActivityModelImpl> impl_;
};

}}
#endif // GUARD_70AB16A4DE504271A0E353B56BC12234
