#ifndef GUARD_63A918D9C2B249D09A8730107539EAEA
#define GUARD_63A918D9C2B249D09A8730107539EAEA

#include "gtest/gtest.h"

#include <memory>

#include <boost/filesystem.hpp>

#include "StoreBasicTest.hpp"

namespace MyTime {

namespace Database {
    class Store;
}

namespace Test { namespace Database {

class StoreMethodsTest : public StoreBasicTest {
protected:
    using StoreBasicTest::StoreBasicTest;

    virtual void SetUp() override;

    std::shared_ptr<MyTime::Database::Store> store() const;

    void checkSettings();

    void checkSettings(const boost::filesystem::path &dbPath);

private:
    std::shared_ptr<MyTime::Database::Store> store_;
};

}}}
#endif // GUARD_63A918D9C2B249D09A8730107539EAEA
