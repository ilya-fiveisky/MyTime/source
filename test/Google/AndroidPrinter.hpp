#ifndef GUARD_99C1306B7B0B46728851D21E731D854F
#define GUARD_99C1306B7B0B46728851D21E731D854F

#include "gtest/gtest.h"

namespace MyTime { namespace Test {

class AndroidPrinter: public ::testing::EmptyTestEventListener
{
public:
    AndroidPrinter();

    void OnEnvironmentsTearDownEnd(const ::testing::UnitTest&) override;
    void OnTestEnd(const ::testing::TestInfo& testInfo) override;
    void OnTestProgramEnd(const ::testing::UnitTest& unitTest) override;
    void OnTestStart(const ::testing::TestInfo& testInfo) override;
};

}}
#endif // GUARD_99C1306B7B0B46728851D21E731D854F
