import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtTest 1.0

Page {
    id: root
    height: 480
    width: 640

    ListModel {
        id: tags

        property int makeNewCount: 0
        property int deleteRowIndex: -1

        function idByRow(row){
            return get(row).id;
        }

        function makeNew(){
            makeNewCount ++;
        }

        function deleteRow(index){
            deleteRowIndex = index;
        }

        ListElement {
            property int id: 1
            name: "111"
        }
        ListElement {
            property int id: 2
            name: "222"
        }
        ListElement {
            property int id: 3
            name: "333"
        }
    }

    ListModel {
        id: selectedTags

        property var ids: [2, 3]

        ListElement {
            property int id: 2
            name: "222"
        }
        ListElement {
            property int id: 3
            name: "333"
        }
    }

    QtObject {
        id: analyticsContext
        property QtObject newTagSelectorModel: tags
        property QtObject tagSelectorModel: selectedTags
        property string filterInterval
        property string startEndInterval
        property string durationSum
        property string average
        property string averageFilter
        property date start: new Date
        property date end: new Date

        property QtObject updatePars: QtObject {
            property int count: 0
            property var ids
            property string searchKeywords
            property date from
            property date to
        }

        function update(searchKeywords, ids, from, to){
            updatePars.count ++;
            updatePars.ids = ids;
            updatePars.searchKeywords = searchKeywords;
            updatePars.from = from;
            updatePars.to = to;
        }
    }

    Analytics4Test {
        id: analyticsView
        context: analyticsContext
        anchors.fill: parent

        TestCase {
            name: "Analytics Tests"
            when: windowShown

            SignalSpy {
                id: spy
                signalName: "clicked"
            }

            function test_000_props() {
                compare(analyticsView.context, analyticsContext, "analyticsView.context != analytics");
            }

            function test_001_submitButton() {
                var searchKeywords = findChild(parent, "searchKeywords");
                var keywords = "A1";
                searchKeywords.text = keywords;

                var from = findChild(parent, "from");
                var fromDate = new Date(1, 1, 1);
                from.selectedDate = fromDate;

                var to = findChild(parent, "to");
                var toDate = new Date(1, 1, 2);
                to.selectedDate = toDate;

                var submitButton = findChild(parent, "submitButton");
                verify(submitButton, "submitButton is null");
                verify(submitButton.visible, "submitButton is not visible");
                spy.clear();
                spy.target = submitButton;
                mouseClick(submitButton);
                compare(spy.count, 1, "spy.count != 1");
                compare(analyticsContext.updatePars.count, 1, "update count != 1");
                compare(analyticsContext.updatePars.ids, selectedTags.ids, "update ids != selectedTags.ids");
                compare(analyticsContext.updatePars.searchKeywords, keywords, "update searchKeywords != keywords");
                compare(analyticsContext.updatePars.from, fromDate, "update from != fromDate");
                compare(analyticsContext.updatePars.to, toDate, "update to != toDate");
                spy.clear();
            }
        }
    }
}
