#ifndef GUARD_A31F171C77D240D5BC1128FDEE0F1954
#define GUARD_A31F171C77D240D5BC1128FDEE0F1954

#include <memory>

#if defined Q_OS_WIN
#include <vector>
#endif

#include <boost/coroutine2/all.hpp>
#include <boost/signals2.hpp>

#include <QDateTime>
#include <QList>
#include <QSqlRecord>

class QSqlQuery;
class QString;

namespace MyTime { namespace Database {

class SqlExecutor;

namespace Backend {

class BasicImpl;

class Basic
{
    //#if defined Q_OS_WIN
    typedef std::vector<QSqlRecord> select_return_type;
    //#else
    //    typedef boost::coroutines2::coroutine<QSqlRecord> coro_t;
    //    typedef coro_t::pull_type select_return_type;
    //#endif

public:
    explicit Basic(const std::shared_ptr<MyTime::Database::SqlExecutor> &sqlExecutor);
    virtual ~Basic();

    select_return_type  select(const QString &queryStr);
    QString             constructFilteredJoinString(
            const QString &keywords, const QList<int> &tags,
            const QDateTime &from, const QDateTime &to);

    static QString FILTERED_JOIN_PLACEHOLDER();

    boost::signals2::signal<void ()> dbChanged;

protected:
    void checkConnection();
    std::shared_ptr<MyTime::Database::SqlExecutor> sqlExecutor() const;

private:
    std::unique_ptr<MyTime::Database::Backend::BasicImpl> impl_;
};

}}}

#endif // GUARD_A31F171C77D240D5BC1128FDEE0F1954
