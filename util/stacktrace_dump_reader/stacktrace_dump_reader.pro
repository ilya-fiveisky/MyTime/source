TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

LIBS += -ldl

!include ($$top_srcdir/lib/libs.pri){
    message($${TARGET}": libs.pri is not included")
}
