#include "gtest/gtest.h"

#include <boost/filesystem.hpp>

#include <QSettings>

#include "Database/db_exception.hpp"
#include "Database/SqlExecutor.hpp"
#include "Database/StoreImpl.hpp"

#include "StoreBasicTest.hpp"

using namespace std;
namespace fs = boost::filesystem;

using namespace MyTime;
using namespace MyTime::Database;

namespace MyTime { namespace Test {namespace Database {

typedef StoreBasicTest DatabaseStoreImplTest;

TEST_F(DatabaseStoreImplTest, constructorExceptions) {
    EXPECT_THROW(StoreImpl s(nullptr),                                              invalid_argument);
    EXPECT_THROW(StoreImpl s(nullptr,       SETTINGS,   TEMP_TEST_DIR.string()),    invalid_argument);
    EXPECT_THROW(StoreImpl s(sqlExecutor(), nullptr,    TEMP_TEST_DIR.string()),    invalid_argument);
    EXPECT_THROW(StoreImpl s(sqlExecutor(), SETTINGS,   fs::unique_path().string()),invalid_argument);
}

TEST_F(DatabaseStoreImplTest, constructor) {
    EXPECT_NO_THROW(StoreImpl s(sqlExecutor()));
    EXPECT_NO_THROW(StoreImpl s(sqlExecutor(), SETTINGS, TEMP_TEST_DIR.string()));
}


TEST_F(DatabaseStoreImplTest, backup) {
    sqlExecutor()->setConnectionString(dbPath().string());
    StoreImpl s(sqlExecutor());
    ASSERT_NO_THROW(s.backup(););
    checkBackup();
}

}}}
