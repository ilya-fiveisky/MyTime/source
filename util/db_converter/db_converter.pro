QT += core sql
QT -= gui

CONFIG += c++11

TARGET = db_converter
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp

MY_SOURCE = ../../lib

INCLUDEPATH += $${MY_SOURCE}
LIBS += -lmylib -L$${MY_SOURCE}

!include ($${MY_SOURCE}/libs.pri){
    message("libs.pri is not included")
}

