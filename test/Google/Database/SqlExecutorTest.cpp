#include "gtest/gtest.h"

#include <exception>
#include <string>
#include <vector>

#include <boost/filesystem.hpp>

#include <QSqlQuery>
#include <QSqlRecord>

#include "Database/db_exception.hpp"
#include "Database/SqlExecutor.hpp"

#include "BasicTest.hpp"

using namespace std;
using namespace boost::filesystem;

using namespace MyTime;
using namespace MyTime::Database;

namespace MyTime { namespace Test {namespace Database {

typedef MyTime::Test::Database::BasicTest SqlExecutorTest;

typedef SqlExecutorTest DatabaseSqlExecutorConstructorTest;

TEST_F(DatabaseSqlExecutorConstructorTest, exceptions) {
    auto notExistingPath = path(TEMP_TEST_DIR) /= unique_path() /= unique_path();
    EXPECT_THROW(SqlExecutor s(notExistingPath.string()), db_exception);
}

TEST_F(DatabaseSqlExecutorConstructorTest, normalTest) {
    EXPECT_NO_THROW(SqlExecutor s);
    EXPECT_NO_THROW(SqlExecutor s(":memory:"));
    EXPECT_NO_THROW(SqlExecutor s(dbPath().string()));
    checkDb();
}

typedef SqlExecutorTest DatabaseSqlExecutorMethodsTest;

TEST_F(DatabaseSqlExecutorMethodsTest, setConnectionStringExceptions) {
    auto notExistingPath = path(TEMP_TEST_DIR) /= unique_path() /= unique_path();
    EXPECT_THROW(sqlExecutor()->setConnectionString(notExistingPath.string()), db_exception);
}

TEST_F(DatabaseSqlExecutorMethodsTest, setConnectionString) {
    int dbChangedCounter = 0;
    sqlExecutor()->dbChanged.connect([&dbChangedCounter](){++dbChangedCounter;});
    EXPECT_NO_THROW(sqlExecutor()->setConnectionString(":memory:"));
    EXPECT_EQ(sqlExecutor()->dbPath(), ":memory:");
    EXPECT_NO_THROW(sqlExecutor()->setConnectionString(dbPath().string()));
    EXPECT_EQ(sqlExecutor()->dbPath(), dbPath().string());
    checkDb();
    EXPECT_EQ(dbChangedCounter, 2);
}

TEST_F(DatabaseSqlExecutorMethodsTest, executeExceptions) {
    EXPECT_THROW(sqlExecutor()->execute(""), logic_error);
    sqlExecutor()->setConnectionString(":memory:");
    EXPECT_THROW(sqlExecutor()->execute("   "), invalid_argument);
    EXPECT_THROW(sqlExecutor()->execute("x"), db_exception);
}

TEST_F(DatabaseSqlExecutorMethodsTest, execute) {
    sqlExecutor()->setConnectionString(":memory:");
    ASSERT_NO_THROW(sqlExecutor()->execute(
                        "CREATE TABLE activity ("
                        "id INTEGER PRIMARY KEY ASC AUTOINCREMENT,"
                        "description TEXT)"));
    QString description{"Some description"};
    ASSERT_NO_THROW(sqlExecutor()->execute("INSERT INTO activity (description) VALUES (?)", {description}));
    QSqlQuery q;
    ASSERT_NO_THROW(q = sqlExecutor()->execute("SELECT * FROM activity"););
    ASSERT_NO_THROW(q.first(););
    EXPECT_EQ(q.record().value("description").toString(), description);
}

TEST_F(DatabaseSqlExecutorMethodsTest, executeInTransactionExceptions) {
    EXPECT_THROW(sqlExecutor()->executeInTransaction({""}), logic_error);
    sqlExecutor()->setConnectionString(":memory:");
    EXPECT_THROW(sqlExecutor()->executeInTransaction({"   "}), invalid_argument);
    EXPECT_THROW(sqlExecutor()->executeInTransaction({"x"}), db_exception);
}

TEST_F(DatabaseSqlExecutorMethodsTest, executeInTransaction) {
    sqlExecutor()->setConnectionString(":memory:");
    string description{"Some description"};
    vector<string> commands
    {
        "CREATE TABLE activity ("
        "id INTEGER PRIMARY KEY ASC AUTOINCREMENT,"
        "description TEXT)",
        "INSERT INTO activity (description) VALUES ('" + description + "')"
    };
    ASSERT_NO_THROW(sqlExecutor()->executeInTransaction(commands));
    QSqlQuery q;
    ASSERT_NO_THROW(q = sqlExecutor()->execute("SELECT * FROM activity"););
    ASSERT_NO_THROW(q.first(););
    EXPECT_EQ(q.record().value("description").toString().toStdString(), description);
}

TEST_F(DatabaseSqlExecutorMethodsTest, dbPathException) {
    EXPECT_THROW(sqlExecutor()->dbPath(), logic_error);
}

TEST_F(DatabaseSqlExecutorMethodsTest, dbPath) {
    sqlExecutor()->setConnectionString(":memory:");
    EXPECT_EQ(sqlExecutor()->dbPath(), ":memory:");
    sqlExecutor()->setConnectionString(dbPath().string());
    EXPECT_EQ(sqlExecutor()->dbPath(), dbPath().string());
}

TEST_F(DatabaseSqlExecutorMethodsTest, isConnectionOpen) {
    EXPECT_FALSE(sqlExecutor()->isConnectionOpen());
    sqlExecutor()->setConnectionString(":memory:");
    EXPECT_TRUE(sqlExecutor()->isConnectionOpen());
}

TEST_F(DatabaseSqlExecutorMethodsTest, tablesException) {
    EXPECT_THROW(sqlExecutor()->tables(), logic_error);
}

TEST_F(DatabaseSqlExecutorMethodsTest, tables) {
    sqlExecutor()->setConnectionString(":memory:");
    EXPECT_EQ(sqlExecutor()->tables().size(), 0);
    string tableName = "activity";
    sqlExecutor()->execute("CREATE TABLE " + tableName + " (description TEXT)");
    EXPECT_EQ(sqlExecutor()->tables().size(), 1);
    EXPECT_TRUE(sqlExecutor()->tables().count(tableName));
}

}}}
