#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "QGuiContext.hpp"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    QGuiContext guiContext;
    engine.rootContext()->setContextObject(&guiContext);
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    return app.exec();
}
