TEMPLATE = subdirs

SUBDIRS += app lib

app.depends = lib

!android {
!win32 {
    SUBDIRS += util test

    test.depends = lib
    util.depends = lib
}}
