#include "MyEnvironment.hpp"

#include <QDebug>
#include <QSqlDatabase>
#include <QSqlQuery>

using namespace std;

MyEnvironment::MyEnvironment()
{

}

void createDatabase()
{
    if(QSqlDatabase::database().isValid())
        return;

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(":memory:");
    db.open();

    QSqlQuery query(db);

    query.exec("CREATE TABLE activity ("
               "id INTEGER PRIMARY KEY ASC AUTOINCREMENT,"
               "description TEXT);");

    query.exec("CREATE TABLE interval ("
               "id INTEGER PRIMARY KEY ASC AUTOINCREMENT,"
               "activity_id INTEGER,"
               "start NUMERIC NOT NULL DEFAULT (strftime('%s', 'now')),"
               "end NUMERIC DEFAULT NULL);");

    query.exec("CREATE TABLE tag ("
               "id INTEGER PRIMARY KEY ASC AUTOINCREMENT,"
               "name TEXT)");

    query.exec("CREATE TABLE activity_tag ("
               "activity_id INTEGER,"
               "tag_id INTEGER,"
               "PRIMARY KEY (activity_id, tag_id)"
               ")");

    query.exec("CREATE TABLE tag_tag ("
               "tag_id INTEGER,"
               "super_tag_id INTEGER,"
               "PRIMARY KEY (tag_id, super_tag_id))");

    query.exec("INSERT INTO activity (description) VALUES ('Activity 1');");
    query.exec("INSERT INTO activity (description) VALUES ('Activity 2');");
    query.exec("INSERT INTO activity (description) VALUES ('Activity 3');");
    query.exec("INSERT INTO activity (description) VALUES ('Activity 4');");

    query.exec("INSERT INTO interval (activity_id, start) "
               "VALUES (3, strftime('%s', 'now', '-10 minutes'));");
    query.exec("INSERT INTO interval (activity_id, start, end) "
               "VALUES (1, strftime('%s', 'now', '-50 minutes'), strftime('%s', 'now', '-40 minutes'));");
    query.exec("INSERT INTO interval (activity_id, start, end) "
               "VALUES (1, strftime('%s', 'now', '-30 minutes'), strftime('%s', 'now', '-20 minutes'));");
    query.exec("INSERT INTO interval (activity_id, start, end) "
               "VALUES (2, strftime('%s', 'now', '-21 minutes'), strftime('%s', 'now', '-11 minutes'));");

    for(int i = 1; i <= 10; ++i){
        QString s = QString::fromStdString(to_string(i));
        query.exec("INSERT INTO tag (name) VALUES ('" + s + s + s + "');");
    }

    query.exec("INSERT INTO activity_tag (activity_id, tag_id) VALUES (1, 1);");
    query.exec("INSERT INTO activity_tag (activity_id, tag_id) VALUES (2, 1);");
    query.exec("INSERT INTO activity_tag (activity_id, tag_id) VALUES (2, 2);");

    query.exec("INSERT INTO tag_tag (tag_id, super_tag_id) VALUES (2, 1);");
    query.exec("INSERT INTO tag_tag (tag_id, super_tag_id) VALUES (2, 3);");
}

void MyEnvironment::SetUp()
{
    createDatabase();
}

void MyEnvironment::TearDown()
{
    for(QString name : QSqlDatabase::connectionNames()){
        QSqlDatabase::removeDatabase(name);
    }
}
