#ifndef GUARD_BF6F8A0D5A774E3C88433ECF4FB07148
#define GUARD_BF6F8A0D5A774E3C88433ECF4FB07148

#include <memory>

#include <QDateTime>

#include "qt-supermacros/QQmlVarPropertyHelpers.h"
#include "qt-supermacros/QQmlPtrPropertyHelpers.h"

#include "mylib_global.h"

class QAbstractItemModel;

namespace MyTime {

namespace Database { namespace Backend {
    class Basic;
}}

namespace Models {
    class QTagModel;
}

class MYLIB_EXPORT QAnalyticsContext : public QObject
{
    Q_OBJECT
    QML_READONLY_VAR_PROPERTY(QString,              average)
    QML_READONLY_VAR_PROPERTY(QString,              averageFilter)
    QML_READONLY_PTR_PROPERTY(QAbstractItemModel,   chartModel)
    QML_READONLY_VAR_PROPERTY(QString,              durationSum)
    QML_READONLY_VAR_PROPERTY(QDateTime,            end)
    QML_READONLY_VAR_PROPERTY(QString,              filterInterval)
    QML_READONLY_PTR_PROPERTY(QAbstractItemModel,   newTagSelectorModel)
    QML_READONLY_VAR_PROPERTY(QDateTime,            start)
    QML_READONLY_VAR_PROPERTY(QString,              startEndInterval)
    QML_READONLY_PTR_PROPERTY(QAbstractItemModel,   tagSelectorModel)

public:
    explicit QAnalyticsContext(const std::shared_ptr<Models::QTagModel> &allTagsModel,
                               const std::shared_ptr<Database::Backend::Basic> &dbBackend,
                               QObject *parent = Q_NULLPTR);

public slots:
    void update(const QString &keywords, const QList<int> &tags, const QDateTime &from, const QDateTime &to);

private:
    QString averageDurationStr(const double &duration, const double &start, const double &end, const double &mult) const;
    QString durationStr(const double &duration) const;
    void    initialize();

    std::shared_ptr<Database::Backend::Basic> dbBackend_;

    static const QString AGREGATE_QUERY;
};
}

#endif // GUARD_BF6F8A0D5A774E3C88433ECF4FB07148
