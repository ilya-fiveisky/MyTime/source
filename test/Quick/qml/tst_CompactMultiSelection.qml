import QtQuick 2.3
import QtTest 1.0
import "../../../lib"

Rectangle {
    height: 300
    width: 300

    CompactMultiSelection {
        TestCase {
            name: "CompactMultiSelection Empty Props Init"

            function test_props() {
                compare(parent.selectorModel, undefined);
                compare(parent.selected, undefined);
                compare(parent.elementHeight, 20);
            }

        }
    }

    ListModel {
        id: tags

        function idByRow(row){
            return get(row).id;
        }

        function setFilter(str){

        }

        ListElement {
            property int id: 1
            name: "111"
        }
        ListElement {
            property int id: 2
            name: "222"
        }
        ListElement {
            property int id: 3
            name: "333"
        }
    }

    ListModel {
        id: selectedTags

        property var ids: [2, 3]

        ListElement {
            property int id: 2
            name: "222"
        }
        ListElement {
            property int id: 3
            name: "333"
        }
    }

    CompactMultiSelection {
        selectorModel: tags
        selected: selectedTags
        elementHeight: 30
        focus: true

        TestCase {
            name: "CompactMultiSelection Tests"
            when: windowShown

            SignalSpy {
                id: spy
                signalName: "clicked"
            }

            function test_plusButton() {
                var plusButton = findChild(parent, "plusButton");
                verify(plusButton, "plusButton is null");
                compare(plusButton.height, 30);
                compare(plusButton.width, 30);
                spy.clear();
                spy.target = plusButton;
                mouseClick(plusButton);
                compare(spy.count, 1);
                spy.clear();
                keyClick(Qt.Key_Space);
                compare(selectedTags.ids.length, 3);
                compare(selectedTags.ids, [2, 3, 1]);
            }

            function test_props() {
                compare(parent.selectorModel.count, 3);
                compare(parent.selected.count, 2);
                compare(parent.elementHeight, 30);
            }

            function test_repeater() {
                var repeater = findChild(parent, "repeater");
                verify(repeater, "repeater is null");
                compare(repeater.model, selectedTags);
                compare(repeater.count, 2);
            }

            function test_tag_deletion() {
                var deleteButton = findChild(parent, "deleteButton");
                verify(deleteButton, "deleteButton is null");
                mouseClick(deleteButton);
                compare(selectedTags.ids.length, 2);
            }
        }
    }
}
