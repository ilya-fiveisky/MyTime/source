import QtQuick 2.5
import QtQuick.Controls 2.0

Button {
    text: selectedDate.toLocaleDateString(Qt.locale(), "dd MMM yyyy")

    property alias selectedDate: calendar.selectedDate

    Popup {
        id: calendarPopup

        Calendar {
            id: calendar

            onSelectedDateChanged: {
                calendarPopup.close();
            }
        }
    }

    onClicked: {
        calendarPopup.open();
    }
}
