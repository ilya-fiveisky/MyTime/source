import QtQuick 2.3
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtTest 1.0
import "../../../lib"

Page {
    id: root
    height: 480
    width: 640

    ListModel {
        id: tags

        property int makeNewCount: 0
        property int deleteRowIndex: -1

        function idByRow(row){
            return get(row).id;
        }

        function makeNew(){
            makeNewCount ++;
        }

        function deleteRow(index){
            deleteRowIndex = index;
        }

        ListElement {
            property int id: 1
            name: "111"
        }
        ListElement {
            property int id: 2
            name: "222"
        }
        ListElement {
            property int id: 3
            name: "333"
        }
    }

    header: ToolBar {
        RowLayout {
            id: toolBarLayout
        }
    }

    TagEditor {
        id: tagEditor
        tagModel: tags
        toolBarLayout: toolBarLayout
        anchors.fill: parent

        TestCase {
            name: "TagEditor Tests"
            when: windowShown

            SignalSpy {
                id: spy
                signalName: "clicked"
            }

            function test_000_props() {
                compare(tagEditor.toolBarLayout, toolBarLayout, "incorrect toolBarLayout");
                compare(tagEditor.tagModel, tags, "tagModel != tags");
            }

            function test_001_addTagButton() {
                var addTagButton = findChild(toolBarLayout, "addTagButton");
                verify(addTagButton, "addTagButton is null");
                verify(addTagButton.visible, "addTagButton is not visible");
                spy.clear();
                spy.target = addTagButton;
                mouseClick(addTagButton);
                compare(spy.count, 1, "spy.count != 1");
                compare(tags.makeNewCount, 1, "tags.makeNewCount != 1");
                spy.clear();
            }

            function test_002_tag_deletion() {
                var deleteButton = findChild(parent, "deleteButton1");
                verify(deleteButton, "deleteButton is null");
                verify(deleteButton.visible, "deleteButton is not visible");
                spy.clear();
                spy.target = deleteButton;
                mouseClick(deleteButton);
                compare(spy.count, 1, "spy.count != 1");
                spy.clear();

                var yesButton = findChild(deleteButton, "yesButton");
                verify(yesButton, "yesButton is null");
                verify(yesButton.visible, "yesButton is not visible");
                spy.clear();
                spy.target = yesButton;
                mouseClick(yesButton);
                compare(spy.count, 1, "spy.count != 1");
                compare(tags.deleteRowIndex, 1, "tags.deleteRowIndex != 1");
                spy.clear();
            }
        }
    }
}
