#include "gtest/gtest.h"

#include "AndroidPrinter.hpp"
#include "MyEnvironment.hpp"

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);

//    ::testing::AddGlobalTestEnvironment(new MyEnvironment());
#ifdef MY_OS_ANDROID
    ::testing::TestEventListeners& listeners =
          ::testing::UnitTest::GetInstance()->listeners();
    // Adds a listener to the end.  Google Test takes the ownership.
    listeners.Append(new MyTime::Test::AndroidPrinter);
#endif
    return RUN_ALL_TESTS();
}
