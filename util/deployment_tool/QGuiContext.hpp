#pragma once

#include <QObject>
#include <QList>
#include <QUrl>

class QGuiContext : public QObject
{
    Q_OBJECT
public:
    explicit QGuiContext(QObject *parent = 0);

public slots:
    void deploy(const QString &librarySources, const bool &isDebug,
                const QString &folderSources,
                const QUrl &executableFile,
                const QUrl &targetFolder);

    QStringList loadConfiguration(const QUrl &loadFromPath);

    void saveConfiguration(const QUrl &saveToPath,
                           const QString &librarySources,
                           const QString &folderSources,
                           const QUrl &executableFile,
                           const QUrl &targetFolder);

private:
    QList<QUrl> getUrlList(const QString &urlString) const;
};
