#include <memory>

#include <boost/filesystem.hpp>
#include <boost/system/error_code.hpp>

#include <QtTest>

#include <QCoreApplication>
#include <QSettings>
#include <QUrl>

#include "Database/Backend/Editor/Activity.hpp"
#include "Database/SqlExecutor.hpp"
#include "Database/Store.hpp"
#include "Database/StoreImpl.hpp"

using namespace std;
using namespace boost::filesystem;
using namespace MyTime;
using namespace MyTime::Database;

class DatabaseStoreTest : public QObject
{
    Q_OBJECT

public:
    DatabaseStoreTest();
    ~DatabaseStoreTest();

private Q_SLOTS:
    void testPeriodicBackup();

private:
    const boost::filesystem::path TEMP_TEST_DIR{
        boost::filesystem::temp_directory_path() /= boost::filesystem::unique_path()};

    const std::shared_ptr<QSettings> SETTINGS {
        std::make_shared<QSettings>(QString::fromStdString(
                                   (boost::filesystem::path(TEMP_TEST_DIR) /= boost::filesystem::unique_path()).string()),
                               QSettings::IniFormat) };

    const int BACKUP_TIMER_INTERVAL = 1000;

    const path dbPath{path(TEMP_TEST_DIR) /= boost::filesystem::unique_path()};
    const path backupPath{path(dbPath).replace_extension(StoreImpl::BACKUP_FILE_EXTENSION)};
};

DatabaseStoreTest::DatabaseStoreTest()
{
    create_directory(TEMP_TEST_DIR);
}

DatabaseStoreTest::~DatabaseStoreTest()
{
    boost::system::error_code errorCode;
    remove_all(TEMP_TEST_DIR, errorCode);
}

void DatabaseStoreTest::testPeriodicBackup()
{
    auto sqlExecutor = make_shared<SqlExecutor>();
    Store store{sqlExecutor, SETTINGS, TEMP_TEST_DIR.string(), BACKUP_TIMER_INTERVAL};
    QUrl url{QUrl::fromLocalFile(QString::fromStdString(dbPath.string()))};
    store.newDb(url);
    auto activityBackend = make_shared<Backend::Editor::Activity>(sqlExecutor);
    auto t = last_write_time(backupPath);
    activityBackend->create();
    QTest::qWait(2000);
    QVERIFY(t < last_write_time(backupPath));
}

QTEST_MAIN(DatabaseStoreTest)

#include "DatabaseStoreTest.moc"
