#ifndef GUARD_B9D83BC98F984B4088BFCDD456927E77
#define GUARD_B9D83BC98F984B4088BFCDD456927E77

#include <exception>
#include <string>

#include "my_throw.hpp"

namespace MyTime {

template<typename PtrT> void check_ptr_arg(const PtrT &ptr, const std::string &argName)
{
    if(!ptr){
        my_throw<std::invalid_argument>(argName + " is NULL");
    }
}

}

#endif // GUARD_B9D83BC98F984B4088BFCDD456927E77
