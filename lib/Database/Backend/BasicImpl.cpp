#include "BasicImpl.hpp"

#include <exception>

#include <QSqlQuery>
#include <QSqlRecord>

#include "check_ptr_arg.hpp"

#include "../SqlExecutor.hpp"
#include "Basic.hpp"

using namespace std;

namespace MyTime { namespace Database { namespace Backend {

const QString BasicImpl::END_TIME_FILTER_PLACEHOLDER = "${endTimeFilterPlaceholder}";
const QString BasicImpl::KEYWORD_FILTER_PLACEHOLDER = "${keywordFilterPlaceholder}";
const QString BasicImpl::TAG_FILTER_PLACEHOLDER = "${tagFilterPlaceholder}";
const QString BasicImpl::START_TIME_FILTER_PLACEHOLDER = "${startTimeFilterPlaceholder}";


const QString BasicImpl::CHILDREN_TAGS_QUERY =
        "SELECT tag_id FROM tag_tag WHERE super_tag_id = ?";

/// Embracing SELECT and corresponding WHERE are necessary besause of NULL interval.end processing.
const QString BasicImpl::FILTERED_JOIN =
        "SELECT * FROM "
        "("
        "SELECT "
        "   activity.id AS activity_id, "
        "   activity.description as description, "
        "   interval.start AS start, "
        "   ifnull(interval.end, CAST(strftime('%s','now') AS NUMERIC)) AS end "
        "FROM "
        "   (((activity LEFT OUTER JOIN interval "
        "   ON activity.id = interval.activity_id) "
        "   LEFT OUTER JOIN activity_tag "
        "   ON activity.id = activity_tag.activity_id) "
        "   LEFT OUTER JOIN tag "
        "   ON tag.id = activity_tag.tag_id) "
        "WHERE "    + BasicImpl::TAG_FILTER_PLACEHOLDER +
        "   AND "   + BasicImpl::KEYWORD_FILTER_PLACEHOLDER +
        "   AND start   >= " + BasicImpl::START_TIME_FILTER_PLACEHOLDER +
        " "
        "GROUP BY interval.id"
        ") "
        "WHERE end <= " + BasicImpl::END_TIME_FILTER_PLACEHOLDER;

BasicImpl::BasicImpl(const std::shared_ptr<MyTime::Database::SqlExecutor> &sqlExecutor):
    sqlExecutor_{sqlExecutor}
{
    check_ptr_arg(sqlExecutor, "sqlExecutor");
}

void BasicImpl::createEmptyDb()
{
    sqlExecutor_->executeInTransaction
            ({
                 "CREATE TABLE activity ("
                 "id INTEGER PRIMARY KEY ASC AUTOINCREMENT,"
                 "description TEXT);",

                 "CREATE TABLE interval ("
                 "id INTEGER PRIMARY KEY ASC AUTOINCREMENT,"
                 "activity_id INTEGER,"
                 "start NUMERIC NOT NULL DEFAULT (strftime('%s', 'now')),"
                 "end NUMERIC DEFAULT NULL);",

                 "CREATE TABLE tag ("
                 "id INTEGER PRIMARY KEY ASC AUTOINCREMENT,"
                 "name TEXT)",

                 "CREATE TABLE activity_tag ("
                 "activity_id INTEGER,"
                 "tag_id INTEGER,"
                 "PRIMARY KEY (activity_id, tag_id)"
                 ")",

                 "CREATE TABLE tag_tag ("
                 "tag_id INTEGER,"
                 "super_tag_id INTEGER,"
                 "PRIMARY KEY (tag_id, super_tag_id))"
             });
}

void BasicImpl::initializeDb()
{
    if(sqlExecutor_->isConnectionOpen()){
        if(sqlExecutor_->tables().empty()){
            createEmptyDb();
        }
    }
}

BasicImpl::~BasicImpl() = default;

vector<int> BasicImpl::childrenTags(const int &parentId)
{
    vector<int> result;
    childrenTags(parentId, parentId, result);
    return result;
}

QString BasicImpl::constructFilteredJoinString(const QString &keywords, const boost::optional<int> &tag,
                                               const QDateTime &from, const QDateTime &to)
{
    QString keywordsCondition;
    if(keywords.trimmed() == ""){
        keywordsCondition = "1";
    }
    else{
        auto keywordsList = keywords.split(" ", QString::SplitBehavior::SkipEmptyParts);
        keywordsCondition = accumulate(
                    std::next(keywordsList.begin()),
                    keywordsList.end(),
                    " activity.description LIKE '%" + keywordsList[0] + "%'",
                [](const QString &a, const QString &b) {
            return a + " OR activity.description LIKE '%" + b + "%'";
        });
    }

    string tagsCondition = tag ? childrenTagsCondition(*tag) : "1";

    return QString(FILTERED_JOIN).
            replace(TAG_FILTER_PLACEHOLDER, QString::fromStdString(tagsCondition)).
            replace(KEYWORD_FILTER_PLACEHOLDER, keywordsCondition).
            replace(START_TIME_FILTER_PLACEHOLDER,
                    QString::fromStdString(to_string(toUtc(from).toTime_t()))).
            replace(END_TIME_FILTER_PLACEHOLDER,
                    QString::fromStdString(to_string(toUtc(to).toTime_t())));
}

QDateTime BasicImpl::toUtc(const QDateTime &dt)
{
    return dt.timeSpec() == Qt::UTC ? dt : dt.toUTC();
}

void BasicImpl::childrenTags(const int &parentId, const int &root, std::vector<int> &aggregate)
{
    QSqlQuery q{sqlExecutor_->execute(CHILDREN_TAGS_QUERY.toStdString(), {parentId})};
    while (q.next()) {
        int childId = q.record().value(0).toInt();
        aggregate.push_back(childId);
        if(childId != root){
            childrenTags(childId, root, aggregate);
        }
    }
}

string BasicImpl::childrenTagsCondition(const int &tag)
{
    auto children = childrenTags(tag);
    children.push_back(tag);
    return
            "tag.id IN (" +
            accumulate(
                std::next(children.begin()),
                children.end(),
                to_string(children[0]),
            [](const string &a, const int &b) {
        return a + "," + to_string(b);
    }) +
    ")";
}

}}}
