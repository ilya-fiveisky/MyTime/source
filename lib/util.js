
.pragma library

function splittrim(str, separator) {
    if(str.trim().length > 0){
        return str.split(separator).map(function (s){return s.trim();});
    }
    else{
        return [];
    }
}
