#ifndef GUARD_82DF4271B2CE4B10B8D50E1D0291C963
#define GUARD_82DF4271B2CE4B10B8D50E1D0291C963

#include <functional>
#include <memory>

#include <QString>

namespace MyTime {

namespace Database {
    class Store;
}

class QGUIContext;

struct QGUIContextImpl
{
    explicit QGUIContextImpl(std::shared_ptr<Database::Store> store);
    virtual ~QGUIContextImpl();

    QString doActionWithStringResult(const std::function<void ()> &action,
                                     QGUIContext *backPtr);

    std::shared_ptr<Database::Store> store_;
};

}
#endif // GUARD_82DF4271B2CE4B10B8D50E1D0291C963
