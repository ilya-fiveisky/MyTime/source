#include <map>
#include <memory>

#include <QCoreApplication>
#include <QDateTime>
#include <QDebug>
#include <QFile>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>

#include "Database/SqlExecutor.hpp"
#include "Database/Store.hpp"

using namespace MyTime;
using namespace std;

QString dir = "/home/ilya/Documents/MyTime/";

map<int, int> fill_tag(){
    QFile file(dir + "tag");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        qDebug() << file.errorString();
    }

    map<int, int> ids;

    file.readLine();
    file.readLine();
    int id = 1;
    while (!file.atEnd()) {
        QString line = file.readLine();
        QStringList splitted = line.split(" ", QString::SkipEmptyParts);
        splitted.removeLast();

        int oldId = splitted[0].trimmed().toInt();
        ids.emplace(make_pair(oldId, id));

        splitted.removeFirst();
        QVariant name = splitted.join(" ");
        if(name.toString().contains("[null]")){
            name = QVariant();
        }
        QSqlQuery query;
        query.prepare("INSERT INTO tag (name) VALUES (:name);");
        query.bindValue(":name", name);
        if(!query.exec()){
            qDebug() << query.lastError();
        }
        id++;
    }
    return ids;
}

map<int, int> fill_activity(){
    QFile file(dir + "atom");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        qDebug() << file.errorString();
    }

    map<int, int> ids;

    QString dateFormat = "yyyy-MM-dd";
    QString timeFormat = "HH:mm:ss.z";

    file.readLine();
    file.readLine();
    int activity_id = 1;
    while (!file.atEnd()) {
        QString line = file.readLine();
        QStringList splitted = line.split(" ", QString::SkipEmptyParts);
        splitted.removeLast();

        int oldId = splitted[0].trimmed().toInt();
        ids.emplace(make_pair(oldId, activity_id));

        QDate startDate = QDate::fromString(splitted[1].trimmed(), dateFormat);
        QTime startTime = QTime::fromString(splitted[2].trimmed(), timeFormat);
        QDateTime start(startDate, startTime);
        QDate endDate = QDate::fromString(splitted[3].trimmed(), dateFormat);
        QTime endTime = QTime::fromString(splitted[4].trimmed(), timeFormat);
        QDateTime end(endDate, endTime);

        for(int i = 0; i < 5; ++i){
            splitted.removeFirst();
        }

        QVariant description = splitted.join(" ");
        if(description.toString().contains("[null]")){
            description = QVariant();
        }
        QSqlQuery query;
        query.prepare("INSERT INTO activity (description) VALUES (:description);");
        query.bindValue(":description", description);
        if(!query.exec()){
            qDebug() << query.lastError();
        }

        query.prepare("INSERT INTO interval (activity_id, start, end) VALUES (:activity_id, :start, :end);");
        query.bindValue(":activity_id", activity_id);
        query.bindValue(":start", start.toTime_t());
        query.bindValue(":end", end.toTime_t());
        if(!query.exec()){
            qDebug() << query.lastError();
        }

        activity_id++;
    }
    return ids;
}

void fill_activity_tag(const map<int, int> &activityIds, const map<int, int> &tagIds){
    QFile file(dir + "atom_tag");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        qDebug() << file.errorString();
    }

    file.readLine();
    file.readLine();
    while (!file.atEnd()) {
        QString line = file.readLine();
        QStringList splitted = line.split(" ", QString::SkipEmptyParts);
        splitted.removeLast();
        int activity_id = activityIds.at(splitted[0].trimmed().toInt());
        int tag_id = tagIds.at(splitted[1].trimmed().toInt());
        QSqlQuery query;
        query.prepare("INSERT INTO activity_tag (activity_id, tag_id) VALUES (:activity_id, :tag_id);");
        query.bindValue(":activity_id", activity_id);
        query.bindValue(":tag_id", tag_id);
        if(!query.exec()){
            qDebug() << query.lastError();
        }
    }
}

int main()
{
    Database::Store store(make_shared<Database::SqlExecutor>());
    store.newDb(dir + "my_time");

    fill_activity_tag(fill_activity(), fill_tag());

    return 0;
}
