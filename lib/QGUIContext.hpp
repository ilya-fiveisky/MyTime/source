#ifndef GUARD_6AB25C2ABDD14569B297EFE4163E2597
#define GUARD_6AB25C2ABDD14569B297EFE4163E2597

#include <memory>

#include <QUrl>

#include "qt-supermacros/QQmlVarPropertyHelpers.h"
#include "qt-supermacros/QQmlPtrPropertyHelpers.h"

#include "mylib_global.h"

#include "QAnalyticsContext.hpp"
#include "Models/QActivityModel.hpp"
#include "Models/QTagModel.hpp"
#include "Models/QTagNameFilterProxyModel.hpp"

namespace MyTime {

namespace Database {
    class Store;
}

struct QGUIContextImpl;

class MYLIB_EXPORT QGUIContext : public QObject
{
    Q_OBJECT

    QML_READONLY_SHARED_PTR_PROPERTY(MyTime::Models::QActivityModel,           activityViewModel)
    QML_READONLY_SHARED_PTR_PROPERTY(QAnalyticsContext,                        analytics)
    QML_READONLY_VAR_PROPERTY       (QString,                                  dbPath)
    QML_READONLY_SHARED_PTR_PROPERTY(MyTime::Models::QTagModel,                tagViewModel)
    QML_READONLY_SHARED_PTR_PROPERTY(MyTime::Models::QTagNameFilterProxyModel, tagSelectorModel)

    friend struct QGUIContextImpl;

public:
    explicit QGUIContext(const std::shared_ptr<Database::Store> &store,
                         const std::shared_ptr<Models::QActivityModel> &activityViewModel,
                         const std::shared_ptr<QAnalyticsContext> &analyticsConext,
                         const std::shared_ptr<Models::QTagModel> &tagViewModel,
                         QObject *parent = 0);
    virtual ~QGUIContext();

public slots:
    QString newDb   (const QUrl &fileUrl) noexcept;
    QString openDb  (const QUrl &fileUrl) noexcept;
    QString saveDbAs(const QUrl &fileUrl) noexcept;

private:
    std::unique_ptr<QGUIContextImpl> impl_;
};
}

#endif // GUARD_6AB25C2ABDD14569B297EFE4163E2597
