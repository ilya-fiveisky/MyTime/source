#include "SqlExecutor.hpp"

#include <algorithm>
#include <string>

#include <stdx/scope_guard/finally.h>

#include <boost/algorithm/string.hpp>
#include <boost/log/attributes/named_scope.hpp>
#include <boost/log/support/exception.hpp>
#include <boost/throw_exception.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <QDebug>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>

#include "db_exception.hpp"
#include "my_throw.hpp"

using namespace std;
using namespace stdx;
namespace logging = boost::log;

namespace MyTime { namespace Database {

SqlExecutor::SqlExecutor()
{
    boost::uuids::random_generator gen;
    boost::uuids::uuid u = gen();

    connection_ = QSqlDatabase::addDatabase("QSQLITE", QString::fromStdString(to_string(u)));
    if(connection_.lastError().isValid()){
        my_throw<db_exception>(getErrorString());
    }
}

SqlExecutor::SqlExecutor(const string &connectionString): SqlExecutor()
{
    setConnectionString(connectionString);
}

SqlExecutor::~SqlExecutor()
{
    connection_.close();
    QSqlDatabase::removeDatabase(connection_.connectionName());
}

QSqlQuery SqlExecutor::execute(const std::string &queryStr, const std::vector<QVariant> &params)
{
    checkConnection();
    if(boost::trim_copy(queryStr).empty()){
        my_throw<invalid_argument>("Query string is empty");
    }
    QSqlQuery q(connection_);
    executeCheckableCommand([&q, queryStr](){
        return q.prepare(QString::fromStdString(queryStr)); });
    for(size_t i = 0; i < params.size(); ++i){
        q.bindValue(i, params[i]);
    }
    executeCheckableCommand([&q](){
        return q.exec(); });

    return q;
}

void SqlExecutor::executeInImmediateTransaction(const std::function<void ()> &f)
{
    checkConnection();
    execute("BEGIN IMMEDIATE;");
    auto guard = finally([this]{ QSqlQuery q(connection_); q.exec("ROLLBACK;"); });
    f();
}

void SqlExecutor::executeCheckableCommand(const std::function<bool()> &f)
{
    BOOST_LOG_FUNCTION();
    if(!f()){
        my_throw<db_exception>(getErrorString());
    }
}

string SqlExecutor::getErrorString() const
{
    return connection_.lastError().text().toStdString();
}

void SqlExecutor::executeInTransaction(const std::vector<string> &queryStrs)
{
    checkConnection();
    if(queryStrs.empty()){
        return;
    }
    if(connection_.transaction()){
        try{
            for(auto s : queryStrs){
                execute(s);
            }
            executeCheckableCommand([this](){return connection_.commit();});
        }
        catch(...){
            executeCheckableCommand([this](){return connection_.rollback();});
            throw;
        }
    }
    else{
        executeCheckableCommand([this](){return connection_.rollback();});
        my_throw<db_exception>(getErrorString());
    }
}

string SqlExecutor::dbPath() const
{
    checkConnection();
    return connection_.databaseName().toStdString();
}

bool SqlExecutor::isConnectionOpen() const
{
    return connection_.isOpen();
}

std::set<string> SqlExecutor::tables()
{
    checkConnection();
    set<string> result;
    auto tables = connection_.tables();
    transform(tables.begin(), tables.end(), inserter(result, result.begin()),
              [](const QString &s){ return s.toStdString(); });

    return result;
}

void SqlExecutor::setConnectionString(const string &connectionStr)
{
    BOOST_LOG_FUNCTION();
    connection_.close();
    connection_.setDatabaseName(QString::fromStdString(connectionStr));
    if(connection_.lastError().isValid()){
        my_throw<db_exception>(getErrorString());
    }
    executeCheckableCommand([this](){
        return connection_.open(); });

    dbChanged();
}

void SqlExecutor::checkConnection() const
{
    if(!connection_.isOpen()){
        my_throw<logic_error>("Connection is closed");
    }
}

}}
