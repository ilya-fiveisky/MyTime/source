#include "gtest/gtest.h"

#include <algorithm>
#include <set>
#include <vector>

#include <QSqlRecord>

#include "Database/db_exception.hpp"
#include "Database/SqlExecutor.hpp"
#include "Database/Backend/Basic.hpp"

#include "../BasicTest.hpp"
#include "BasicMethodsTest.hpp"

using namespace std;

using namespace MyTime;
using namespace MyTime::Database;

typedef MyTime::Database::Backend::Basic BasicBackend;

namespace MyTime { namespace Test { namespace Database { namespace Backend {

typedef BasicTest DatabaseBackendBasicTest;

TEST_F(DatabaseBackendBasicTest, constructorExceptions) {
    EXPECT_THROW(BasicBackend b(nullptr), invalid_argument);
}

TEST_F(DatabaseBackendBasicTest, constructor) {
    EXPECT_NO_THROW(BasicBackend b(sqlExecutor()));
}

TEST_F(DatabaseBackendBasicTest, constructorWithConnectedSqlExecutor) {
    sqlExecutor()->setConnectionString(":memory:");
    EXPECT_NO_THROW(BasicBackend b(sqlExecutor()));
    set<string> tables{"activity", "interval", "tag", "activity_tag", "tag_tag"};
    auto executorTables = sqlExecutor()->tables();
    EXPECT_TRUE(includes(executorTables.begin(), executorTables.end(), tables.begin(), tables.end()))
            << "Tables are: "
            << accumulate(executorTables.begin(), executorTables.end(), string(""),
                          [](const string &a, const string &b){return a + " " + b;});
}

typedef BasicMethodsTest<BasicBackend> DatabaseBackendBasicMethodsTest;

TEST_F(DatabaseBackendBasicMethodsTest, selectExceptions) {
    EXPECT_THROW(backend()->select("SELECT"), db_exception);
    sqlExecutor()->setConnectionString(":memory:");
    EXPECT_THROW(backend()->select(""), invalid_argument);
    EXPECT_THROW(backend()->select("CREATE"), invalid_argument);
}

TEST_F(DatabaseBackendBasicMethodsTest, select) {
    sqlExecutor()->setConnectionString(":memory:");
    string description{"some description"};
    sqlExecutor()->execute("INSERT INTO activity (description) VALUES ('" + description + "')");
    vector<QSqlRecord> records;
    ASSERT_NO_THROW(
                auto result = backend()->select("SELECT * FROM activity");
                records.assign(begin(result), end(result));
                );
    ASSERT_EQ(records.size(), 1);
    ASSERT_EQ(records[0].value("description").toString().toStdString(), description);
}

TEST_F(DatabaseBackendBasicMethodsTest, constructFilteredJoinString) {
    QString query;
    QDateTime from{QDateTime::currentDateTime().addYears(-1)};
    QDateTime to{QDateTime::currentDateTime().addYears(1)};
    ASSERT_NO_THROW(query = backend()->constructFilteredJoinString("", {}, from, to));
    ASSERT_FALSE(query.isEmpty());
    ASSERT_TRUE(query.startsWith("SELECT "));
    ASSERT_FALSE(query.contains("INTERSECT"));
    sqlExecutor()->setConnectionString(":memory:");
    ASSERT_NO_THROW(query = backend()->constructFilteredJoinString("", {0, 1}, from, to));
    ASSERT_FALSE(query.isEmpty());
    ASSERT_TRUE(query.startsWith("SELECT "));
    ASSERT_EQ(query.count(" INTERSECT "), 1);
    ASSERT_NO_THROW(query = backend()->constructFilteredJoinString("key1 key2", {0, 1, 2}, from, to));
    ASSERT_FALSE(query.isEmpty());
    ASSERT_TRUE(query.startsWith("SELECT "));
    ASSERT_EQ(query.count(" INTERSECT "), 2);
}

TEST_F(DatabaseBackendBasicMethodsTest, constructFilteredJoinStringAndTestAgainstDb) {
    QString query;
    QDateTime from{QDateTime::currentDateTime().addYears(-1)};
    QDateTime to{QDateTime::currentDateTime().addYears(1)};
    ASSERT_NO_THROW(query = backend()->constructFilteredJoinString("", {}, from, to));

    sqlExecutor()->setConnectionString(":memory:");
    string description{"some description"};
    sqlExecutor()->execute("INSERT INTO activity (description) VALUES ('" + description + "')");
    sqlExecutor()->execute("INSERT INTO interval (activity_id, start) VALUES (1, strftime('%s', 'now'))");
    vector<QSqlRecord> records;
    ASSERT_NO_THROW(
                auto result = backend()->select(query);
                records.assign(begin(result), end(result));
                );
    ASSERT_EQ(records.size(), 1);
    ASSERT_EQ(records[0].value("description").toString().toStdString(), description);
}

TEST_F(DatabaseBackendBasicMethodsTest, dbChanged) {
    int countDbChanged = 0;
    EXPECT_NO_THROW(backend()->dbChanged.connect([&countDbChanged](){++countDbChanged;}));
    EXPECT_EQ(countDbChanged, 0);
    sqlExecutor()->setConnectionString(":memory:");
    EXPECT_EQ(countDbChanged, 1);
}

}}}}
