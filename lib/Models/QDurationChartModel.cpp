#include "QDurationChartModel.hpp"

#include <algorithm>
#include <exception>
#include <iterator>

#include <stdx/scope_guard/finally.h>

#include <QDate>
#include <QDebug>
#include <QSqlRecord>

#include "check_ptr_arg.hpp"

#include "Database/Backend/Basic.hpp"

using namespace std;
using namespace stdx;

namespace MyTime { namespace Models {

const QString QDurationChartModel::CHART_QUERY =
        "SELECT "
        "   date(start, 'unixepoch', 'localtime'), "
        "   SUM(end-start)/3600.0 AS duration "
        "FROM "
        "   ("
            + Database::Backend::Basic::FILTERED_JOIN_PLACEHOLDER() +
        "   ) "
        "GROUP BY date(start, 'unixepoch', 'localtime') "
        "ORDER BY start ASC";

QDurationChartModel::QDurationChartModel(const std::shared_ptr<Database::Backend::Basic> &dbBackend, QObject *parent):
    BaseT(parent), dbBackend_{dbBackend}
{
    check_ptr_arg(dbBackend, "db backend");
    dbBackend_->dbChanged.connect([this]()
    {
        refresh();
    });
}

int QDurationChartModel::columnCount(const QModelIndex &) const
{
    return 2;
}

QVariant QDurationChartModel::data(const QModelIndex &index, int) const
{
    if(!index.isValid())
        return QVariant();
    // These two checks should be filtered out by the first one,
    // i.e. they should not throw if there were not pathologic changes in the code.
    if(size_t(index.row()) >= durationMap_.size())
        my_throw<out_of_range>("index row is out of range: " + to_string(index.row()));
    if(index.column() > 1 || index.column() < 0)
        my_throw<out_of_range>("index column is out of range: " + to_string(index.column()));

    auto it = durationMap_.begin();
    for(int i = 0; i < index.row(); ++i) ++it;
    return index.column() == 0 ? QVariant(it->first) : QVariant(it->second);
}

int QDurationChartModel::rowCount(const QModelIndex &) const
{
    return durationMap_.size();
}

void QDurationChartModel::refresh(const QString &keywords, const QList<int> &tags, const QDateTime &from, const QDateTime &to)
{
    beginResetModel();
    auto guard = finally([this]{ endResetModel(); });

    durationMap_.clear();
    QString queryStr{QString(CHART_QUERY).
                replace(Database::Backend::Basic::FILTERED_JOIN_PLACEHOLDER(),
                        dbBackend_->constructFilteredJoinString(keywords, tags, from, to))};
    auto it = dbBackend_->select(queryStr);
    vector<QSqlRecord> result(begin(it), end(it));
    if(!result.empty()){
        QDate start = result[0].value(0).toDate();
        QDate end = result[result.size()-1].value(0).toDate();
        int days = start.daysTo(end) + 1;
        for(int i = 0; i < days; ++i){
            durationMap_[start.addDays(i)] = 0;
        }
        for(auto r : result){
            durationMap_.at(r.value(0).toDate()) = r.value(1).toDouble();
        }
    }
}

void QDurationChartModel::refresh()
{
    refresh("", {},
            QDateTime::currentDateTime().addMonths(-1),
            QDateTime::currentDateTime().addDays(1));
}

}}
