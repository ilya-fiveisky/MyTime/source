#include "gtest/gtest.h"

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

#include <QSettings>

#include "Database/db_exception.hpp"
#include "Database/SqlExecutor.hpp"
#include "Database/Store.hpp"
#include "Database/StoreImpl.hpp"

#include "StoreBasicTest.hpp"
#include "StoreMethodsTest.hpp"

using namespace std;
using namespace boost::filesystem;

using namespace MyTime;
using namespace MyTime::Database;

namespace MyTime { namespace Test { namespace Database {

typedef StoreBasicTest DatabaseStoreConstructorTest;

TEST_F(DatabaseStoreConstructorTest, Exceptions) {
    EXPECT_THROW(Store s(nullptr),                                              invalid_argument);
    EXPECT_THROW(Store s(nullptr,       SETTINGS,   TEMP_TEST_DIR.string()),    invalid_argument);
    EXPECT_THROW(Store s(sqlExecutor(), nullptr,    TEMP_TEST_DIR.string()),    invalid_argument);
    EXPECT_THROW(Store s(sqlExecutor(), SETTINGS,   unique_path().string()),invalid_argument);
}

TEST_F(DatabaseStoreConstructorTest, normalTest) {
    EXPECT_NO_THROW(Store s(sqlExecutor()));
    EXPECT_NO_THROW(Store s(sqlExecutor(), SETTINGS, TEMP_TEST_DIR.string()));
}

typedef StoreMethodsTest DatabaseStoreMethodsTest;

TEST_F(DatabaseStoreMethodsTest, newDbExceptions) {
    path tmp{path(TEMP_TEST_DIR) /= unique_path()};
    EXPECT_FALSE(exists(tmp));
    QUrl url{ QUrl::fromLocalFile(QString::fromStdString(tmp.string())) };
    boost::filesystem::ofstream f{tmp};
    EXPECT_TRUE(exists(tmp));
    EXPECT_THROW(store()->newDb(url), invalid_argument);
}

TEST_F(DatabaseStoreMethodsTest, newDb) {
    store()->newDb(QUrl::fromLocalFile(QString::fromStdString(dbPath().string())));
    checkDb();
    checkSettings();
}

class DatabaseStoreTestWithExistingDb : public DatabaseStoreMethodsTest {
protected:
    using DatabaseStoreMethodsTest::DatabaseStoreMethodsTest;

    virtual void SetUp() override
    {
        DatabaseStoreMethodsTest::SetUp();
        sqlExecutor()->setConnectionString(dbPath().string());
    }
};

TEST_F(DatabaseStoreMethodsTest, pathException) {
    EXPECT_THROW(store()->path(), logic_error);
}

TEST_F(DatabaseStoreTestWithExistingDb, path) {
    EXPECT_EQ(store()->path(), QString::fromStdString(dbPath().string()));
}

TEST_F(DatabaseStoreMethodsTest, openWithArgumentException) {
    path tmp{path(TEMP_TEST_DIR) /= unique_path()};
    EXPECT_FALSE(exists(tmp));
    QUrl url{ QUrl::fromLocalFile(QString::fromStdString(tmp.string())) };
    EXPECT_THROW(store()->open(url), invalid_argument);
}

TEST_F(DatabaseStoreTestWithExistingDb, openWithArgument) {
    store()->open(QUrl::fromLocalFile(QString::fromStdString(dbPath().string())));
    checkBackup();
    checkSettings();
}

class DatabaseStoreOpenArgumentlessNewTest : public DatabaseStoreMethodsTest {
protected:
    DatabaseStoreOpenArgumentlessNewTest():
        DatabaseStoreMethodsTest(StoreImpl::NEW_DB_NAME_DEFAULT)
    {
    }
};

TEST_F(DatabaseStoreOpenArgumentlessNewTest, test) {
    store()->open();
    checkDb();
    checkSettings();
}

class DatabaseStoreOpenArgumentlessExistTest : public DatabaseStoreTestWithExistingDb {
protected:
    DatabaseStoreOpenArgumentlessExistTest():
        DatabaseStoreTestWithExistingDb(StoreImpl::NEW_DB_NAME_DEFAULT)
    {
        SETTINGS->setValue(StoreImpl::LAST_DB_PATH_SETTING_NAME,
                           QUrl::fromLocalFile(QString::fromStdString(dbPath().string())));
    }
};

TEST_F(DatabaseStoreOpenArgumentlessExistTest, normalTest) {
    checkSettings();
    store()->open();
    checkDb();
    checkBackup();
    checkSettings();
}

TEST_F(DatabaseStoreOpenArgumentlessExistTest, abcentDbTest) {
    checkSettings();
    remove(dbPath());
    EXPECT_FALSE(exists(dbPath()));
    store()->open();
    checkDb();
    checkBackup();
    checkSettings();
}

TEST_F(DatabaseStoreTestWithExistingDb, saveAsException) {
    path tmp{path(TEMP_TEST_DIR) /= unique_path()};
    EXPECT_FALSE(exists(tmp));
    QUrl url{ QUrl::fromLocalFile(QString::fromStdString(tmp.string())) };
    EXPECT_TRUE(remove(store()->path().toStdString()));
    EXPECT_THROW(store()->saveAs(url), invalid_argument);
}

TEST_F(DatabaseStoreTestWithExistingDb, saveAs) {
    auto newDbPath = path(TEMP_TEST_DIR) /= unique_path();
    store()->saveAs(QUrl::fromLocalFile(QString::fromStdString(newDbPath.string())));
    checkDb(newDbPath);
    checkBackup(newDbPath);
    checkSettings(newDbPath);
}

}}}
