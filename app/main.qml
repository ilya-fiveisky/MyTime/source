import QtQuick 2.7
import QtQuick.Controls 2.0
import Qt.labs.platform 1.0 as QtLabs
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2
import "."

ApplicationWindow {
    id: app
    title: qsTr("MyTime: ") + dbPath
    height: Screen.desktopAvailableHeight * 0.9
    width: Screen.desktopAvailableWidth / 2
    visible: true

    function setScale(factor){
        toolBar.scale = factor;
        stackLayout.scale = factor;
        tabBar.scale = factor;
    }

    header: ToolBar {
        id: toolBar
        transformOrigin: Item.TopLeft

        RowLayout {
            width: parent.width

            RowLayout {
                id: currentTabToolBarLayout
                Layout.alignment: Qt.AlignLeft
            }

            IconToolButton {
                Layout.alignment: Qt.AlignRight
                iconSource: "icons/more_vert.svg"
                iconColor: "white"
                MyToolTip {text: qsTr("Main Menu")}

                onClicked: menu.open()

                Menu {
                    id: menu

                    MenuItem {
                        text: qsTr("New...")
                        onTriggered: newFileDialog.open()
                    }
                    MenuItem {
                        text: qsTr("Open...")
                        onTriggered: openFileDialog.open()
                    }
                    MenuItem {
                        text: qsTr("Save as ...")
                        onTriggered: saveFileAsDialog.open()
                    }
                    MenuItem {
                        text: qsTr("Exit")
                        onTriggered: Qt.quit();
                    }
                }
            }
        }
    }

    QtLabs.MessageDialog {
        id: message
        buttons: QtLabs.MessageDialog.Close
    }

    QtObject {
        id: paths
        property url documents : Qt.resolvedUrl(QtLabs.StandardPaths.writableLocation(QtLabs.StandardPaths.DocumentsLocation))
    }

    QtLabs.FileDialog {
        id: newFileDialog
        title: qsTr("Please name a file")
        folder: paths.documents
        acceptLabel: qsTr("Create")
        fileMode: QtLabs.FileDialog.SaveFile
        onAccepted: {
            var error = newDb(file);
            if(error!==""){
                message.text = error;
                message.open();
            }
        }
        onRejected: {
            console.log("New Canceled");
        }
    }
    QtLabs.FileDialog {
        id: openFileDialog
        title: qsTr("Please choose a file")
        folder: paths.documents
        fileMode: QtLabs.FileDialog.OpenFile
        options: QtLabs.FileDialog.ReadOnly
        onAccepted: {
            var error = openDb(file);
            if(error!==""){
                message.text = error;
                message.open();
            }
        }
        onRejected: {
            console.log("Open Canceled");
        }
    }

    QtLabs.FileDialog {
        id: saveFileAsDialog
        title: qsTr("Please choose a file")
        folder: paths.documents
        fileMode: QtLabs.FileDialog.SaveFile
        onAccepted: {
            var error = saveDbAs(file);
            if(error!==""){
                message.text = error;
                message.open();
            }
        }
        onRejected: {
            console.log("'Save As' Canceled");
        }
    }

    StackLayout {
        id: stackLayout
        anchors.fill: parent
        currentIndex: tabBar.currentIndex
        transformOrigin: Item.TopLeft

        Keys.onPressed: {
            if ((event.key === Qt.Key_Q) && (event.modifiers & Qt.ControlModifier)){
                Qt.quit();
            }
        }

        ActivityEditor {
            toolBarLayout: currentTabToolBarLayout
            tagsModel: tagSelectorModel
            activityModel: activityViewModel
        }
        TagEditor {
            toolBarLayout: currentTabToolBarLayout
            tagModel: tagViewModel
            superTagModel: tagSelectorModel
        }
        Analytics {
            context: analytics
            durationChartView: DurationChartView{}
        }
    }

    footer: TabBar {
        id: tabBar
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignBottom
        transformOrigin: Item.TopLeft

        TabButton {
            text: "Activities"
        }

        TabButton {
            text: "Tags"
        }

        TabButton {
            text: "Analytics"
        }
    }

}
