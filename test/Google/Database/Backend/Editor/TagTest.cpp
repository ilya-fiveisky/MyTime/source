#include "gtest/gtest.h"

#include <algorithm>
#include <set>
#include <vector>

#include <QSqlRecord>

#include "Database/db_exception.hpp"
#include "Database/SqlExecutor.hpp"
#include "Database/Backend/Editor/Tag.hpp"

#include "../../BasicTest.hpp"
#include "../BasicMethodsTest.hpp"

using namespace std;

using namespace MyTime::Database;
using namespace MyTime::Database::Backend::Editor;

typedef MyTime::Database::Backend::Editor::Tag TagEditorBackend;

namespace MyTime { namespace Test { namespace Database { namespace Backend { namespace Editor {

typedef BasicTest DatabaseBackendEditorTagTest;

TEST_F(DatabaseBackendEditorTagTest, constructor) {
    EXPECT_THROW(TagEditorBackend b(nullptr), invalid_argument);
    EXPECT_NO_THROW(TagEditorBackend b(sqlExecutor()));
}

TEST_F(DatabaseBackendEditorTagTest, updateName) {
    TagEditorBackend tagBackend(sqlExecutor());
    int tagId = 0;
    EXPECT_THROW(tagBackend.updateName(tagId, ""), db_exception);
    sqlExecutor()->setConnectionString(":memory:");
    tagId = tagBackend.create();
    auto result = tagBackend.select("SELECT name FROM tag");
    vector<QSqlRecord> records{begin(result), end(result)};
    ASSERT_TRUE(records[0].value("name").isNull());
    QString name{"some name"};
    ASSERT_NO_THROW(tagBackend.updateName(tagId, name));
    result = tagBackend.select("SELECT name FROM tag");
    records.assign(begin(result), end(result));
    ASSERT_EQ(records[0].value("name").toString(), name);
}

}}}}}
