#include "gtest/gtest.h"

#include <QSqlQueryModel>

#include "Models/QSqlRolesModel.hpp"

using namespace MyTime;

TEST(ModelsQSqlRolesModel, roleNamesCount) {
    Models::QSqlRolesModel<QSqlQueryModel> m;
    int parentRolesCount = m.roleNames().count();
    m.setUserRoleNames({"role1"});
    EXPECT_EQ(m.roleNames().count(), parentRolesCount + 1);
}
