import QtQuick 2.3
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtTest 1.0
import "../../../lib"

Page {
    id: root
    height: 480
    width: 640

    ListModel {
        id: activities

        property int makeNewCount: 0
        property int deleteRowIndex: -1

        function idByRow(row){
            return get(row).id;
        }

        function makeNew(){
            makeNewCount ++;
            return "";
        }

        function deleteRow(index){
            deleteRowIndex = index;
            return "";
        }

        function start(index){
            setProperty(index, "running", true);
            return "";
        }

        function stop(index){
            setProperty(index, "running", false);
            return "";
        }

        ListElement {
            property int id: 1
            description: "A1"
            running: false
            start: "01/01/2016"
            end: "01/01/2016"
            duration: "00:00:10"
        }
        ListElement {
            property int id: 2
            description: "A2"
            running: false
            start: "01/01/2016"
            end: "01/01/2016"
            duration: "00:00:10"
        }
        ListElement {
            property int id: 3
            description: "A3"
            running: false
            start: "01/01/2016"
            end: "01/01/2016"
            duration: "00:00:10"
        }
    }

    ListModel {
        id: tags

        property int makeNewCount: 0
        property int deleteRowIndex: -1

        function idByRow(row){
            return get(row).id;
        }

        function makeNew(){
            makeNewCount ++;
        }

        function deleteRow(index){
            deleteRowIndex = index;
        }

        ListElement {
            property int id: 1
            name: "111"
        }
        ListElement {
            property int id: 2
            name: "222"
        }
        ListElement {
            property int id: 3
            name: "333"
        }
    }

    header: ToolBar {
        RowLayout {
            id: toolBarLayout
        }
    }

    ActivityEditor {
        id: activityEditor
        activityModel: activities
        tagsModel: tags
        toolBarLayout: toolBarLayout
        anchors.fill: parent

        TestCase {
            name: "ActivityEditor Tests"
            when: windowShown

            SignalSpy {
                id: spy
                signalName: "clicked"
            }

            function test_000_props() {
                compare(activityEditor.toolBarLayout, toolBarLayout, "incorrect toolBarLayout");
                compare(activityEditor.tagsModel, tags, "tagModel != tags");
                compare(activityEditor.activityModel, activities, "activityModel != activities");
            }

            function test_001_addActivityButton() {
                var addActivityButton = findChild(toolBarLayout, "addActivityButton");
                verify(addActivityButton, "addActivityButton is null");
                verify(addActivityButton.visible, "addActivityButton is not visible");
                spy.clear();
                spy.target = addActivityButton;
                mouseClick(addActivityButton);
                compare(spy.count, 1, "spy.count != 1");
                compare(activities.makeNewCount, 1, "activities.makeNewCount != 1");
                spy.clear();
            }

            function test_002_activity_deletion() {
                var deleteButton = findChild(parent, "deleteButton1");
                verify(deleteButton, "deleteButton is null");
                verify(deleteButton.visible, "deleteButton is not visible");
                spy.clear();
                spy.target = deleteButton;
                mouseClick(deleteButton);
                compare(spy.count, 1, "spy.count != 1");
                spy.clear();

                var yesButton = findChild(deleteButton, "yesButton");
                verify(yesButton, "yesButton is null");
                verify(yesButton.visible, "yesButton is not visible");
                spy.clear();
                spy.target = yesButton;
                mouseClick(yesButton);
                compare(spy.count, 1, "spy.count != 1");
                compare(activities.deleteRowIndex, 1, "activities.deleteRowIndex != 1");
                spy.clear();
            }

            function test_003_startButton() {
                var index = 1;
                var startButton = findChild(parent, "startButton" + index.toString());
                verify(startButton, "startButton is null");
                verify(startButton.visible, "startButton is not visible");
                spy.clear();
                spy.target = startButton;
                mouseClick(startButton);
                compare(spy.count, 1, "spy.count != 1");
                verify(activities.get(index).running, "activity " + index.toString() + " is not running");
                mouseClick(startButton);
                compare(spy.count, 2, "spy.count != 2");
                verify(!activities.get(index).running, "activity " + index.toString() + " is running");
                spy.clear();
            }

        }
    }
}
