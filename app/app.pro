TEMPLATE = app

QT += core qml quick sql charts widgets

CONFIG += c++14 #object_parallel_to_source

TARGET = MyTime

SOURCES += main.cpp

RESOURCES += qml.qrc

# Default rules for deployment.
include(deployment.pri)

INCLUDEPATH += ../lib

!include (../lib/libs.pri){
    message("libs.pri is not included")
}

unix:LIBS += -lmylib -L../lib
win32:release:LIBS += -lmylib -L../lib/release
win32:debug:LIBS += -lmylib -L../lib/debug

#unix:LIBS += -L../../../breakpad/src/src/client/linux -lbreakpad_client
#win32:LIBS += -L../../../breakpad-windows64-build/release -lbreakpad

#INCLUDEPATH += ../../../breakpad/src/src

DISTFILES +=

