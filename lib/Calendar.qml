import QtQuick 2.5
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.1
import Qt.labs.calendar 1.0

ColumnLayout {
    id: root

    property date selectedDate: new Date

    onSelectedDateChanged: {
        grid.month = selectedDate.getMonth();
        grid.year = selectedDate.getFullYear();
    }

    RowLayout {
        Layout.alignment: Qt.AlignHCenter

        ToolButton {
            text: "<"
            font.bold: true
            Layout.alignment: Qt.AlignLeft
            Layout.fillWidth: true

            onClicked: {
                grid.shiftMonth(-1);
            }
        }

        Label {
            text: grid.title
            Layout.alignment: Qt.AlignHCenter
        }

        ToolButton {
            text: ">"
            font.bold: true
            Layout.alignment: Qt.AlignRight
            Layout.fillWidth: true

            onClicked: {
                grid.shiftMonth(1);
            }
        }

    }

    GridLayout {
        columns: 2

        DayOfWeekRow {
            locale: grid.locale

            Layout.column: 1
            Layout.fillWidth: true

            delegate: CalendarDelegate {
                text: model.shortName
            }
        }

        WeekNumberColumn {
            month: grid.month
            year: grid.year
            locale: grid.locale

            Layout.fillHeight: true

            delegate: CalendarDelegate {
                text: model.weekNumber
            }
        }

        MonthGrid {
            id: grid
            month: selectedDate.getMonth()
            year: selectedDate.getFullYear()
            locale: Qt.locale("en_US")

            Layout.fillWidth: true
            Layout.fillHeight: true

            delegate: CalendarDelegate {
                opacity: model.month === grid.month ? 1 : 0
                text: model.day

                ToolButton {
                    anchors.fill: parent

                    onClicked: {
                        if(parent.opacity){
                            selectedDate = new Date(model.year, model.month, model.day);
                        }
                    }
                }
            }

            function shiftMonth(i){
                var d = new Date(grid.year, grid.month + i);
                grid.year = d.getFullYear();
                grid.month = d.getMonth();
            }
        }
    }
}
