#ifndef GUARD_6A99AF8783E44849A82F06B526E2FBB7
#define GUARD_6A99AF8783E44849A82F06B526E2FBB7

#include <memory>

#include "mylib_global.h"

#include "QModelBase.hpp"

class QSqlRecord;

namespace MyTime {

namespace Database { namespace Backend { namespace Editor {
    class Basic;
    class Tag;
}}}

namespace Models {

class MYLIB_EXPORT QTagModel: public QModelBase
{
    Q_OBJECT

    typedef QModelBase BaseT;

public:
    explicit QTagModel(const std::shared_ptr<Database::Backend::Editor::Tag> &dbBackend,
                       QObject *parent = Q_NULLPTR);

    QVariant data(const QModelIndex &index, int role) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;

    enum RoleIndex {id, name, supertags};

public slots:
    QString nameById(const int &id) const;

protected:
    std::shared_ptr<QSqlRecord>
                createRecord() const override;

    std::shared_ptr<Database::Backend::Editor::Basic>
                dbBackend() const override;

    QModelBase *tagModel() override;

private:
    std::shared_ptr<QSqlRecord> createRecord(const QSqlRecord &record) override;
    void updateName(const int &row, const QString &name);

    static const QString QUERY;
    static const std::vector<QString> roles_;

    std::shared_ptr<Database::Backend::Editor::Tag>
                            dbBackend_;
};

}}
#endif // GUARD_6A99AF8783E44849A82F06B526E2FBB7
