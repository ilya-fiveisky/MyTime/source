#include "Basic.hpp"

#include <algorithm>
#include <exception>

#include <boost/format.hpp>

#include <QSqlQuery>

#include "../../SqlExecutor.hpp"

using namespace std;

namespace MyTime { namespace Database { namespace Backend { namespace Editor {

Basic::Basic(const std::shared_ptr<SqlExecutor> &sqlExecutor):
    Backend::Basic{sqlExecutor}
{
}

int Basic::create()
{
    checkConnection();
    return sqlExecutor()->execute("INSERT INTO " + mainName() + " DEFAULT VALUES").lastInsertId().toInt();
}

void Basic::remove(const int &id)
{
    checkConnection();
    sqlExecutor()->executeInTransaction(getDeleteCommands(id));
}

void Basic::updateTags(const int &id, const QList<int> &tagIds)
{
    checkConnection();
    string deleteCommand = (boost::format("DELETE FROM %1%_tag WHERE %1%_id = %2%") % mainName() % id).str();

    string tagsString = tagIds.length() == 0 ? "" : accumulate(
                                                   std::next(tagIds.begin()),
                                                   tagIds.end(),
                                                   to_string(tagIds[0]),
            [](const string &a, const int &b) {
        return a + "," + to_string(b);
    });

    string insertCommand =
            (boost::format("INSERT INTO %3%_tag (%3%_id, %4%_id) SELECT %1%, tag.id FROM tag WHERE tag.id IN (%2%)")
             % id
             % tagsString
             % mainName()
             % tagName()
             ).str();

    sqlExecutor()->executeInTransaction({deleteCommand, insertCommand});
}

std::vector<std::string> Basic::getDeleteCommands(const int &id) const
{
    auto idStr = to_string(id);
    return {
        "DELETE FROM activity_tag WHERE " + mainName() + "_id = " + idStr,
                "DELETE FROM " + mainName() + " WHERE id = " + idStr
    };
}

}}}}
