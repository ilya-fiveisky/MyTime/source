#ifndef GUARD_91CC31F9CAC5404AA889D4613485D497
#define GUARD_91CC31F9CAC5404AA889D4613485D497

#include "gtest/gtest.h"

#include <boost/filesystem.hpp>

#include "../BasicTest.hpp"

namespace MyTime { namespace Test {namespace Database {

class BasicTest : public MyTime::Test::BasicTest {
protected:
    BasicTest() = default;
    explicit BasicTest(const boost::filesystem::path &dbName);

    virtual void SetUp() override;

    virtual void TearDown() override;

    boost::filesystem::path dbPath() const;

    void checkDb(const boost::filesystem::path &dbPath);

    void checkDb();

    const boost::filesystem::path TEMP_TEST_DIR{
        boost::filesystem::temp_directory_path() /= boost::filesystem::unique_path()};

    const boost::filesystem::path DB_NAME{boost::filesystem::unique_path()};
};

}}}

#endif // GUARD_91CC31F9CAC5404AA889D4613485D497
