import QtQuick 2.5
import QtQuick.Controls 2.0

Label {
    font: grid.font
    horizontalAlignment: Text.AlignHCenter
    verticalAlignment: Text.AlignVCenter
}
