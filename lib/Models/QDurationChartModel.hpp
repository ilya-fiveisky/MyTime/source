#ifndef GUARD_2878DCD864244259A897AAD9EF84D104
#define GUARD_2878DCD864244259A897AAD9EF84D104

#include <map>
#include <memory>

#include <QAbstractTableModel>
#include <QDate>

#include "mylib_global.h"

namespace MyTime {

namespace Database { namespace Backend {
    class Basic;
}}

namespace Models {

class QDurationChartModel: public QAbstractTableModel
{
    Q_OBJECT
    typedef QAbstractTableModel BaseT;
public:
    explicit QDurationChartModel(const std::shared_ptr<Database::Backend::Basic> &dbBackend,
                                 QObject *parent = Q_NULLPTR);

    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    void refresh(const QString &keywords, const QList<int> &tags, const QDateTime &from, const QDateTime &to);
    void refresh();

private:
    std::shared_ptr<Database::Backend::Basic> dbBackend_;
    std::map<QDate, double> durationMap_;

    static const QString CHART_QUERY;
};
}}
#endif // GUARD_2878DCD864244259A897AAD9EF84D104
