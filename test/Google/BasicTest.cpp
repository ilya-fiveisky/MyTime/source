#include "BasicTest.hpp"

using namespace std;

namespace MyTime { namespace Test {

shared_ptr<Database::SqlExecutor> BasicTest::sqlExecutor()
{
    // This is done in order to not create SqlExecutor in the SqlExecutorTest before constructor tests.
    if(!sqlExecutor_){
        sqlExecutor_ = make_shared<Database::SqlExecutor>();
    }
    return sqlExecutor_;
}

}}
