#ifndef GUARD_DB1F834AFEDA4A08BFF7D1EADB57CDB3
#define GUARD_DB1F834AFEDA4A08BFF7D1EADB57CDB3

#include <memory>

#include <boost/filesystem.hpp>

#include <QSettings>

#include "gtest/gtest.h"

#include "BasicTest.hpp"

namespace MyTime { namespace Test {namespace Database {

class StoreBasicTest : public MyTime::Test::Database::BasicTest {
protected:
    using MyTime::Test::Database::BasicTest::BasicTest;

    virtual void TearDown() override;

    void checkBackup(const boost::filesystem::path &dbPath);

    void checkBackup();

    const std::shared_ptr<QSettings> SETTINGS {
        std::make_shared<QSettings>(QString::fromStdString(
                                   (boost::filesystem::path(TEMP_TEST_DIR) /= boost::filesystem::unique_path()).string()),
                               QSettings::IniFormat) };
};

}}}
#endif // GUARD_DB1F834AFEDA4A08BFF7D1EADB57CDB3
