#ifndef MYENVIRONMENT_HPP
#define MYENVIRONMENT_HPP

#include "gtest/gtest.h"

class MyEnvironment : public ::testing::Environment
{
public:
    MyEnvironment();

    void SetUp() override;
    void TearDown() override;
};

#endif // MYENVIRONMENT_HPP
