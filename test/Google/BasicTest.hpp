#ifndef GUARD_BCC4FF3A1EE54B5C8F9A759C3049E877
#define GUARD_BCC4FF3A1EE54B5C8F9A759C3049E877

#include <memory>

#include "gtest/gtest.h"

#include "Database/SqlExecutor.hpp"

namespace MyTime { namespace Test {

class BasicTest : public ::testing::Test {
protected:
    BasicTest() = default;
    virtual ~BasicTest() = default;

    std::shared_ptr<MyTime::Database::SqlExecutor> sqlExecutor();

private:
    std::shared_ptr<MyTime::Database::SqlExecutor> sqlExecutor_;
};

}}
#endif // GUARD_BCC4FF3A1EE54B5C8F9A759C3049E877
