#ifndef GUARD_BCE0B7A4520A4F77BC4E1BB88FC3382D
#define GUARD_BCE0B7A4520A4F77BC4E1BB88FC3382D

#include <memory>
#include <vector>

#include <QAbstractTableModel>

class QSqlRecord;

namespace MyTime {

namespace Database { namespace Backend { namespace Editor {
    class Basic;
}}}

namespace Models {

class QModelBase: public QAbstractTableModel
{
    Q_OBJECT

    typedef QAbstractTableModel BaseT;

public:
    explicit QModelBase(QString query,
                        const int &idIndex, const int &tagsIndex,
                        QObject *parent = Q_NULLPTR);

    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;

public slots:
    QString deleteRow(const int &row);
    int     idByRow(const int &row) const;
    QString makeNew();
    QString refresh();

protected:
    int     columnFromRole(const int &role) const;

    virtual std::shared_ptr<QSqlRecord>
            createRecord() const = 0;

    virtual std::shared_ptr<QSqlRecord>
            createRecord(const QSqlRecord &r) = 0;

    virtual std::shared_ptr<Database::Backend::Editor::Basic>
            dbBackend() const = 0;

    void    setRecordTags(const QSqlRecord &record, const std::shared_ptr<QSqlRecord> &r);
    void    setRecordTags(const QStringList &supertagIdStrs, const std::shared_ptr<QSqlRecord> &r);

    void setUserRoleNames(const std::vector<QString> &roleNames);

    virtual QModelBase*
            tagModel() = 0;

    void    updateTags(const int &row, const QVariant &value);

    int         idIndex_;
    std::vector<std::shared_ptr<QSqlRecord>>
                records_;
    int         tagsIndex_;

private:
    QString                 query_;
    std::vector<QString>    userRoleNames_;
};

}}
#endif // GUARD_BCE0B7A4520A4F77BC4E1BB88FC3382D
