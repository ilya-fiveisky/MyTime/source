#ifndef GUARD_73B3B86F472444AAA0A40BBA6FEF224C
#define GUARD_73B3B86F472444AAA0A40BBA6FEF224C

#include <boost/log/support/exception.hpp>
#include <boost/throw_exception.hpp>

namespace MyTime {

template<typename ExT> void my_throw(const std::string &what){
    BOOST_THROW_EXCEPTION(boost::enable_error_info(ExT(what)) << boost::log::current_scope());
}

}
#endif // GUARD_73B3B86F472444AAA0A40BBA6FEF224C
