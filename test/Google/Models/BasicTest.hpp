#ifndef GUARD_547884ECA7024EC9ADD3C215757C693E
#define GUARD_547884ECA7024EC9ADD3C215757C693E

#include <memory>

#include "gtest/gtest.h"

#include <QSqlDatabase>

#include "Database/Backend/Basic.hpp"
#include "Database/Backend/Editor/Tag.hpp"
#include "Database/SqlExecutor.hpp"
#include "Models/QTagModel.hpp"

#include "../BasicTest.hpp"

namespace MyTime { namespace Test { namespace Models {

class BasicTest : public MyTime::Test::BasicTest {
protected:
    BasicTest();

    std::shared_ptr<MyTime::Database::Backend::Editor::Tag> tagDbBackend() const;
    std::shared_ptr<MyTime::Models::QTagModel> tags() const;

private:
    std::shared_ptr<MyTime::Database::Backend::Editor::Tag> tagDbBackend_;
    std::shared_ptr<MyTime::Models::QTagModel> tags_;
};

}}}
#endif // GUARD_547884ECA7024EC9ADD3C215757C693E
