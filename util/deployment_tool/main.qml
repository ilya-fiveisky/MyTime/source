import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import Qt.labs.platform 1.0 as QtLabs

ApplicationWindow {
    id: appWindow
    visible: true
    width: 640
    height: 480
    title: qsTr("Deployment Tool")

    QtLabs.FileDialog {
        id: saveConfigurationFileDialog
        title: qsTr("Please choose executable file")
        fileMode: QtLabs.FileDialog.SaveFile
        onAccepted: {
            saveConfiguration(file, librarySources.text, folderSources.text, executableFile.text, targetFolder.text);
        }
    }

    QtLabs.FileDialog {
        id: loadConfigurationFileDialog
        title: qsTr("Please choose configuration file")
        fileMode: QtLabs.FileDialog.OpenFile
        options: QtLabs.FileDialog.ReadOnly
        onAccepted: {
            var cfg = loadConfiguration(file);
            librarySources.text = cfg[0];
            folderSources.text = cfg[1];
            executableFile.text = cfg[2];
            targetFolder.text = cfg[3];
        }
    }

    header: ToolBar {
        RowLayout {
            ToolButton {
                Image {
                    anchors.fill: parent
                    source: "icons/ic_save_black_48px.svg"
                }
                onClicked: saveConfigurationFileDialog.open()
            }
            ToolButton {
                Image {
                    anchors.fill: parent
                    source: "icons/open.svg"
                }
                onClicked: loadConfigurationFileDialog.open()
            }
        }
    }

    QtLabs.FolderDialog {
        id: librarySourcesDialog
        title: qsTr("Please choose folder with libraries")
        onAccepted: {
            librarySources.text += folder + "; "
        }
    }

    QtLabs.FolderDialog {
        id: folderSourcesDialog
        title: qsTr("Please choose folder with folders")
        onAccepted: {
            folderSources.text += folder + "; "
        }
    }

    QtLabs.FileDialog {
        id: executableFileDialog
        title: qsTr("Please choose executable file")
        fileMode: QtLabs.FileDialog.OpenFile
        options: QtLabs.FileDialog.ReadOnly
        onAccepted: {
            executableFile.text = file;
        }
    }

    QtLabs.FolderDialog {
        id: targetFolderDialog
        title: qsTr("Please choose folder where to deploy to")
        onAccepted: {
            targetFolder.text = folder
        }
    }


    ColumnLayout {
        GridLayout {
            columns: 3

            Label {
                text: qsTr("Library Sources") + ": "
            }
            TextArea {
                id: librarySources
                Layout.preferredWidth: appWindow.width / 2
                Layout.fillWidth: true
                wrapMode: TextEdit.Wrap
                background: Rectangle {border.width: 1}
            }
            RowLayout {
            Button {
                text: qsTr("Add")
                onClicked: librarySourcesDialog.open()
            }
            CheckBox {
                text: qsTr("Debug")
                id: isDebug
            }
            }

            Label {
                text: qsTr("Folder Sources") + ": "
            }
            TextArea {
                id: folderSources
                Layout.preferredWidth: appWindow.width / 2
                Layout.fillWidth: true
                wrapMode: TextEdit.Wrap
                background: Rectangle {border.width: 1}
            }
            Button {
                text: qsTr("Add")
                onClicked: folderSourcesDialog.open()
            }

            Label {
                text: qsTr("Executable File") + ": "
            }
            TextArea {
                id: executableFile
                Layout.preferredWidth: appWindow.width / 2
                Layout.fillWidth: true
                wrapMode: TextEdit.Wrap
                background: Rectangle {border.width: 1}
            }
            Button {
                text: qsTr("Select")
                onClicked: executableFileDialog.open()
            }

            Label {
                text: qsTr("Target Folder") + ": "
            }
            TextArea {
                id: targetFolder
                Layout.preferredWidth: appWindow.width / 2
                Layout.fillWidth: true
                wrapMode: TextEdit.Wrap
                background: Rectangle {border.width: 1}
            }
            Button {
                text: qsTr("Select")
                onClicked: targetFolderDialog.open()
            }
        }

        Button {
            text: qsTr("Deploy")
            onClicked: {
                deploy(librarySources.text, isDebug.checked, folderSources.text, executableFile.text, targetFolder.text);
                toolTip.visible = true;
            }
            ToolTip {
                id: toolTip
                text: qsTr("Deployed succesfully")
                timeout: 3000
            }
        }
    }
}
