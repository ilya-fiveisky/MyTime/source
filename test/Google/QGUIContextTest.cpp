#include "gtest/gtest.h"

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

#include <QSettings>

#include "Database/Backend/Basic.hpp"
#include "Database/Backend/Editor/Activity.hpp"
#include "Database/Backend/Editor/Tag.hpp"
#include "Database/db_exception.hpp"
#include "Database/SqlExecutor.hpp"
#include "Database/Store.hpp"
#include "Database/StoreImpl.hpp"
#include "Models/QActivityModel.hpp"
#include "Models/QTagModel.hpp"
#include "QAnalyticsContext.hpp"

#include "QGUIContext.hpp"

#include "Database/StoreBasicTest.hpp"
#include "Database/StoreMethodsTest.hpp"

using namespace std;
using namespace boost::filesystem;

using namespace MyTime;
using namespace MyTime::Database;
using namespace MyTime::Models;

namespace MyTime { namespace Test {

typedef Database::StoreBasicTest QGUIContextTest;

TEST_F(QGUIContextTest, constructor) {
    sqlExecutor()->setConnectionString(":memory:");
    auto store = make_shared<MyTime::Database::Store>(sqlExecutor());
    auto dbBackend = make_shared<MyTime::Database::Backend::Basic>(sqlExecutor());
    auto tagDbBackend = make_shared<MyTime::Database::Backend::Editor::Tag>(sqlExecutor());
    auto tagViewModel = make_shared<QTagModel>(tagDbBackend);
    auto activityDbBackend = make_shared<MyTime::Database::Backend::Editor::Activity>(sqlExecutor());
    auto activityViewModel = make_shared<QActivityModel>(tagViewModel, activityDbBackend, nullptr);
    auto analytics = make_shared<QAnalyticsContext>(tagViewModel, dbBackend);

    EXPECT_THROW(QGUIContext c(nullptr, activityViewModel,  analytics,  tagViewModel),  invalid_argument);
    EXPECT_THROW(QGUIContext c(store,   nullptr,            analytics,  tagViewModel),  invalid_argument);
    EXPECT_THROW(QGUIContext c(store,   activityViewModel,  nullptr,    tagViewModel),  invalid_argument);
    EXPECT_THROW(QGUIContext c(store,   activityViewModel,  analytics,  nullptr     ),  invalid_argument);

    EXPECT_NO_THROW(QGUIContext c(store,   activityViewModel,  analytics,  tagViewModel));
}

class QGUIContextMethodsTest : public Database::StoreMethodsTest {
protected:
    using StoreMethodsTest::StoreMethodsTest;

    virtual void SetUp() override
    {
        StoreMethodsTest::SetUp();

        store()->open();
        auto dbBackend = make_shared<Backend::Basic>(sqlExecutor());
        auto tagDbBackend = make_shared<Backend::Editor::Tag>(sqlExecutor());
        auto tagViewModel = make_shared<QTagModel>(tagDbBackend);
        auto activityDbBackend = make_shared<Backend::Editor::Activity>(sqlExecutor());
        auto activityViewModel = make_shared<QActivityModel>(tagViewModel, activityDbBackend, nullptr);
        auto analytics = make_shared<QAnalyticsContext>(tagViewModel, dbBackend);
        guiContext_ =  make_shared<QGUIContext>(store(), activityViewModel, analytics, tagViewModel);
    }

    shared_ptr<QGUIContext> guiContext() const
    {
        return guiContext_;
    }

private:
    shared_ptr<QGUIContext> guiContext_;
};

TEST_F(QGUIContextMethodsTest, properties) {
    EXPECT_TRUE(guiContext()->get_activityViewModel());
    EXPECT_TRUE(guiContext()->get_analytics());
    EXPECT_EQ(  guiContext()->get_dbPath().toStdString(),
                (path(TEMP_TEST_DIR) /= StoreImpl::NEW_DB_NAME_DEFAULT).string());
    EXPECT_TRUE(guiContext()->get_tagSelectorModel());
    EXPECT_TRUE(guiContext()->get_tagViewModel());
}

TEST_F(QGUIContextMethodsTest, newDb) {
    path tmp{path(TEMP_TEST_DIR) /= unique_path()};
    EXPECT_FALSE(exists(tmp));
    QUrl url{ QUrl::fromLocalFile(QString::fromStdString(tmp.string())) };
    boost::filesystem::ofstream f{tmp};
    EXPECT_TRUE(exists(tmp));
    EXPECT_FALSE(guiContext()->newDb(url).isEmpty());
    QString error;
    EXPECT_NO_THROW(error = guiContext()->newDb(QUrl::fromLocalFile(QString::fromStdString(dbPath().string()))));
    EXPECT_TRUE(error.isEmpty()) << error.toStdString();
    checkDb();
    checkSettings();
}

TEST_F(QGUIContextMethodsTest, openDb) {
    sqlExecutor()->setConnectionString(dbPath().string());
    path tmp{path(TEMP_TEST_DIR) /= unique_path()};
    EXPECT_FALSE(exists(tmp));
    QUrl url{ QUrl::fromLocalFile(QString::fromStdString(tmp.string())) };
    EXPECT_FALSE(guiContext()->openDb(url).isEmpty());
    QString error;
    EXPECT_NO_THROW(error = guiContext()->openDb(QUrl::fromLocalFile(QString::fromStdString(dbPath().string()))));
    EXPECT_TRUE(error.isEmpty()) << error.toStdString();
    checkBackup();
    checkSettings();
}


TEST_F(QGUIContextMethodsTest, saveDbAs) {
    path tmp{path(TEMP_TEST_DIR) /= unique_path()};
    EXPECT_FALSE(exists(tmp));
    QUrl url{ QUrl::fromLocalFile(QString::fromStdString(tmp.string())) };
    EXPECT_TRUE(remove(store()->path().toStdString()));
    EXPECT_FALSE(guiContext()->saveDbAs(url).isEmpty());
    auto newDbPath = path(TEMP_TEST_DIR) /= unique_path();
    sqlExecutor()->setConnectionString(dbPath().string());
    QString error;
    EXPECT_NO_THROW(error = guiContext()->saveDbAs(QUrl::fromLocalFile(QString::fromStdString(newDbPath.string()))));
    EXPECT_TRUE(error.isEmpty()) << error.toStdString();
    checkDb(newDbPath);
    checkBackup(newDbPath);
    checkSettings(newDbPath);
}

}}
