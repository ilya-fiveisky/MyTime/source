#ifndef GUARD_4B28F796B74244A095188E1F6F70672B
#define GUARD_4B28F796B74244A095188E1F6F70672B

#include "gtest/gtest.h"

#include <memory>

#include "Database/Backend/Editor/Activity.hpp"
#include "Models/BasicTest.hpp"

namespace MyTime { namespace Test { namespace Models {

class QActivityModelBasicTest : public Models::BasicTest {
protected:
    QActivityModelBasicTest();

    std::shared_ptr<MyTime::Database::Backend::Editor::Activity> activityDbBackend() const;

    void invalidateDbConnection();

private:
    std::shared_ptr<MyTime::Database::Backend::Editor::Activity> activityDbBackend_;
};

}}}
#endif // GUARD_4B28F796B74244A095188E1F6F70672B
