#include "gtest/gtest.h"

#include <algorithm>
#include <set>
#include <vector>

#include <QSqlRecord>

#include "Database/db_exception.hpp"
#include "Database/SqlExecutor.hpp"
#include "Database/Backend/Editor/Activity.hpp"

#include "../../BasicTest.hpp"
#include "../BasicMethodsTest.hpp"

using namespace std;

using namespace MyTime::Database;
using namespace MyTime::Database::Backend::Editor;

typedef MyTime::Database::Backend::Editor::Activity ActivityEditorBackend;

namespace MyTime { namespace Test { namespace Database { namespace Backend { namespace Editor {

typedef BasicTest DatabaseBackendEditorActivityTest;

TEST_F(DatabaseBackendEditorActivityTest, constructor) {
    EXPECT_THROW(ActivityEditorBackend b(nullptr), invalid_argument);
    EXPECT_NO_THROW(ActivityEditorBackend b(sqlExecutor()));
}

TEST_F(DatabaseBackendEditorActivityTest, start) {
    ActivityEditorBackend activityBackend(sqlExecutor());
    int activityId = 1;
    EXPECT_THROW(activityBackend.start(activityId), db_exception);
    sqlExecutor()->setConnectionString(":memory:");
    auto result = activityBackend.select("SELECT * FROM interval");
    vector<QSqlRecord> records{begin(result), end(result)};
    ASSERT_EQ(records.size(), 0);
    ASSERT_NO_THROW(activityBackend.start(activityId));
    result = activityBackend.select("SELECT start, end FROM interval");
    records.assign(begin(result), end(result));
    ASSERT_EQ(records.size(), 1);
    ASSERT_FALSE(records[0].value("start").isNull());
    ASSERT_TRUE(records[0].value("end").isNull());
}

TEST_F(DatabaseBackendEditorActivityTest, stop) {
    ActivityEditorBackend activityBackend(sqlExecutor());
    int activityId = 1;
    EXPECT_THROW(activityBackend.stop(activityId), db_exception);
    sqlExecutor()->setConnectionString(":memory:");
    activityBackend.start(activityId);
    auto result = activityBackend.select("SELECT end FROM interval");
    vector<QSqlRecord> records{begin(result), end(result)};
    ASSERT_EQ(records.size(), 1);
    ASSERT_TRUE(records[0].value("end").isNull());
    ASSERT_NO_THROW(activityBackend.stop(activityId));
    result = activityBackend.select("SELECT end FROM interval");
    records.assign(begin(result), end(result));
    ASSERT_EQ(records.size(), 1);
    ASSERT_FALSE(records[0].value("end").isNull());
}

TEST_F(DatabaseBackendEditorActivityTest, updateDescription) {
    ActivityEditorBackend activityBackend(sqlExecutor());
    int activityId = 1;
    EXPECT_THROW(activityBackend.updateDescription(activityId, ""), db_exception);
    sqlExecutor()->setConnectionString(":memory:");
    activityId = activityBackend.create();
    auto result = activityBackend.select("SELECT description FROM activity");
    vector<QSqlRecord> records{begin(result), end(result)};
    ASSERT_TRUE(records[0].value("description").isNull());
    QString description{"some description"};
    ASSERT_NO_THROW(activityBackend.updateDescription(activityId, description));
    result = activityBackend.select("SELECT description FROM activity");
    records.assign(begin(result), end(result));
    ASSERT_EQ(records[0].value("description").toString(), description);
}

}}}}}
