#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QDirIterator>
#include <QFile>
#include <QFileInfo>
#include <QResource>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QString qmlDirPath = "/data/data/org.qtproject.example.tst_mytime_quick_test/files/";

    QDir d(qmlDirPath);
    if(!d.exists()){
        qDebug() << "mkpath: " << d.mkpath(qmlDirPath);
    }

    QDirIterator qrcIt(":/qml");
    while (qrcIt.hasNext()) {
        QString fromPath = qrcIt.next();
        QString toPath = qmlDirPath + qrcIt.fileName();
        qDebug() << "from: " << fromPath;
        qDebug() << "to: " << toPath;
        qDebug() << "copy: " << QFile::copy(fromPath, toPath);
    }

    QDirIterator dirIt(qmlDirPath);
    while (dirIt.hasNext()) {
        qDebug() << dirIt.next();
    }

    return a.exec();
}
