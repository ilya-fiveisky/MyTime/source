#include "BasicTest.hpp"

using namespace std;
using namespace boost::filesystem;

namespace MyTime { namespace Test {namespace Database {

BasicTest::BasicTest(const path &dbName): DB_NAME{dbName}
{
}

void BasicTest::SetUp()
{
    MyTime::Test::BasicTest::SetUp();
    ASSERT_TRUE(create_directory(TEMP_TEST_DIR)) << "Temp directory " << TEMP_TEST_DIR << " was not created.";
}

void BasicTest::TearDown()
{
    MyTime::Test::BasicTest::TearDown();
    remove_all(TEMP_TEST_DIR);
    EXPECT_FALSE(exists(TEMP_TEST_DIR)) << "Temp directory " << TEMP_TEST_DIR << " still exists.";
}

path BasicTest::dbPath() const
{
    return path(TEMP_TEST_DIR) /= DB_NAME;
}

void BasicTest::checkDb(const path &dbPath)
{
    EXPECT_TRUE(exists(dbPath)) << "DB file " << dbPath << " does not exist.";
}

void BasicTest::checkDb()
{
    checkDb(dbPath());
}


}}}
