#include "gtest/gtest.h"

#include <exception>
#include <memory>

#include <QSignalSpy>
#include <QSqlDatabase>
#include <QTimeZone>

#include "Database/Backend/Basic.hpp"
#include "Database/Backend/Editor/Activity.hpp"
#include "Database/db_exception.hpp"
#include "Database/SqlExecutor.hpp"
#include "Models/QDurationChartModel.hpp"

#include "Models/QActivityModelBasicTest.hpp"

using namespace std;

using namespace MyTime;
using namespace MyTime::Database;
using namespace MyTime::Models;

namespace MyTime { namespace Test { namespace Models {

using namespace Models;

class ModelsQDurationChartModelBasicTest : public Models::QActivityModelBasicTest {
protected:
    ModelsQDurationChartModelBasicTest()
    {
        dbBackend_ = make_shared<Database::Backend::Basic>(sqlExecutor());
    }

    shared_ptr<Database::Backend::Basic> dbBackend() const
    {
        return dbBackend_;
    }

private:
    shared_ptr<Database::Backend::Basic> dbBackend_;
};

class ModelsQDurationChartModelMethodsTest : public ModelsQDurationChartModelBasicTest {
protected:
    ModelsQDurationChartModelMethodsTest()
    {
        chartModel_ = make_shared<QDurationChartModel>(dbBackend());
    }

    shared_ptr<QDurationChartModel> chartModel() const
    {
        return chartModel_;
    }

private:
    shared_ptr<QDurationChartModel> chartModel_;
};

typedef ModelsQDurationChartModelBasicTest ModelsQDurationChartModelConstructorTest;

TEST_F(ModelsQDurationChartModelConstructorTest, test) {
    EXPECT_THROW(QDurationChartModel m(nullptr), invalid_argument);

    EXPECT_NO_THROW(QDurationChartModel m(dbBackend()));
}

TEST_F(ModelsQDurationChartModelMethodsTest, columnCount) {
    EXPECT_EQ(chartModel()->columnCount(), 2);
}

TEST_F(ModelsQDurationChartModelMethodsTest, rowCount) {
    EXPECT_EQ(chartModel()->rowCount(), 0);
    int id = activityDbBackend()->create();
    activityDbBackend()->start(id);
    activityDbBackend()->stop(id);
    chartModel()->refresh();
    EXPECT_EQ(chartModel()->rowCount(), 1);
}

TEST_F(ModelsQDurationChartModelMethodsTest, data) {
    int id = activityDbBackend()->create();
    activityDbBackend()->start(id);
    activityDbBackend()->stop(id);
    chartModel()->refresh();
    EXPECT_FALSE(chartModel()->data(QModelIndex()).isValid());
    EXPECT_FALSE(chartModel()->data(chartModel()->index(1, 0)).isValid());
    EXPECT_FALSE(chartModel()->data(chartModel()->index(0, 2)).isValid());
    QDate date = chartModel()->data(chartModel()->index(0, 0)).toDate();
    EXPECT_EQ(date, QDate::currentDate()) << date.toString().toStdString();
    EXPECT_EQ(chartModel()->data(chartModel()->index(0, 1)).toDouble(), 0);
}

TEST_F(ModelsQDurationChartModelMethodsTest, refreshException) {
    invalidateDbConnection();
    QSignalSpy spy(chartModel().get(), SIGNAL(modelReset()));
    EXPECT_EQ(spy.count(), 0);
    ASSERT_THROW(chartModel()->refresh(), db_exception);
    EXPECT_EQ(spy.count(), 1);
}

TEST_F(ModelsQDurationChartModelMethodsTest, refresh) {
    EXPECT_EQ(chartModel()->rowCount(), 0);
    int id = activityDbBackend()->create();
    activityDbBackend()->start(id);
    activityDbBackend()->stop(id);
    EXPECT_EQ(chartModel()->rowCount(), 0);
    QSignalSpy spy(chartModel().get(), SIGNAL(modelReset()));
    EXPECT_EQ(spy.count(), 0);
    chartModel()->refresh();
    EXPECT_EQ(spy.count(), 1);
    EXPECT_EQ(chartModel()->rowCount(), 1);
}

TEST_F(ModelsQDurationChartModelMethodsTest, localTimeGrouping) {
//--------- Filling DB ---------------
    activityDbBackend()->create();
    activityDbBackend()->create();
    activityDbBackend()->create();
    activityDbBackend()->create();
    // Creating time intervals near midnight for local time (activities 1 and 2)
    // and for UTC (activities 3 and 4).
    sqlExecutor()->executeInTransaction
            ({
                 "INSERT INTO interval (activity_id, start, end) "
                 "VALUES (1, strftime('%s', '2017-01-02T23:59:59', 'utc'), strftime('%s', '2017-01-03T00:00:01', 'utc'))",
                 "INSERT INTO interval (activity_id, start, end) "
                 "VALUES (2, strftime('%s', '2017-01-03T00:00:01', 'utc'), strftime('%s', '2017-01-03T00:00:02', 'utc'))",
                 "INSERT INTO interval (activity_id, start, end) "
                 "VALUES (3, strftime('%s', '2017-01-12T23:59:59'), strftime('%s', '2017-01-13T00:00:01'))",
                 "INSERT INTO interval (activity_id, start, end) "
                 "VALUES (4, strftime('%s', '2017-01-13T00:00:01'), strftime('%s', '2017-01-13T00:00:02'))"
             });
//---------- End of filling DB ------------------------------

    QDateTime from{QDateTime::fromString("2017-01-01T00:00:01", Qt::ISODate)};
    from.setTimeSpec(Qt::UTC);
    QDateTime to{QDateTime::fromString("2017-01-05T00:00:01", Qt::ISODate)};
    to.setTimeSpec(Qt::UTC);
    chartModel()->refresh("", {}, from, to);
    EXPECT_EQ(chartModel()->rowCount(), 2);

    QDateTime from2{QDateTime::fromString("2017-01-05T00:00:01", Qt::ISODate)};
    from2.setTimeSpec(Qt::UTC);
    QDateTime to2{QDateTime::fromString("2017-01-15T00:00:01", Qt::ISODate)};
    to2.setTimeSpec(Qt::UTC);
    chartModel()->refresh("", {}, from2, to2);
    bool isUtc = QDateTime::currentDateTime().timeZone() == QTimeZone::utc();
    EXPECT_EQ(chartModel()->rowCount(), isUtc ? 2 : 1);
}

}}}
