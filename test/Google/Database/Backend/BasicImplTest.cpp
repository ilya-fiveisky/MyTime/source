#include "gtest/gtest.h"

#include <algorithm>
#include <set>
#include <vector>

#include <QSqlRecord>

#include "Database/db_exception.hpp"
#include "Database/SqlExecutor.hpp"
#include "Database/Backend/BasicImpl.hpp"

#include "../BasicTest.hpp"
#include "BasicMethodsTest.hpp"

using namespace std;

using namespace MyTime;
using namespace MyTime::Database;

typedef MyTime::Database::Backend::BasicImpl BasicBackendImpl;

namespace MyTime { namespace Test { namespace Database { namespace Backend {

typedef BasicTest DatabaseBackendBasicImplTest;

TEST_F(DatabaseBackendBasicImplTest, constructorExceptions) {
    EXPECT_THROW(BasicBackendImpl b(nullptr), invalid_argument);
}

TEST_F(DatabaseBackendBasicImplTest, constructor) {
    EXPECT_NO_THROW(BasicBackendImpl b(sqlExecutor()));
}

typedef BasicMethodsTest<BasicBackendImpl> DatabaseBackendBasicImplMethodsTest;

TEST_F(DatabaseBackendBasicImplMethodsTest, constructFilteredJoinString) {
    QString query;
    QDateTime from{QDateTime::currentDateTime().addYears(-1)};
    QDateTime to{QDateTime::currentDateTime().addYears(1)};
    ASSERT_NO_THROW(query = backend()->constructFilteredJoinString("", boost::none, from, to));
    ASSERT_FALSE(query.isEmpty());
    ASSERT_TRUE(query.startsWith("SELECT "));
    ASSERT_FALSE(query.contains(" LIKE "));
    ASSERT_FALSE(query.contains(" IN "));
    QString keys{"key1 key2"};
    ASSERT_THROW(query = backend()->constructFilteredJoinString(keys, 1, from, to), logic_error);
    sqlExecutor()->setConnectionString(":memory:");
    ASSERT_NO_THROW(backend()->initializeDb());
    ASSERT_NO_THROW(query = backend()->constructFilteredJoinString(keys, 1, from, to));
    ASSERT_FALSE(query.isEmpty());
    ASSERT_TRUE(query.startsWith("SELECT "));
    ASSERT_EQ(query.count(" LIKE "), 2);
    ASSERT_EQ(query.count(" IN "), 1);
}

TEST_F(DatabaseBackendBasicImplMethodsTest, initializeDb) {
    sqlExecutor()->setConnectionString(":memory:");
    ASSERT_NO_THROW(backend()->initializeDb());
    set<string> tables{"activity", "interval", "tag", "activity_tag", "tag_tag"};
    auto executorTables = sqlExecutor()->tables();
    EXPECT_TRUE(includes(executorTables.begin(), executorTables.end(), tables.begin(), tables.end()))
            << "Tables are: "
            << accumulate(executorTables.begin(), executorTables.end(), string(""),
                          [](const string &a, const string &b){return a + " " + b;});
}

TEST_F(DatabaseBackendBasicImplMethodsTest, createEmptyDb) {
    sqlExecutor()->setConnectionString(":memory:");
    ASSERT_NO_THROW(backend()->createEmptyDb());
    set<string> tables{"activity", "interval", "tag", "activity_tag", "tag_tag"};
    auto executorTables = sqlExecutor()->tables();
    EXPECT_TRUE(includes(executorTables.begin(), executorTables.end(), tables.begin(), tables.end()))
            << "Tables are: "
            << accumulate(executorTables.begin(), executorTables.end(), string(""),
                          [](const string &a, const string &b){return a + " " + b;});
}

TEST_F(DatabaseBackendBasicImplMethodsTest, childrenTags) {
    sqlExecutor()->setConnectionString(":memory:");
    ASSERT_NO_THROW(backend()->initializeDb());
    sqlExecutor()->execute("INSERT INTO tag DEFAULT VALUES");
    sqlExecutor()->execute("INSERT INTO tag DEFAULT VALUES");
    sqlExecutor()->execute("INSERT INTO tag DEFAULT VALUES");
    sqlExecutor()->execute("INSERT INTO tag_tag (tag_id, super_tag_id) VALUES (2, 1)");
    sqlExecutor()->execute("INSERT INTO tag_tag (tag_id, super_tag_id) VALUES (3, 1)");
    vector<int> children;
    ASSERT_NO_THROW(children = backend()->childrenTags(1));
    EXPECT_EQ(children.size(), 2);


}

}}}}
