#include "try_catch_to_string.hpp"

#include <exception>

#include <boost/algorithm/string.hpp>
#include <boost/exception/all.hpp>

using namespace std;

namespace MyTime {

boost::optional<std::string> try_catch_to_string(const std::function<void ()> &f)
{
    try{
        f();
        return boost::none;
    }
    catch(const exception &ex){
        string exStr{ex.what()};
//        string exStr{boost::diagnostic_information(ex)};
        return boost::trim_copy(exStr).empty() ? "Undefined error" : exStr;
    }
    catch(...){
        return boost::current_exception_diagnostic_information();
    }
}

}
