#ifndef GUARD_1DE1906D7CEA446E9D0EA36C88A4D60F
#define GUARD_1DE1906D7CEA446E9D0EA36C88A4D60F

#include <functional>
#include <string>

#include <boost/optional.hpp>

namespace MyTime {

boost::optional<std::string> try_catch_to_string(const std::function<void ()> &f);

}
#endif // GUARD_1DE1906D7CEA446E9D0EA36C88A4D60F
