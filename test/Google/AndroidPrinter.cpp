#include "AndroidPrinter.hpp"

#include <QDebug>

using namespace testing;

namespace MyTime { namespace Test {

AndroidPrinter::AndroidPrinter()
{

}

void AndroidPrinter::OnEnvironmentsTearDownEnd(const UnitTest &)
{
    qDebug() << "OnEnvironmentsTearDownEnd";
}

void AndroidPrinter::OnTestEnd(const TestInfo &testInfo)
{
    qDebug() << testInfo.test_case_name() << "." << testInfo.name() << " end";
}

void AndroidPrinter::OnTestProgramEnd(const UnitTest &unitTest)
{
    qDebug() << "Test program end:";
    qDebug() << "Total test count = "       << unitTest.total_test_count();
    qDebug() << "Failed test count = "      << unitTest.failed_test_count();
    qDebug() << "Disabled test count = "    << unitTest.disabled_test_count();
    qDebug() << "Test to run count = "      << unitTest.test_to_run_count();
}

void AndroidPrinter::OnTestStart(const TestInfo &testInfo)
{
    qDebug() << testInfo.test_case_name() << "." << testInfo.name() << " start";
}

}}
