#include "QTagNameFilterProxyModel.hpp"

#include <algorithm>
#include <exception>

#include "my_throw.hpp"

using namespace std;

namespace MyTime { namespace Models {

QTagNameFilterProxyModel::QTagNameFilterProxyModel(const int &nameIndex, QObject *parent):
    QSortFilterProxyModel(parent),
    nameIndex_(nameIndex), filter_("")
{
    if(nameIndex < 0){
        my_throw<invalid_argument>("name index is negative (" + to_string(nameIndex) + ")");
    }
}

bool QTagNameFilterProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex &) const
{
    if(filter_ == "")
        return true;

    QString name = sourceModel()->data(sourceModel()->index(sourceRow, 0), Qt::UserRole + nameIndex_ + 1).toString();
    auto words = name.split(QRegExp("\\W+"), QString::SkipEmptyParts);
    return any_of(words.begin(), words.end(), [this](const QString &w)
    {
        return w.startsWith(filter_, Qt::CaseInsensitive);
    });
}

int QTagNameFilterProxyModel::idByRow(const int &row)
{
    return data(index(row, 0), Qt::UserRole + 0 + 1).toInt();
}

void QTagNameFilterProxyModel::setFilter(const QString &filter)
{
    beginResetModel();
    filter_ = filter;
    endResetModel();
}

}}
