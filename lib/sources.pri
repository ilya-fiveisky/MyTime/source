HEADERS += \
    $$PWD/QGUIContext.hpp \
    $$PWD/QGUIContextImpl.hpp \
    $$PWD/Models/QActivityModel.hpp \
    $$PWD/Models/QSqlRolesModel.hpp \
    $$PWD/Models/QModelBase.hpp \
    $$PWD/Models/QTagModel.hpp \
    $$PWD/Models/QActivityModelImpl.hpp \
    $$PWD/mylib_global.h \
    $$PWD/QAnalyticsContext.hpp \
    $$PWD/Models/QDurationChartModel.hpp \
    $$PWD/Models/QTagIdFilterProxyModel.hpp \
    $$PWD/Models/QTagNameFilterProxyModel.hpp \
    $$PWD/Database/Store.hpp \
    $$PWD/Database/StoreImpl.hpp \
    $$PWD/Database/db_exception.hpp \
    $$PWD/Database/SqlExecutor.hpp \
    $$PWD/Database/Backend/Basic.hpp \
    $$PWD/Database/Backend/BasicImpl.hpp \
    $$PWD/Database/Backend/Editor/Basic.hpp \
    $$PWD/Database/Backend/Editor/Tag.hpp \
    $$PWD/Database/Backend/Editor/Activity.hpp \
    $$PWD/QMyApplication.hpp \
    $$PWD/try_catch_to_string.hpp \
    $$PWD/check_ptr_arg.hpp \
    $$PWD/my_throw.hpp

SOURCES += \
    $$PWD/QGUIContext.cpp \
    $$PWD/QGUIContextImpl.cpp \
    $$PWD/Models/QActivityModel.cpp \
    $$PWD/Models/QModelBase.cpp \
    $$PWD/Models/QTagModel.cpp \
    $$PWD/Models/QActivityModelImpl.cpp \
    $$PWD/QAnalyticsContext.cpp \
    $$PWD/Models/QDurationChartModel.cpp \
    $$PWD/Models/QTagIdFilterProxyModel.cpp \
    $$PWD/Models/QTagNameFilterProxyModel.cpp \
    $$PWD/Database/Store.cpp \
    $$PWD/Database/StoreImpl.cpp \
    $$PWD/Database/SqlExecutor.cpp \
    $$PWD/Database/Backend/Basic.cpp \
    $$PWD/Database/Backend/BasicImpl.cpp \
    $$PWD/Database/Backend/Editor/Basic.cpp \
    $$PWD/Database/Backend/Editor/Tag.cpp \
    $$PWD/Database/Backend/Editor/Activity.cpp \
    $$PWD/QMyApplication.cpp \
    $$PWD/try_catch_to_string.cpp

!include ($$PWD/qt-supermacros/QtSuperMacros.pri){
    message("qt-supermacros not included")
}
