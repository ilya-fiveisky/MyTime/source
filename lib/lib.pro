TARGET = mylib

TEMPLATE = lib

DEFINES += MYLIB_LIBRARY

!include ($$PWD/config.pri){
    message("config.pri is not included")
}

!include ($$PWD/libs.pri){
    message("libs.pri is not included")
}

!include ($$PWD/sources.pri){
    message("sources.pri is not included")
}

RESOURCES += \
    mylib.qrc

