import QtQuick 2.7
import QtQuick.Controls 2.1

ListView {
    function setStepSize(){
        var rowCount = model.rowCount();
        verticalScrollBar.stepSize = rowCount ? 1/rowCount : 1;
        forceActiveFocus();
    }

    Component.onCompleted: {
        model.rowsInserted.connect(setStepSize);
        model.rowsRemoved.connect(setStepSize);
        setStepSize();
    }

    onVisibleChanged: if(visible) forceActiveFocus()

    ScrollBar.vertical: ScrollBar {
        id: verticalScrollBar
    }

    Keys.onUpPressed: verticalScrollBar.decrease()
    Keys.onDownPressed: verticalScrollBar.increase()
}
