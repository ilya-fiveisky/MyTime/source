#ifndef GUARD_59A68856688146FDAF7057EDC7C759FC
#define GUARD_59A68856688146FDAF7057EDC7C759FC

#include "Basic.hpp"

namespace MyTime { namespace Database {

class SqlExecutor;

namespace Backend { namespace Editor {

class Tag : public Basic
{
public:
    explicit Tag(const std::shared_ptr<MyTime::Database::SqlExecutor> &sqlExecutor);

    void    updateName(const int &id, const QString &name);

protected:
    virtual std::vector<std::string> getDeleteCommands(const int &id) const override;
    std::string mainName() const override;
    std::string tagName() const override;
};

}}}}
#endif // GUARD_59A68856688146FDAF7057EDC7C759FC
