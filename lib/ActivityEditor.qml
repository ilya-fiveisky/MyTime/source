import QtQuick 2.7
import QtQuick.Controls 1.4 as Controls1
import QtQuick.Controls 2.1
import QtQuick.Dialogs 1.1
import QtQuick.Layouts 1.1
import QtQuick.Window 2.2
import "."

Page {
    id: root

    property Item toolBarLayout: null
    property QtObject tagsModel
    property QtObject activityModel

    onToolBarLayoutChanged: {
        plusButton.parent = toolBarLayout;
        plusButton.visible = toolBarLayout !== null;
    }

    onVisibleChanged: {
        if(toolBarLayout !== null){
            plusButton.visible = visible;
        }
    }

    IconToolButton {
        id: plusButton
        objectName: "addActivityButton"
        visible: false
        Layout.alignment: Qt.AlignLeading
        font.bold: true
        iconSource: "icons/add.svg"
        iconColor: "white"
        MyToolTip {text: qsTr("Create New Activity")}

        onClicked: {
            doCheckableAction(function(){return activityModel.makeNew();});
        }
    }

    MessageDialog {
        id: message
        standardButtons: StandardButton.Close
    }

    function doCheckableAction(action){
        var error = action();
        if(error!==""){
            message.text = error;
            message.open();
        }
    }

    Flickable {
        id: flickable
        anchors.fill: parent
        contentWidth: Math.max(1000, Screen.desktopAvailableWidth)

        ColumnLayout {
            id: columnLayout
            height: parent.height
            width: ApplicationWindow.window.width
            spacing: 0

            Rectangle {
                Layout.preferredHeight: 40
                Layout.fillWidth: true
                color: Style.current.background
                z: 1

                // In order to not allow to press start/stop buttons under the header.
                MouseArea {
                    anchors.fill: parent
                }

                Controls1.SplitView {
                    id: splitView
                    anchors.fill: parent

                    handleDelegate: SplitterBase {
                        Layout.fillHeight: true
                        width: size
                        color: Style.backgroundAltColor()
                    }

                    Item {
                        id: deleteHeader
                        Layout.minimumWidth: 30
                        Layout.maximumWidth: 30
                        height: parent.height
                    }
                    Label {
                        id: descriptionHeader
                        text: qsTr("Description")
                        height: parent.height
                        Layout.minimumWidth: flickable.contentWidth / 6
                        Layout.fillWidth: true
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        font.bold: true
                    }
                    Item {
                        id: startButtonHeader
                        Layout.minimumWidth: 50
                        Layout.maximumWidth: 50
                        height: parent.height
                    }
                    Column {
                        id: timeHeader
                        Layout.minimumWidth: 140
                        Layout.maximumWidth: 140
                        height: parent.height
                        padding: parent.height / 10
                        Row {
                            anchors.horizontalCenter: parent.horizontalCenter
                            spacing: 5

                            Label { text: qsTr("Start"); font.bold: true }
                            Label { text: "-"; font.bold: true }
                            Label { text: qsTr("End"); font.bold: true }
                        }
                        Label {
                            text: qsTr("Duration")
                            anchors.horizontalCenter: parent.horizontalCenter
                            font.bold: true
                        }
                    }
                    Label {
                        id: tagsHeader
                        text: qsTr("Tags")
                        Layout.minimumWidth: flickable.contentWidth / 5
                        height: parent.height
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        font.bold: true
                    }
                }
            }

            SplitterBase {
                Layout.fillWidth: true
                height: size
                color: Style.backgroundAltColor()
                z: 1
            }

            MyListView {
                id: activityView
                Layout.fillWidth: true
                Layout.fillHeight: true
                contentWidth: flickable.contentWidth
                model: activityModel
                spacing: 0

                delegate: Pane {
                    spacing: 0
                    padding: 0

                    background: Rectangle {
                        color: index % 2 === 1 ? Style.current.background : Style.backgroundAltColor();
                    }

                    RowLayout {
                        id: activityRow
                        spacing: 0

                        IconToolButton {
                            id: deleteButton
                            objectName: "deleteButton" + index.toString()
                            Layout.preferredHeight: deleteHeader.width
                            Layout.preferredWidth: deleteHeader.width
                            iconSource: "icons/remove_circle_outline.svg"
                            iconColor: Style.foregroundAltColor()
                            MyToolTip {text: qsTr("Delete Activity")}

                            Popup {
                                id: deleteActivityDialog

                                ColumnLayout {
                                    Label {
                                        text: qsTr("Delete activity?")
                                    }
                                    RowLayout {
                                        Button {
                                            text: qsTr("Yes")
                                            objectName: "yesButton"
                                            onClicked: {
                                                deleteActivityDialog.close();
                                                doCheckableAction(function(){return activityModel.deleteRow(index);});
                                            }
                                        }
                                        Button {
                                            text: qsTr("No")
                                            onClicked: {
                                                deleteActivityDialog.close();
                                            }
                                        }
                                    }
                                }
                            }

                            onClicked: {
                                deleteActivityDialog.open();
                            }
                        }
                        ColumnSplitter {}
                        TextField {
                            id: descriptionField
                            Layout.preferredWidth: descriptionHeader.width
                            text: model.description
                            placeholderText: qsTr("Enter description")

                            onEditingFinished: model.description = text
                        }
                        ColumnSplitter {}
                        IconToolButton {
                            id: startButton
                            objectName: "startButton" + index.toString()
                            Layout.preferredWidth: startButtonHeader.width
                            checkable: true
                            checked: model.running
                            iconSource: checked ? "icons/pause.svg" : "icons/play.svg"

                            MyToolTip {text: startButton.checked ? qsTr("Pause Activity") : qsTr("Start Activity")}

                            onClicked: {
                                if(model.running){
                                    doCheckableAction(function(){return activityModel.stop(index);});
                                }
                                else{
                                    doCheckableAction(function(){return activityModel.start(index);});
                                }
                            }
                        }
                        ColumnSplitter {}
                        ColumnLayout {
                            Layout.preferredWidth: timeHeader.width
                            Layout.topMargin: 5
                            Layout.bottomMargin: 5

                            RowLayout {
                                Layout.preferredWidth: timeHeader.width - 10
                                anchors.horizontalCenter: parent.horizontalCenter

                                Label {
                                    text: model.start
                                    Layout.alignment: Qt.AlignLeft
                                }
                                Label {
                                    text: "-"
                                    visible: model.start
                                    anchors.horizontalCenter: parent.horizontalCenter
                                }
                                Label {
                                    text: model.end
                                    Layout.alignment: Qt.AlignRight
                                }
                            }
                            Label {
                                text: model.duration
                                anchors.horizontalCenter: parent.horizontalCenter
                            }
                        }
                        ColumnSplitter {}
                        CompactMultiSelection {
                            Layout.preferredWidth: tagsHeader.width
                            elementHeight: 20
                            selectorModel: tagsModel
                            selected: model.tags
                            elementName: "Tag"

                            onChanged: {
                                model.tags = selected;
                            }
                        }
                    }
                }

                section.property: "start_date"
                section.criteria: ViewSection.FullString
                section.delegate: GroupBox {
                    width: parent.width
                    padding: 10

                    Label {
                        text: section
                        font.bold: true
                    }
                }
            }
        }
    }
}
