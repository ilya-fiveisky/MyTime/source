#include "QGuiContext.hpp"

#include <boost/algorithm/string/predicate.hpp>
#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <QDebug>
#include <QUrl>

using namespace std;
using namespace boost::algorithm;
using namespace boost::filesystem;
namespace pt = boost::property_tree;

QGuiContext::QGuiContext(QObject *parent) : QObject(parent)
{

}

void QGuiContext::deploy(const QString &librarySources, const bool &isDebug,
                         const QString &folderSources,
                         const QUrl &executableFile,
                         const QUrl &targetFolder)
{
    path deployDir{targetFolder.toLocalFile().toStdString()};

    for(QUrl libUrl : getUrlList(librarySources)){
        for(auto de : directory_iterator(libUrl.toLocalFile().toStdString())){
            if(is_regular_file(de.path()) && ends_with(de.path().string(), ".dll")){
                bool isDebugVersionExists = exists(
                            (de.path().parent_path() /= de.path().stem()).string() + "d.dll");
                if(!isDebug || !isDebugVersionExists){
                    copy_file(de.path(),
                              path(deployDir) /= de.path().filename(),
                              copy_option::overwrite_if_exists);
                }
            }
        }
    }
    qInfo() << "Libriaries copied";

    for(QUrl folderUrl : getUrlList(folderSources)){
        path folder{folderUrl.toLocalFile().toStdString()};
        for(auto de : recursive_directory_iterator(folder)){
            if(is_regular_file(de)){
                copy_file(de.path(), path(deployDir) /= relative(de.path(), folder),
                          copy_option::overwrite_if_exists);
            }
            else{
//                if(!exists(de)){
                    copy(de.path(), path(deployDir) /= relative(de.path(), folder));
//                }
            }
        }
    }
    qInfo() << "Folders copied";

    path execFile{executableFile.toLocalFile().toStdString()};
    copy_file(execFile,
              path(deployDir) /= execFile.filename(),
              copy_option::overwrite_if_exists);
    qInfo() << "Exe copied";
}

QStringList QGuiContext::loadConfiguration(const QUrl &loadFromPath)
{
    pt::ptree tree;
    pt::read_xml(loadFromPath.toLocalFile().toStdString(), tree);
    QStringList result;
    result.append(QString::fromStdString(tree.get<std::string>("librarySources")));
    result.append(QString::fromStdString(tree.get<std::string>("folderSources")));
    result.append(QString::fromStdString(tree.get<std::string>("executableFile")));
    result.append(QString::fromStdString(tree.get<std::string>("targetFolder")));
    return result;
}

void QGuiContext::saveConfiguration(
        const QUrl &saveToPath,
        const QString &librarySources,
        const QString &folderSources,
        const QUrl &executableFile,
        const QUrl &targetFolder)
{
    pt::ptree tree;
    tree.put("librarySources", librarySources.toStdString());
    tree.put("folderSources", folderSources.toStdString());
    tree.put("executableFile", executableFile.toString().toStdString());
    tree.put("targetFolder", targetFolder.toString().toStdString());
    pt::write_xml(saveToPath.toLocalFile().toStdString(), tree);
}

QList<QUrl> QGuiContext::getUrlList(const QString &urlString) const
{
    auto preparedString = QString(urlString).replace("; ", ";");
    return QUrl::fromStringList(preparedString.split(";", QString::SkipEmptyParts));
}
