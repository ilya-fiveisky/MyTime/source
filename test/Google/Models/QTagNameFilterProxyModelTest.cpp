#include "gtest/gtest.h"

#include <exception>
#include <memory>

#include "Database/Backend/Editor/Tag.hpp"
#include "Database/SqlExecutor.hpp"
#include "Models/QTagNameFilterProxyModel.hpp"
#include "Models/QTagModel.hpp"

#include "BasicTest.hpp"

using namespace std;

using namespace MyTime;
using namespace MyTime::Models;

namespace MyTime { namespace Test { namespace Models {

using namespace Models;

typedef BasicTest ModelsQTagNameFilterProxyModelBasicTest;

class ModelsQTagNameFilterProxyModelMethodsTest : public ModelsQTagNameFilterProxyModelBasicTest {
protected:
    ModelsQTagNameFilterProxyModelMethodsTest()
    {
        tagFilterModel_ = make_shared<QTagNameFilterProxyModel>(QTagModel::RoleIndex::name);
        tagFilterModel_->setSourceModel(tags().get());
    }

    shared_ptr<QTagNameFilterProxyModel> tagFilterModel() const
    {
        return tagFilterModel_;
    }

private:
    shared_ptr<QTagNameFilterProxyModel> tagFilterModel_;
};

typedef ModelsQTagNameFilterProxyModelBasicTest ModelsQTagNameFilterProxyModelConstructorTest;

TEST_F(ModelsQTagNameFilterProxyModelConstructorTest, test) {
    EXPECT_THROW(QTagNameFilterProxyModel m(-1), invalid_argument);

    EXPECT_NO_THROW(QTagNameFilterProxyModel m(1));
}

TEST_F(ModelsQTagNameFilterProxyModelMethodsTest, filterAcceptsRowAndSetFilter) {
    int id1 = tagDbBackend()->create();
    tagDbBackend()->updateName(id1, "abc bcd");
    int id2 = tagDbBackend()->create();
    tagDbBackend()->updateName(id2, "xyz.bc");
    tags()->refresh();

    ASSERT_NO_THROW(tagFilterModel()->setFilter("abc"));
    EXPECT_TRUE(tagFilterModel()->filterAcceptsRow(0));
    EXPECT_FALSE(tagFilterModel()->filterAcceptsRow(1));

    ASSERT_NO_THROW(tagFilterModel()->setFilter("bc"));
    EXPECT_TRUE(tagFilterModel()->filterAcceptsRow(0));
    EXPECT_TRUE(tagFilterModel()->filterAcceptsRow(1));

    ASSERT_NO_THROW(tagFilterModel()->setFilter("xy"));
    EXPECT_FALSE(tagFilterModel()->filterAcceptsRow(0));
    EXPECT_TRUE(tagFilterModel()->filterAcceptsRow(1));

    ASSERT_NO_THROW(tagFilterModel()->setFilter("z"));
    EXPECT_FALSE(tagFilterModel()->filterAcceptsRow(0));
    EXPECT_FALSE(tagFilterModel()->filterAcceptsRow(1));
}

TEST_F(ModelsQTagNameFilterProxyModelMethodsTest, idByRow) {
    int id = tagDbBackend()->create();
    tagDbBackend()->create();
    tags()->refresh();
    EXPECT_EQ(tagFilterModel()->idByRow(0), id);
    EXPECT_THROW(tagFilterModel()->idByRow(1000), out_of_range);
}

}}}
