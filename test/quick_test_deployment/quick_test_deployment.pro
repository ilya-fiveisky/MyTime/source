QT += core
QT -= gui

CONFIG += c++11

TARGET = mytime_quick_test_deployment
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp


RESOURCES += qml.qrc

android {
    NDK_SOURCES = $$(ANDROID_NDK_ROOT)/sources

    equals(ANDROID_TARGET_ARCH, armeabi-v7a) {
        QMAKE_LIBDIR += $${NDK_SOURCES}/crystax/libs/armeabi-v7a
    }

    ANDROID_EXTRA_LIBS += \
        $${NDK_SOURCES}/boost/1.59.0/libs/armeabi-v7a/gnu-4.9/libboost_date_time.so \
        $${NDK_SOURCES}/crystax/libs/armeabi-v7a/libcrystax.so

}
