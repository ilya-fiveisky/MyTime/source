#include "QMyApplication.hpp"

#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/log/attributes/named_scope.hpp>
#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/file.hpp>

#include <QDebug>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QStandardPaths>

#include "Database/Backend/Basic.hpp"
#include "Database/Backend/Editor/Activity.hpp"
#include "Database/Backend/Editor/Tag.hpp"
#include "Database/db_exception.hpp"
#include "Database/SqlExecutor.hpp"
#include "Database/Store.hpp"
#include "Models/QActivityModel.hpp"
#include "Models/QTagModel.hpp"
#include "QAnalyticsContext.hpp"
#include "QGUIContext.hpp"

using namespace std;

namespace logging = boost::log;
namespace src = boost::log::sources;
namespace expr = boost::log::expressions;
namespace keywords = boost::log::keywords;

using namespace MyTime::Models;

namespace MyTime {

QMyApplication::QMyApplication(int &argc, char **argv) :
    QApplication(argc, argv),
    engine_{make_shared<QQmlApplicationEngine>()}
{
    initLog();

    BOOST_LOG_FUNCTION();
    BOOST_LOG_TRIVIAL(info) << "Start QMyApplication construction.";

    auto sqlExecutor = make_shared<Database::SqlExecutor>();
    auto store = make_shared<Database::Store>(sqlExecutor);
    store->open();
    auto dbBackend = make_shared<Database::Backend::Basic>(sqlExecutor);
    auto tagDbBackend = make_shared<Database::Backend::Editor::Tag>(sqlExecutor);
    auto tagViewModel = make_shared<QTagModel>(tagDbBackend);
    auto activityDbBackend = make_shared<Database::Backend::Editor::Activity>(sqlExecutor);
    auto activityViewModel = make_shared<QActivityModel>(tagViewModel, activityDbBackend, nullptr);
    auto analytics = make_shared<QAnalyticsContext>(tagViewModel, dbBackend);
    guiContext_ = make_shared<QGUIContext>(store, activityViewModel, analytics, tagViewModel);

    engine_->rootContext()->setContextObject(guiContext_.get());
    engine_->load(QUrl(QStringLiteral("qrc:/main.qml")));

    BOOST_LOG_TRIVIAL(info) << "QMyApplication constructed.";
}

bool QMyApplication::notify(QObject *receiver, QEvent *event)
{
    bool done = true;
    try {
        done = QApplication::notify(receiver, event);
    }
    catch(const boost::exception &ex){
        BOOST_LOG_TRIVIAL(fatal) << boost::diagnostic_information(ex);
    }
    catch(...){
        BOOST_LOG_TRIVIAL(fatal) << boost::current_exception_diagnostic_information();
    }
    return done;
}

QString QMyApplication::localDataPath()
{
    return QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
}

void QMyApplication::initLog()
{
    logging::add_console_log();

    QString filePath{localDataPath() + "/MyTime_%N.log"};

    auto timeStampAttr =
            expr::format_date_time< boost::posix_time::ptime >
            ("TimeStamp", "%Y-%m-%d %H:%M:%S");
    auto scopeAttr = expr::attr<logging::attributes::named_scope::value_type>("Scope");
    logging::add_file_log
            (
                keywords::file_name = filePath.toStdString(),
                keywords::rotation_size = 1024 * 1024,
                keywords::time_based_rotation = logging::sinks::file::rotation_at_time_point(0, 0, 0),
                keywords::format = (expr::stream
                                    << "[" << timeStampAttr << "]"
                                    << "[" << logging::trivial::severity << "]"
                                    << "[" << scopeAttr << "]"
                                    << ": " << expr::smessage
                                    ),
                keywords::auto_flush = true,
                keywords::open_mode = std::ios_base::app
            );

    logging::core::get()->set_filter
            (
                logging::trivial::severity >= logging::trivial::info
                );

    // Necessary in order to write TimeStamp.
    logging::add_common_attributes();

    logging::core::get()->add_global_attribute("Scope", logging::attributes::named_scope());
}

}
