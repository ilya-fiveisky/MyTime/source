import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import "."

Flow {
    id: root
    spacing: 3
    padding: 3

    property var selectorModel
    property var selected
    property int elementHeight: 20
    property string elementName

    signal changed

    IconToolButton {
        objectName: "plusButton"
        height: elementHeight
        width: elementHeight
        iconColor: Style.foregroundAltColor()
        iconSource: "icons/add_box.svg"
        iconScale: 1

        onClicked: {
            comboBoxComponent.createObject(comboBoxPlace);
            var selector = comboBoxPlace.children[0];
            var right = selector.x + selector.width;
            var sceneRight = mapToItem(null, right, 0);
            if(ApplicationWindow.window){
                var d = ApplicationWindow.window.width - sceneRight.x;
                if(d < 0){
                    selector.popup.x = selector.x + d;
                }
                else{
                    selector.popup.x = selector.x;
                }
            }
            if(ApplicationWindow.window){
                selector.popup.height = ApplicationWindow.window.height * 0.75;
            }
            selector.popup.open();
            selector.incrementCurrentIndex();
        }

        Item {
            id: comboBoxPlace
        }

        Component {
            id: comboBoxComponent

            ComboBox {
                id: selector
                visible: false
                model: selectorModel
                textRole: "name"
                Layout.preferredHeight: elementHeight
                width: 200

                contentItem: TextField {
                    id: filterField
                    onTextChanged: {
                        selectorModel.setFilter(text);
                    }
                }

                onActivated: {
                    if(index > -1 && index < model.rowCount()){
                        var id = model.idByRow(index);
                        if(selected.ids.indexOf(id) === -1){
                            var newIds = selected.ids.slice(0);
                            newIds.push(id);
                            selected.ids = newIds;
                        }
                    }
                }

                Component.onCompleted: {
                    currentIndex = -1;
                    filterField.forceActiveFocus();
                    selector.popup.closed.connect(function (){
                        filterField.clear();
                        selector.destroy();
                    });
                }
            }
        }
    }

    Label {
        id: placeholderLabel
        text: qsTr("Add ") + elementName
        visible: repeater.count == 0
        verticalAlignment: Text.AlignVCenter
        color: Style.foregroundAltColor(1.5)
    }

    Repeater {
        id: repeater
        objectName: "repeater"
        model: selected

        onModelChanged: {
            if(model){
                model.modelReset.connect(function(){
                    if(root){
                        root.changed();
                    }
                });
            }
        }

        GroupBox {
            id: groupBox
            padding: 0
            leftPadding: 5
            spacing: 0

            background: Rectangle {
                y: groupBox.topPadding - groupBox.padding
                width: parent.width
                height: parent.height - groupBox.topPadding + groupBox.padding
                color: "transparent"
                border.width: 1
                border.color: Style.foregroundAltColor(1.5)
                radius: 5
            }

            RowLayout {
                Layout.preferredHeight: elementHeight
                spacing: 0

                Label {
                    id: tagName
                    text: model.name
                    Layout.preferredHeight: elementHeight
                    verticalAlignment: Text.AlignVCenter
                }

                IconToolButton {
                    objectName: "deleteButton"
                    Layout.preferredHeight: elementHeight
                    Layout.preferredWidth: elementHeight
                    iconSource: "icons/backspace.svg"
                    iconColor: Style.foregroundAltColor()

                    onClicked: {
                        var newIds = selected.ids.slice(0);
                        var i = newIds.indexOf(model.id);
                        newIds.splice(i, 1);
                        selected.ids = newIds;
                    }
                }
            }
        }
    }
}
