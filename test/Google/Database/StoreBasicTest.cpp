#include "StoreBasicTest.hpp"

#include <boost/filesystem.hpp>

#include <QSettings>

#include "Database/StoreImpl.hpp"

using namespace std;
using namespace boost::filesystem;
using namespace MyTime::Database;

namespace MyTime { namespace Test {namespace Database {

void StoreBasicTest::TearDown()
{
    SETTINGS->sync();
    MyTime::Test::Database::BasicTest::TearDown();
}

void StoreBasicTest::checkBackup(const path &dbPath)
{
    auto backupPath = path(dbPath).replace_extension(StoreImpl::BACKUP_FILE_EXTENSION);
    EXPECT_TRUE(exists(backupPath)) << "DB backup file " << backupPath << " does not exist.";
}

void StoreBasicTest::checkBackup()
{
    checkBackup(dbPath());
}

}}}
