#include "Activity.hpp"

#include <vector>

#include <QSqlQuery>

#include "../../SqlExecutor.hpp"

using namespace std;

namespace MyTime { namespace Database { namespace Backend { namespace Editor {

Activity::Activity(const std::shared_ptr<SqlExecutor> &sqlExecutor) :
    Basic(sqlExecutor){}

void Activity::start(const int &id)
{
    checkConnection();
    sqlExecutor()->execute("INSERT INTO interval (activity_id) VALUES (?)", {id});
}

void Activity::stop(const int &id)
{
    checkConnection();
    sqlExecutor()->execute("UPDATE interval SET end = strftime('%s', 'now') WHERE activity_id = ? AND end ISNULL", {id});
}

void Activity::updateDescription(const int &id, const QString &description)
{
    checkConnection();
    sqlExecutor()->execute("UPDATE activity SET description = ? WHERE id = ?", {description, id});
}

vector<string> Activity::getDeleteCommands(const int &id) const
{
    auto idStr = to_string(id);
    vector<string> commands(Basic::getDeleteCommands(id));
    commands.insert(begin(commands),
                    "DELETE FROM interval WHERE activity_id = " + idStr);
    return commands;
}

string Activity::mainName() const
{
    return "activity";
}

string Activity::tagName() const
{
    return "tag";
}

}}}}
