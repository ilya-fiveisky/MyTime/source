#include "QActivityModelBasicTest.hpp"

using namespace std;

namespace MyTime { namespace Test { namespace Models {

QActivityModelBasicTest::QActivityModelBasicTest()
{
    activityDbBackend_ = make_shared<Database::Backend::Editor::Activity>(sqlExecutor());
}

shared_ptr<Database::Backend::Editor::Activity> QActivityModelBasicTest::activityDbBackend() const
{
    return activityDbBackend_;
}

void QActivityModelBasicTest::invalidateDbConnection()
{
    for(auto connName : QSqlDatabase::connectionNames()){
        QSqlDatabase::removeDatabase(connName);
    }
}


}}}
