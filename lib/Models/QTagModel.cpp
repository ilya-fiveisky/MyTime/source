#include "QTagModel.hpp"

#include <exception>

#include <QDebug>
#include <QSqlField>
#include <QSqlRecord>

#include "check_ptr_arg.hpp"

#include "Database/Backend/Editor/Tag.hpp"
#include "QTagIdFilterProxyModel.hpp"

namespace MyTime { namespace Models {

using namespace std;

const QString QTagModel::QUERY =
        "SELECT tag.id, tag.name, group_concat(supertag.id, ',') AS supertags "
        "FROM "
        "(tag LEFT OUTER JOIN tag_tag ON tag.id = tag_tag.tag_id) "
        "LEFT OUTER JOIN tag AS supertag ON supertag.id = tag_tag.super_tag_id "
        "GROUP BY tag.id "
        "ORDER BY tag.name ASC";

const vector<QString> QTagModel::roles_ = {"id", "name", "supertags"};

QTagModel::QTagModel(const std::shared_ptr<Database::Backend::Editor::Tag> &dbBackend, QObject *parent):
    BaseT(QTagModel::QUERY, id, supertags, parent),
    dbBackend_{dbBackend}
{
    check_ptr_arg(dbBackend, "db backend");
    dbBackend_->dbChanged.connect([this](){ refresh(); });
    setUserRoleNames(roles_);
    refresh();
}

QVariant QTagModel::data(const QModelIndex &index, int role) const
{
    return BaseT::data(index, role);
}

bool QTagModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(columnFromRole(role) == RoleIndex::name && value != data(index, role)){
        updateName(index.row(), value.toString());
    }
    return BaseT::setData(index, value, role);
}

QString QTagModel::nameById(const int &id) const
{
    auto it = find_if(records_.begin(), records_.end(),
                      [id](const shared_ptr<QSqlRecord> &r){
        return r->value(RoleIndex::id).toInt() == id;});
    if(it == records_.end()){
        my_throw<invalid_argument>("There is no such id: " + to_string(id));
    }
    return (*it)->value(RoleIndex::name).toString();
}

std::shared_ptr<Database::Backend::Editor::Basic> QTagModel::dbBackend() const
{
    return dbBackend_;
}

QModelBase *QTagModel::tagModel()
{
    return this;
}

std::shared_ptr<QSqlRecord> QTagModel::createRecord(const QSqlRecord &record)
{
    auto r = createRecord();

    r->setValue(RoleIndex::id, record.value(RoleIndex::id));
    r->setValue(RoleIndex::name, record.value(RoleIndex::name));
    setRecordTags(record, r);

    return r;
}

std::shared_ptr<QSqlRecord> QTagModel::createRecord() const
{
    auto r = make_shared<QSqlRecord>();
    r->append(QSqlField(roles_[RoleIndex::id], QVariant::Int));
    r->append(QSqlField(roles_[RoleIndex::name], QVariant::String));
    r->append(QSqlField(roles_[RoleIndex::supertags]));
    return r;
}

void QTagModel::updateName(const int &row, const QString &name)
{
    dbBackend_->updateName(idByRow(row), name);
}

}}
