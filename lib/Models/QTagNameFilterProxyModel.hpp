#ifndef GUARD_F71CA0BA91FC478D873F9E90344BA7C0
#define GUARD_F71CA0BA91FC478D873F9E90344BA7C0

#include <QSortFilterProxyModel>

#include "mylib_global.h"

namespace MyTime { namespace Models {

class MYLIB_EXPORT QTagNameFilterProxyModel: public QSortFilterProxyModel
{
    Q_OBJECT

public:
    explicit QTagNameFilterProxyModel(const int &nameIndex, QObject *parent = 0);

    bool filterAcceptsRow(int sourceRow, const QModelIndex &parent = QModelIndex()) const override;

public slots:
    int idByRow(const int &row);
    void setFilter(const QString &filter);

private:
    int nameIndex_;
    QString filter_;
};

}}
#endif // GUARD_F71CA0BA91FC478D873F9E90344BA7C0
