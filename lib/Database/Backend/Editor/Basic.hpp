#ifndef GUARD_16BD0369371B487ABD2B59905352978F
#define GUARD_16BD0369371B487ABD2B59905352978F

#include <string>
#include <vector>

#include <QList>
#include <QString>

#include "../Basic.hpp"

namespace MyTime { namespace Database {

class SqlExecutor;

namespace Backend { namespace Editor {

class Basic : public Backend::Basic
{
public:
    Basic(const std::shared_ptr<MyTime::Database::SqlExecutor> &sqlExecutor);

    int     create();
    void    remove(const int &id);
    void    updateTags(const int &id, const QList<int> &tagIds);

protected:
    virtual std::vector<std::string> getDeleteCommands(const int &id) const;
    virtual std::string mainName() const = 0;
    virtual std::string tagName() const = 0;
};

}}}}
#endif // GUARD_16BD0369371B487ABD2B59905352978F
