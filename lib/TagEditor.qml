import QtQuick 2.7
import QtQuick.Controls 1.4 as Controls1
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.1
import QtQuick.Window 2.2
import "."

Page {
    id: root

    property Item toolBarLayout: null
    property QtObject superTagModel
    property QtObject tagModel

    onToolBarLayoutChanged: {
        plusButton.parent = toolBarLayout;
        plusButton.visible = toolBarLayout !== null;
    }

    onVisibleChanged: {
        if(toolBarLayout !== null){
            plusButton.visible = visible;
        }
    }

    IconToolButton {
        id: plusButton
        objectName: "addTagButton"
        visible: false
        Layout.alignment: Qt.AlignLeading
        iconSource: "icons/add.svg"
        iconColor: "white"
        font.bold: true

        MyToolTip {text: qsTr("Create New Tag")}

        onClicked: {
            tagModel.makeNew();
        }
    }

    Flickable {
        id: flickable
        anchors.fill: parent
        contentWidth: Math.max(1000, Screen.desktopAvailableWidth)

        ColumnLayout {
            id: columnLayout
            height: parent.height
            width: ApplicationWindow.window.width
            spacing: 0

            Rectangle {
                Layout.preferredHeight: 60
                Layout.fillWidth: true
                color: Style.current.background
                z: 1

                // In order to not allow to press start/stop buttons under the header.
                MouseArea {
                    anchors.fill: parent
                }

                Controls1.SplitView {
                    id: splitView
                    anchors.fill: parent

                    handleDelegate: SplitterBase {
                        Layout.fillHeight: true
                        width: size
                        color: Style.backgroundAltColor()
                    }

                    Item {
                        id: deleteHeader
                        Layout.minimumWidth: 30
                        Layout.maximumWidth: 30
                        height: parent.height
                    }
                    Label {
                        id: nameHeader
                        text: qsTr("Name")
                        height: parent.height
                        Layout.minimumWidth: flickable.contentWidth / 6
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        font.bold: true
                    }
                    Label {
                        id: supertagsHeader
                        text: qsTr("Supertags")
                        Layout.minimumWidth: flickable.contentWidth / 6
                        Layout.fillWidth: true
                        height: parent.height
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        font.bold: true
                    }
                }
            }

            SplitterBase {
                Layout.fillWidth: true
                height: size
                color: Style.backgroundAltColor()
                z: 1
            }

            MyListView {
                id: tagView
                Layout.fillWidth: true
                Layout.fillHeight: true
                model: tagModel
                spacing: 0

                delegate: Pane {
                    spacing: 0
                    padding: 0

                    background: Rectangle {
                        color: index % 2 === 1 ? Style.current.background : Style.backgroundAltColor();
                    }

                    RowLayout {
                        id: tagRow
                        spacing: 0

                        IconToolButton {
                            id: deleteButton
                            objectName: "deleteButton" + index.toString()
                            Layout.preferredHeight: deleteHeader.width
                            Layout.preferredWidth: deleteHeader.width
                            iconSource: "icons/remove_circle_outline.svg"
                            iconColor: Style.foregroundAltColor()
                            MyToolTip {text: qsTr("Delete Tag")}

                            Popup {
                                id: deleteTagDialog

                                ColumnLayout {
                                    Label {
                                        text: qsTr("Delete tag?")
                                    }
                                    RowLayout {
                                        Button {
                                            objectName: "yesButton"
                                            text: qsTr("Yes")
                                            onClicked: {
                                                tagModel.deleteRow(index);
                                                deleteTagDialog.close();
                                            }
                                        }
                                        Button {
                                            text: qsTr("No")
                                            onClicked: {
                                                deleteTagDialog.close();
                                            }
                                        }
                                    }
                                }
                            }

                            onClicked: {
                                deleteTagDialog.open();
                            }
                        }
                        ColumnSplitter {}
                        TextField {
                            id: nameField
                            Layout.preferredWidth: nameHeader.width
                            text: model.name
                            placeholderText: qsTr("Enter name")

                            onEditingFinished: model.name = text
                        }
                        ColumnSplitter {}
                        CompactMultiSelection {
                            Layout.preferredWidth: supertagsHeader.width
                            elementHeight: 20
                            selected: model.supertags
                            selectorModel: superTagModel
                            elementName: "Supertag"

                            onChanged: {
                                model.supertags = selected;
                            }
                        }
                    }
                }
            }
        }
    }
}
