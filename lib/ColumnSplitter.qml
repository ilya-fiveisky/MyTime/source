import QtQuick 2.7
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.1

SplitterBase {
    Layout.fillHeight: true
    width: size
    color: {
        var altColor = Material.theme === Material.Dark ?
                    Qt.lighter(Material.background, 1.2) :
                    Qt.darker(Material.background, 1.05);
        return index % 2 === 0 ? Material.background : altColor;
    }
}
