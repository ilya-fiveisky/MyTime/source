#include "QActivityModel.hpp"

#include <algorithm>
#include <ctime>
#include <exception>
#include <iterator>

#include <boost/date_time.hpp>
#include <boost/date_time/c_local_time_adjustor.hpp>
#include <boost/date_time/local_time/local_time.hpp>

#include <QDateTime>
#include <QSqlField>
#include <QTimerEvent>

#include "try_catch_to_string.hpp"

#include "Database/Backend/Editor/Activity.hpp"
#include "QActivityModelImpl.hpp"

using namespace std;
using namespace boost;
using namespace boost::local_time;
using namespace boost::posix_time;

namespace MyTime { namespace Models {

QActivityModel::QActivityModel(const std::shared_ptr<QModelBase> &tagModel,
                               const std::shared_ptr<Database::Backend::Editor::Activity> &dbBackend,
                               QObject *parent):
    BaseT(QActivityModelImpl::QUERY, QActivityModelImpl::RoleIndex::id, QActivityModelImpl::RoleIndex::tags, parent),
    impl_{new QActivityModelImpl{tagModel, dbBackend}}
{
    setUserRoleNames(QActivityModelImpl::roles);
    impl_->dbBackend_->dbChanged.connect([this](){ refresh(); });
    refresh();
    impl_->timerId_ = startTimer(QActivityModelImpl::TIMER_INTERVAL);
}

QActivityModel::~QActivityModel() = default;

bool QActivityModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(columnFromRole(role) == QActivityModelImpl::RoleIndex::description && value != data(index, role)){
        impl_->updateDescription(index.row(), value.toString(), this);
    }
    return BaseT::setData(index, value, role);
}

QString QActivityModel::start(const int &row)
{
    auto error = try_catch_to_string([this, row](){
        impl_->dbBackend_->start(idByRow(row));
        auto r = records_.at(row);
        if(r->isNull(QActivityModelImpl::RoleIndex::start_time)){
            r->setValue(QActivityModelImpl::RoleIndex::start_time,
                        QString::fromStdString(to_simple_string(second_clock::local_time().time_of_day())));
        }
        r->setValue(QActivityModelImpl::RoleIndex::running, true);
        r->setValue(QActivityModelImpl::RoleIndex::run_time, QDateTime::currentDateTime());
        auto ind = index(row, 0);
        emit dataChanged(ind, ind);
    });

    return error ? QString::fromStdString(*error) : "";
}

QString QActivityModel::stop(const int &row)
{
    auto error = try_catch_to_string([this, row](){
        impl_->dbBackend_->stop(idByRow(row));
        auto r = records_.at(row);
        auto durationFromLastStart = QDateTime::currentDateTime().toSecsSinceEpoch()
                - r->value(QActivityModelImpl::RoleIndex::run_time).toDateTime().toSecsSinceEpoch();
        r->setValue(QActivityModelImpl::RoleIndex::duration_num,
                    r->value(QActivityModelImpl::RoleIndex::duration_num).toDouble()
                    + durationFromLastStart);
        r->setValue(QActivityModelImpl::RoleIndex::running, false);
        r->field(QActivityModelImpl::RoleIndex::run_time).clear();
        auto ind = index(row, 0);
        emit dataChanged(ind, ind);
    });

    return error ? QString::fromStdString(*error) : "";
}

std::shared_ptr<QSqlRecord> QActivityModel::createRecord() const
{
    auto r = make_shared<QSqlRecord>();
    r->append(QSqlField(QActivityModelImpl::roles[QActivityModelImpl::RoleIndex::description], QVariant::String));
    r->append(QSqlField(QActivityModelImpl::roles[QActivityModelImpl::RoleIndex::start_time], QVariant::String));
    r->append(QSqlField(QActivityModelImpl::roles[QActivityModelImpl::RoleIndex::end_time], QVariant::String));
    r->append(QSqlField(QActivityModelImpl::roles[QActivityModelImpl::RoleIndex::duration], QVariant::String));
    r->append(QSqlField(QActivityModelImpl::roles[QActivityModelImpl::RoleIndex::tags], QVariant::List));
    r->append(QSqlField(QActivityModelImpl::roles[QActivityModelImpl::RoleIndex::id], QVariant::Int));
    r->append(QSqlField(QActivityModelImpl::roles[QActivityModelImpl::RoleIndex::running], QVariant::Bool));
    r->append(QSqlField(QActivityModelImpl::roles[QActivityModelImpl::RoleIndex::duration_num], QVariant::Double));
    r->append(QSqlField(QActivityModelImpl::roles[QActivityModelImpl::RoleIndex::start_date], QVariant::String));
    r->setValue(QActivityModelImpl::RoleIndex::start_date,
                impl_->dateString(second_clock::local_time()));
    r->append(QSqlField(QActivityModelImpl::roles[QActivityModelImpl::RoleIndex::run_time], QVariant::DateTime));

    return r;
}

std::shared_ptr<QSqlRecord> QActivityModel::createRecord(const QSqlRecord &record)
{
    auto r = createRecord();

    double durSec = record.field(QActivityModelImpl::RoleIndex::duration).value().toDouble();

    r->setValue(QActivityModelImpl::RoleIndex::description,
                record.value(QActivityModelImpl::RoleIndex::description));
    if(!record.value(QActivityModelImpl::RoleIndex::start_time).isNull()){
        auto localStartTime = date_time::c_local_adjustor<ptime>::utc_to_local(
                    from_time_t(record.value(QActivityModelImpl::RoleIndex::start_time).toDouble()));
        r->setValue(QActivityModelImpl::RoleIndex::start_time,
                    QString::fromStdString(to_simple_string(localStartTime.time_of_day())));
        r->setValue(QActivityModelImpl::RoleIndex::start_date,
                    impl_->dateString(localStartTime));
        auto localEndTime = date_time::c_local_adjustor<ptime>::utc_to_local(
                    from_time_t(record.value(QActivityModelImpl::RoleIndex::end_time).toDouble()));
        r->setValue(QActivityModelImpl::RoleIndex::end_time,
                    QString::fromStdString(to_simple_string(localEndTime.time_of_day())));
        r->setValue(QActivityModelImpl::RoleIndex::duration,
                    QString::fromStdString(to_simple_string(seconds(durSec))));
    }
    setRecordTags(record, r);
    r->setValue(QActivityModelImpl::RoleIndex::id,
                record.value(QActivityModelImpl::RoleIndex::id));
    r->setValue(QActivityModelImpl::RoleIndex::running,
                record.value(QActivityModelImpl::RoleIndex::running));
    if(record.value(QActivityModelImpl::RoleIndex::running).toBool()){
        r->setValue(QActivityModelImpl::RoleIndex::run_time, QDateTime::currentDateTime());
    }
    r->setValue(QActivityModelImpl::RoleIndex::duration_num, durSec);
    r->field(QActivityModelImpl::RoleIndex::duration_num).setReadOnly(true);

    return r;
}

std::shared_ptr<Database::Backend::Editor::Basic> QActivityModel::dbBackend() const
{
    return impl_->dbBackend_;
}

QModelBase *QActivityModel::tagModel()
{
    return impl_->tagModel_.get();
}

void QActivityModel::timerEvent(QTimerEvent *event)
{
    int roleBase = Qt::UserRole + 1;
    QVector<int> rolesToUpdate = {
        roleBase + QActivityModelImpl::RoleIndex::end_time,
        roleBase + QActivityModelImpl::RoleIndex::duration_num,
        roleBase + QActivityModelImpl::RoleIndex::duration};

    if(event->timerId() == *impl_->timerId_){
        for(size_t row = 0; row < records_.size(); ++row){
            auto r = records_.at(row);
            if(r->value(QActivityModelImpl::RoleIndex::running).toBool()){
                r->setValue(QActivityModelImpl::RoleIndex::end_time,
                            QString::fromStdString(to_simple_string(second_clock::local_time().time_of_day())));
                auto durationFromLastStart = QDateTime::currentDateTime().toSecsSinceEpoch()
                        - r->value(QActivityModelImpl::RoleIndex::run_time).toDateTime().toSecsSinceEpoch();
                auto duration = r->value(QActivityModelImpl::RoleIndex::duration_num).toDouble()
                        + durationFromLastStart;
                r->setValue(QActivityModelImpl::RoleIndex::duration,
                            QString::fromStdString(to_simple_string(seconds(duration))));
                auto ind = index(row, 0);
                emit dataChanged(ind, ind, rolesToUpdate);
            }
        }
    }
}

}}
