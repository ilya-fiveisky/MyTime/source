#ifndef GUARD_5F21F19EB57A453BB782CE36A043E01A
#define GUARD_5F21F19EB57A453BB782CE36A043E01A

#include <functional>
#include <set>

#include <boost/signals2.hpp>

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QVariant>

namespace MyTime { namespace Database {

class SqlExecutor
{
public:
    SqlExecutor();
    explicit SqlExecutor(const std::string &connectionString);
    virtual ~SqlExecutor();

    QSqlQuery   execute(const std::string &queryStr, const std::vector<QVariant> &params = {});
    void        executeInImmediateTransaction(const std::function<void()> &f);
    void        executeInTransaction(const std::vector<std::string> &queryStrs);
    std::string dbPath() const;
    bool        isConnectionOpen() const;
    std::set<std::string>
                tables();
    void        setConnectionString(const std::string &connectionStr);

    boost::signals2::signal<void ()> dbChanged;

private:
    void        checkConnection() const;
    void        executeCheckableCommand(const std::function<bool()> &f);
    std::string getErrorString() const;

    QSqlDatabase connection_;
};

}}

#endif // GUARD_5F21F19EB57A453BB782CE36A043E01A
