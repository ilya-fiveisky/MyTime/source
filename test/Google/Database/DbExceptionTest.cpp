#include "gtest/gtest.h"

#include "Database/db_exception.hpp"

using namespace std;

using namespace MyTime;
using namespace MyTime::Database;

namespace MyTime { namespace Test {namespace Database {

TEST(DatabaseDbExceptionTest, what) {
    try{
        throw db_exception("error");
    }
    catch(const db_exception &ex){
        EXPECT_EQ(string(ex.what()), "error");
    }
}

}}}
