#ifndef GUARD_17F475B9BBD34525B319050EE5027B01
#define GUARD_17F475B9BBD34525B319050EE5027B01

#include "Basic.hpp"

namespace MyTime { namespace Database {

class SqlExecutor;

namespace Backend { namespace Editor {

class Activity : public Basic
{
public:
    explicit Activity(const std::shared_ptr<MyTime::Database::SqlExecutor> &sqlExecutor);

    void    start(const int &id);
    void    stop(const int &id);
    void    updateDescription(const int &id, const QString &description);

protected:
    virtual std::vector<std::string> getDeleteCommands(const int &id) const override;
    std::string mainName() const override;
    std::string tagName() const override;
};

}}}}
#endif // GUARD_17F475B9BBD34525B319050EE5027B01
