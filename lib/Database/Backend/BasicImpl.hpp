#ifndef GUARD_1893C382934448AA9ADB134675067090
#define GUARD_1893C382934448AA9ADB134675067090

#include <memory>
#include <vector>

#include <boost/optional.hpp>

#include <QDateTime>
#include <QString>

namespace MyTime { namespace Database {

class SqlExecutor;

namespace Backend {

class Basic;

struct BasicImpl
{
    explicit BasicImpl(const std::shared_ptr<MyTime::Database::SqlExecutor> &sqlExecutor);
    virtual ~BasicImpl();

    void                createEmptyDb();
    void                initializeDb();
    std::vector<int>    childrenTags(const int &parentId);
    void                childrenTags(const int &parentId, const int &root, std::vector<int> &aggregate);
    std::string         childrenTagsCondition(const int &tag);
    QString             constructFilteredJoinString(
            const QString &keywords, const boost::optional<int> &tag,
            const QDateTime &from, const QDateTime &to);

    std::shared_ptr<MyTime::Database::SqlExecutor> sqlExecutor_;

    static QDateTime        toUtc(const QDateTime &dt);

    static const QString CHILDREN_TAGS_QUERY;
    static const QString FILTERED_JOIN;
    static const QString END_TIME_FILTER_PLACEHOLDER;
    static const QString KEYWORD_FILTER_PLACEHOLDER;
    static const QString START_TIME_FILTER_PLACEHOLDER;
    static const QString TAG_FILTER_PLACEHOLDER;
};

}}}
#endif // GUARD_1893C382934448AA9ADB134675067090
