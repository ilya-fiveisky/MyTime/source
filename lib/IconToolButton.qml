import QtQuick 2.7
import QtQuick.Controls 2.1
import QtGraphicalEffects 1.0
import "."

ToolButton {
    property alias iconSource : mask.source
    property real iconScale : 0.7
    property alias iconColor : colorRect.color

    Rectangle {
        id: colorRect
        anchors.fill: parent
        color: Style.current.foreground
        visible: false
    }
    Image {
        id: mask
        source: iconSource
        visible: false
        sourceSize.width: 64
        sourceSize.height: 64
    }
    OpacityMask {
        anchors.centerIn: parent
        height: parent.height * iconScale
        width: parent.width * iconScale
        source: colorRect
        maskSource: mask
    }
}
