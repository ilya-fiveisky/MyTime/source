#ifndef GUARD_51871D6377AB4A51B3A600562D3CC18D
#define GUARD_51871D6377AB4A51B3A600562D3CC18D

#include <QSortFilterProxyModel>

#include "mylib_global.h"

namespace MyTime { namespace Models {

class MYLIB_EXPORT QTagIdFilterProxyModel: public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(QList<int> ids READ ids WRITE setIds NOTIFY idsChanged)

public:
    explicit QTagIdFilterProxyModel(const int &idIndex, QObject *parent = 0);

    bool filterAcceptsRow(int sourceRow, const QModelIndex &parent = QModelIndex()) const override;
    void setIds(const QList<int> newIds);

signals:
    void idsChanged();

public slots:
    QList<int> ids() const;

private:
    int idIndex_;
    QList<int> ids_;
};

}}
#endif // GUARD_51871D6377AB4A51B3A600562D3CC18D
