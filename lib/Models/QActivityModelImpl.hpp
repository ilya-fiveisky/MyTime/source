#ifndef GUARD_42C591DB82384B7EB83A15E04153EDD1
#define GUARD_42C591DB82384B7EB83A15E04153EDD1

#include <memory>
#include <vector>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/optional.hpp>

#include <QString>

class QSqlRecord;

namespace MyTime {

namespace Database { namespace Backend { namespace Editor {
class Basic;
class Activity;
}}}

namespace Models {

class QModelBase;

struct QActivityModelImpl
{
    enum RoleIndex {description, start_time, end_time, duration, tags, id,
                    running, duration_num, start_date, run_time};
    static const std::vector<QString> roles;

    explicit QActivityModelImpl(
            const std::shared_ptr<QModelBase> &tagModel,
            const std::shared_ptr<Database::Backend::Editor::Activity> &dbBackend);
    virtual ~QActivityModelImpl();

    void updateDescription(const int &row, const QString &description, const QModelBase *backPointer);

    static QString dateString(const boost::posix_time::ptime &d);

    static const QString QUERY;
    static const int TIMER_INTERVAL;

    std::shared_ptr<Database::Backend::Editor::Activity>
                                dbBackend_;
    std::shared_ptr<QModelBase> tagModel_;
    boost::optional<int>        timerId_;
};

}}
#endif // GUARD_42C591DB82384B7EB83A15E04153EDD1
