import QtQuick 2.7
import QtQuick.Controls 2.1

ToolTip {
    delay: 1000
    timeout: 5000
    visible: parent ? parent.hovered : false
}
