TEMPLATE = app

TARGET = MyTimeGoogleTests

CONFIG += console c++14 thread
CONFIG -= app_bundle

QT += core quick sql testlib

GOOGLE_TEST_SOURCE = $$(GOOGLE_TEST_ROOT)/source/googletest

!android{
    GOOGLE_TEST_BUILD = $$(GOOGLE_TEST_ROOT)/build/googlemock/gtest
}

android{
    # Using this because Q_OS_ANDROID does not work (is not defined for Android builds).
    DEFINES += MY_OS_ANDROID

    SOURCES += \
        $${GOOGLE_TEST_SOURCE}/src/gtest-all.cc
#    GOOGLE_TEST_BUILD = $$(GOOGLE_TEST_ROOT)/build-android/googlemock/gtest
}

MY_SOURCE = ../../lib

INCLUDEPATH += $${MY_SOURCE}

INCLUDEPATH += $${GOOGLE_TEST_SOURCE}
INCLUDEPATH += $${GOOGLE_TEST_SOURCE}/include

unix{
    !android:LIBS += -L$${GOOGLE_TEST_BUILD} -lgtest
}

SOURCES += \
    Models/QActivityModelTest.cpp \
    Models/QSqlRolesModelTest.cpp \
    QAnalyticsContextTest.cpp \
    main.cpp \
    MyEnvironment.cpp \
    Database/StoreTest.cpp \
    Models/BasicTest.cpp \
    BasicTest.cpp \
    Database/SqlExecutorTest.cpp \
    Database/BasicTest.cpp \
    Database/StoreImplTest.cpp \
    Database/StoreBasicTest.cpp \
    Database/Backend/BasicTest.cpp \
    Database/Backend/Editor/BasicTest.cpp \
    Database/Backend/Editor/ActivityTest.cpp \
    Database/Backend/Editor/TagTest.cpp \
    QGUIContextTest.cpp \
    Database/StoreMethodsTest.cpp \
    Models/QTagModelTest.cpp \
    Models/QDurationChartModelTest.cpp \
    Models/QActivityModelBasicTest.cpp \
    Models/QTagIdFilterProxyModelTest.cpp \
    Models/QTagNameFilterProxyModelTest.cpp \
    AndroidPrinter.cpp \
    Database/Backend/BasicImplTest.cpp \
    Database/DbExceptionTest.cpp

HEADERS += \
    MyEnvironment.hpp \
    Models/BasicTest.hpp \
    BasicTest.hpp \
    Database/BasicTest.hpp \
    Database/StoreBasicTest.hpp \
    Database/Backend/BasicMethodsTest.hpp \
    Database/StoreMethodsTest.hpp \
    Models/QActivityModelBasicTest.hpp \
    AndroidPrinter.hpp

!include ($${MY_SOURCE}/config.pri){
    message("config.pri is not included")
}

!include ($${MY_SOURCE}/libs.pri){
    message("libs.pri is not included")
}

!include ($${MY_SOURCE}/sources.pri){
    message("sources.pri is not included")
}
