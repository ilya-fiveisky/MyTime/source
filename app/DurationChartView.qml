import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import QtCharts 2.2
import "."

ChartView {
    legend.visible: true
    antialiasing: true
    theme: Style.current.theme == Style.current.Dark ? ChartView.ChartThemeDark : ChartView.ChartThemeLight
    focus: true

    onVisibleChanged: if(visible) forceActiveFocus()

    onWidthChanged: dateAxis.tickCount = ApplicationWindow.window.width / 150

    VXYModelMapper {
        model: context ? context.chartModel : null
        series: lineSeries
        xColumn: 0
        yColumn: 1

        Component.onCompleted: {
            model.modelReset.connect(valueAxis.applyNiceNumbers);
        }
    }
        
    DateTimeAxis {
        id: dateAxis
        min: context ? context.start : new Date
        max: context ? context.end : new Date
        format: "dd MMM yyyy"
    }
    
    ValueAxis {
        id: valueAxis
    }

    LineSeries {
        id: lineSeries
        name: qsTr("Duration per Day")
        axisX: dateAxis
        axisY: valueAxis
    }
}
