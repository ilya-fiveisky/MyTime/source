#include "QTagIdFilterProxyModel.hpp"

#include <algorithm>
#include <exception>

#include "my_throw.hpp"

using namespace std;

namespace MyTime { namespace Models {

QTagIdFilterProxyModel::QTagIdFilterProxyModel(const int &idIndex, QObject *parent):
    QSortFilterProxyModel(parent),
    idIndex_(idIndex)
{
    if(idIndex < 0){
        my_throw<invalid_argument>("id index is negative (" + to_string(idIndex) + ")");
    }
}

bool QTagIdFilterProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex &) const
{
    int id = sourceModel()->data(sourceModel()->index(sourceRow, 0), Qt::UserRole + idIndex_ + 1).toInt();
    return find(ids_.begin(), ids_.end(), id) != ids_.end();
}

QList<int> QTagIdFilterProxyModel::ids() const
{
    return ids_;
}

void QTagIdFilterProxyModel::setIds(const QList<int> newIds)
{
    if(newIds.length() == ids_.length()){
        return;
    }
    beginResetModel();
    ids_ = newIds;
    endResetModel();
}

}}
