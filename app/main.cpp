#include <cstdlib>
#include <exception>
#include <memory>
#include <signal.h>

#include <../lib/stdx/scope_guard/finally.h>

#include <boost/exception/diagnostic_information.hpp>
#include <boost/log/trivial.hpp>
//#include <boost/stacktrace.hpp>

#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QtGlobal>

//#if defined(Q_OS_UNIX)
//#include "client/linux/handler/exception_handler.h"
//#elif defined(Q_OS_WIN)
//#include "client/windows/handler/exception_handler.h"
//#endif

#include "QMyApplication.hpp"

using namespace std;
using namespace stdx;
using namespace boost::log;
//using namespace boost::stacktrace;
using namespace MyTime;

//void my_signal_handler(int signum) {
//    ::signal(signum, SIG_DFL);
//    safe_dump_to(
//                "./stacktrace.dump");
////                (QMyApplication::localDataPath() + "/stacktrace.dump").toStdString().c_str());
//    _Exit(-1);
//}

//void crash() { volatile int* a = (int*)(NULL); *a = 1; }

int main(int argc, char *argv[])
{
//        ::signal(SIGSEGV, &my_signal_handler);
//        ::signal(SIGABRT, &my_signal_handler);
//#ifdef Q_OS_UNIX
//    google_breakpad::MinidumpDescriptor descriptor("/tmp");
//    google_breakpad::ExceptionHandler eh(descriptor, NULL, NULL, NULL, true, -1);
//#endif
//#ifdef Q_OS_WIN
//    google_breakpad::ExceptionHandler eh(L"c:\\temp", NULL, NULL, NULL, -1);
//#endif

//    crash();

    QCoreApplication::setOrganizationName("ilya5");
    QCoreApplication::setApplicationName("MyTime");

#ifndef Q_OS_WIN
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    // According to http://www.boost.org/doc/libs/1_63_0/libs/log/doc/html/log/rationale/why_crash_on_term.html
    auto guard = finally([]
    {
        core::get()->remove_all_sinks();
    });

    try{
//        Q_INIT_RESOURCE(mylib);
        QMyApplication app(argc, argv);

        return app.exec();
    }
    catch(const boost::exception &ex){
        BOOST_LOG_TRIVIAL(fatal) << boost::diagnostic_information(ex);
        throw;
    }
    catch(...){
        BOOST_LOG_TRIVIAL(fatal) << boost::current_exception_diagnostic_information();
        throw;
    }
}
