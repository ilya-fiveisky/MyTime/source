#include "StoreImpl.hpp"

#include <exception>

#include <boost/filesystem.hpp>

#include <QDebug>
#include <QObject>
#include <QSettings>
#include <QStandardPaths>

#include "check_ptr_arg.hpp"

#include "Database/SqlExecutor.hpp"

using namespace std;
using namespace boost::filesystem;

namespace MyTime { namespace Database {

const string StoreImpl::BACKUP_FILE_EXTENSION = "backup";

const int StoreImpl::BACKUP_TIMER_INTERVAL_DEFAULT = 3600*1000;

const QString StoreImpl::LAST_DB_PATH_SETTING_NAME = "lastDbPath";

const string StoreImpl::NEW_DB_NAME_DEFAULT = unique_path("sample_%%%%").string();

StoreImpl::StoreImpl(const shared_ptr<SqlExecutor> &sqlExecutor):
    StoreImpl(sqlExecutor, make_shared<QSettings>(), temp_directory_path().string())
{
}

StoreImpl::StoreImpl(const std::shared_ptr<SqlExecutor> &sqlExecutor,
                     const std::shared_ptr<QSettings> &settings,
                     const std::string &tempDirPath,
                     const int &backupTimerInterval):
    backupTimer_{new QTimer()},
    settings_{settings},
    sqlExecutor_{sqlExecutor},
    BACKUP_TIMER_INTERVAL{backupTimerInterval},
    NEW_DB_PATH_DEFAULT{QUrl::fromLocalFile(QString::fromStdString((path(tempDirPath) /= NEW_DB_NAME_DEFAULT).string()))}
{
    check_ptr_arg(settings, "settings");
    check_ptr_arg(sqlExecutor, "sqlExecutor is NULL");
    if(!exists(tempDirPath)){
        my_throw<invalid_argument>(tempDirPath + " does not exist");
    }
    backupTimer_->setInterval(BACKUP_TIMER_INTERVAL);
    QObject::connect(backupTimer_.get(), &QTimer::timeout, [this]{
        sqlExecutor_->executeInImmediateTransaction([this]{ backup(); });
    });
}

StoreImpl::StoreImpl(const std::shared_ptr<SqlExecutor> &sqlExecutor,
                     const std::shared_ptr<QSettings> &settings,
                     const string &tempDirPath):
    StoreImpl(sqlExecutor, settings, tempDirPath, BACKUP_TIMER_INTERVAL_DEFAULT)

{
}

StoreImpl::~StoreImpl() = default;

void StoreImpl::backup()
{
    path from(sqlExecutor_->dbPath());

    if(exists(from)){
        path to(path(from).replace_extension(BACKUP_FILE_EXTENSION));
        copy_file(from, to, copy_option::overwrite_if_exists);
    }
}

void StoreImpl::startBackup()
{
    backup();
    backupTimer_->start();
}

}}
