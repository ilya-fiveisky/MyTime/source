#ifndef GUARD_56F763A90B2C44D99D20161466474244
#define GUARD_56F763A90B2C44D99D20161466474244

#include <memory>

#include <QApplication>

#include "mylib_global.h"

//namespace boost { namespace log { namespace sources {
//      class logger;
//}}}

class QQmlApplicationEngine;

namespace MyTime {

class QGUIContext;

class MYLIB_EXPORT QMyApplication : public QApplication
{
    Q_OBJECT
public:
    explicit QMyApplication(int &argc, char **argv);

    bool notify(QObject *receiver, QEvent *event) override;

    static QString localDataPath();

private:
    void initLog();

    std::shared_ptr<QQmlApplicationEngine> engine_;
    std::shared_ptr<MyTime::QGUIContext> guiContext_;
};
}
#endif // GUARD_56F763A90B2C44D99D20161466474244
