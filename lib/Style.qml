pragma Singleton
import QtQuick 2.0
import QtQuick.Controls.Material 2.0

QtObject {
    readonly property var current: Material

    readonly property color red: current.color(current.Red)

    function backgroundAltColor(level){
        if(typeof level === 'undefined') level = 1;
        return current.theme === current.Dark ?
                    Qt.lighter(current.background, 1.3*level) :
                    Qt.darker(current.background, 1.05*level);
    }

    function foregroundAltColor(level){
        if(typeof level === 'undefined') level = 1;
        return current.theme === current.Light ?
                    Qt.lighter(current.foreground, 2*level) :
                    Qt.darker(current.foreground, 1.5*level);
    }
}
