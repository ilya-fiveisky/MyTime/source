TEMPLATE = app
TARGET = tst_mytime_quick_test
CONFIG += warn_on qmltestcase
SOURCES += tst_quick.cpp

DISTFILES += \
    qml/tst_CompactMultiSelection.qml \
    qml/tst_TagEditor.qml \
    qml/tst_ActivityEditor.qml \
    qml/tst_Analytics.qml \
    qml/Analytics4Test.qml

android {
    NDK_SOURCES = $$(ANDROID_NDK_ROOT)/sources

    equals(ANDROID_TARGET_ARCH, armeabi-v7a) {
        QMAKE_LIBDIR += $${NDK_SOURCES}/crystax/libs/armeabi-v7a
    }

    ANDROID_EXTRA_LIBS += \
        $${NDK_SOURCES}/boost/1.59.0/libs/armeabi-v7a/gnu-4.9/libboost_date_time.so \
        $${NDK_SOURCES}/crystax/libs/armeabi-v7a/libcrystax.so

}
