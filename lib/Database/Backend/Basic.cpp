#include "Basic.hpp"

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QString>

#include "../db_exception.hpp"
#include "../SqlExecutor.hpp"
#include "BasicImpl.hpp"
#include "my_throw.hpp"

using namespace std;

namespace MyTime { namespace Database { namespace Backend {

QString Basic::FILTERED_JOIN_PLACEHOLDER(){ return "${filteredJoinPlaceholder}"; }

Basic::Basic(const std::shared_ptr<MyTime::Database::SqlExecutor> &sqlExecutor):
    impl_{new BasicImpl{sqlExecutor}}
{
    impl_->initializeDb();
    impl_->sqlExecutor_->dbChanged.connect([this](){
        impl_->initializeDb();
        dbChanged();
    });
}

Basic::~Basic() = default;

void Basic::checkConnection()
{
    if(!sqlExecutor()->isConnectionOpen()){
        my_throw<db_exception>("Connection is closed");
    }
}

Basic::select_return_type Basic::select(const QString &queryStr)
{
    checkConnection();
    if(queryStr.trimmed().isEmpty()){
        my_throw<invalid_argument>("Query is empty");
    }
    if(!queryStr.trimmed().startsWith("SELECT", Qt::CaseInsensitive)){
        my_throw<invalid_argument>("Not a SELECT query : " + queryStr.toStdString());
    }
//#if defined Q_OS_WIN
    auto q = impl_->sqlExecutor_->execute(queryStr.toStdString());
    select_return_type source;
    while(q.next()){
        source.push_back(q.record());
    }
//#else
//    select_return_type source(
//                [this, queryStr](coro_t::push_type & sink){
//        auto q = impl_->sqlExecutor_->execute(queryStr.toStdString());
//        while(q.next()){
//            sink(q.record());
//        }
//    });
//#endif
    return source;
}

std::shared_ptr<SqlExecutor> Basic::sqlExecutor() const
{
    return impl_->sqlExecutor_;
}

QString Basic::constructFilteredJoinString(
        const QString &keywords, const QList<int> &tags,
        const QDateTime &from, const QDateTime &to)
{
    if(tags.length() > 0){
        return accumulate(
                    std::next(tags.begin()),
                    tags.end(),
                    impl_->constructFilteredJoinString(keywords, tags[0], from, to),
                [this, keywords, from, to](const QString &a, const int &b) {
            return a + " INTERSECT " + impl_->constructFilteredJoinString(keywords, b, from, to);
        });
    }
    else{
        return impl_->constructFilteredJoinString(keywords, boost::none, from, to);
    }
}

}}}
