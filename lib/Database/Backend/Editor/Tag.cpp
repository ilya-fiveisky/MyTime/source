#include "Tag.hpp"

#include <vector>

#include <QSqlQuery>

#include "../../SqlExecutor.hpp"

using namespace std;

namespace MyTime { namespace Database { namespace Backend { namespace Editor {

Tag::Tag(const std::shared_ptr<SqlExecutor> &sqlExecutor) :
    Basic(sqlExecutor){}

void Tag::updateName(const int &id, const QString &name)
{
    checkConnection();
    sqlExecutor()->execute("UPDATE tag SET name = ? WHERE id = ?", {name, id});
}

vector<string> Tag::getDeleteCommands(const int &id) const
{
    auto idStr = to_string(id);
    vector<string> commands(Basic::getDeleteCommands(id));
    commands.insert(begin(commands),
                    "DELETE FROM tag_tag WHERE tag_id = " + idStr + " OR super_tag_id = " + idStr);
    return commands;
}

string Tag::mainName() const
{
    return "tag";
}

string Tag::tagName() const
{
    return "super_tag";
}

}}}}
