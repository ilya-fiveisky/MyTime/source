import QtQuick 2.5
import QtQuick.Controls 2.0
import "."

ToolButton {
    id: plusButton
    text: "+"
    font.bold: true

    Component.onCompleted: {
        contentItem.color = Style.current.foreground;
    }

    contentItem: Label {
        text: plusButton.text
        font: plusButton.font
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    background: Rectangle {
        color: "transparent"
        border.width: 1
        border.color: Style.foregroundAltColor()
        radius: 5
    }
}
