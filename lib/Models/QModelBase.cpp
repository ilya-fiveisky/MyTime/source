#include "QModelBase.hpp"

#include <stdx/scope_guard/finally.h>

#include <QDebug>
#include <QSqlField>
#include <QSqlQuery>
#include <QSqlRecord>

#include "try_catch_to_string.hpp"

#include "../Database/Backend/Editor/Basic.hpp"
#include "QTagIdFilterProxyModel.hpp"

using namespace std;
using namespace stdx;

namespace MyTime { namespace Models {

QModelBase::QModelBase(QString query,
                       const int &idIndex, const int &tagsIndex,
                       QObject *parent): BaseT(parent),
    idIndex_{idIndex}, tagsIndex_{tagsIndex}, query_{query}
{
}

int QModelBase::columnCount(const QModelIndex &) const
{
    return userRoleNames_.size();
}

QVariant QModelBase::data(const QModelIndex &index, int role) const
{
    QVariant value;
    if(role >= Qt::UserRole){
        int columnIdx = role - Qt::UserRole - 1;
        value = records_.at(index.row())->value(columnIdx);
    }
    return value;
}

QHash<int, QByteArray> QModelBase::roleNames() const
{
    QHash<int, QByteArray> roleNames = BaseT::roleNames();
    for(size_t i = 0; i < userRoleNames_.size(); i++){
        roleNames[Qt::UserRole + i + 1] = userRoleNames_[i].toUtf8();
    }
    return roleNames;
}

bool QModelBase::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!index.isValid()){
        return true;
    }

    if(columnFromRole(role) == tagsIndex_){
        updateTags(index.row(), value);
        emit dataChanged(index, index);
    }
    else if(value != data(index, role)){
        records_.at(index.row())->setValue(columnFromRole(role), value);
        emit dataChanged(index, index);
    }
    return true;
}

void QModelBase::setUserRoleNames(const std::vector<QString> &roleNames)
{
    userRoleNames_ = roleNames;
}


int QModelBase::rowCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : records_.size();
}

QString QModelBase::deleteRow(const int &row)
{
    auto error = try_catch_to_string([this, row](){
        beginRemoveRows(QModelIndex(), row, row);
        auto guard = finally([this]{ endRemoveRows(); });
        dbBackend()->remove(idByRow(row));
        records_.erase(records_.begin() + row);
    });

    return error ? QString::fromStdString(*error) : "";
}

int QModelBase::idByRow(const int &row) const
{
    return records_.at(row)->value(idIndex_).toInt();
}

QString QModelBase::makeNew()
{
    auto error = try_catch_to_string([this](){
        beginInsertRows(QModelIndex(), 0, 0);
        auto guard = finally([this]{ endInsertRows(); });
        auto r = createRecord();
        r->setValue(idIndex_, dbBackend()->create());
        setRecordTags(QStringList(), r);
        records_.insert(records_.begin(), r);
    });

    return error ? QString::fromStdString(*error) : "";
}

QString QModelBase::refresh()
{
    auto error = try_catch_to_string([this](){
        beginResetModel();
        auto guard = finally([this]{ endResetModel(); });
        auto result = dbBackend()->select(query_);
        records_.clear();
        for(auto r : result) {
            records_.push_back(createRecord(r));
        }
    });

    return error ? QString::fromStdString(*error) : "";
}

int QModelBase::columnFromRole(const int &role) const
{
    return role >= Qt::UserRole ? role - Qt::UserRole - 1 : 0;
}

void QModelBase::setRecordTags(const QSqlRecord &record, const shared_ptr<QSqlRecord> &r){
    QStringList supertagIdStrs = record.value(tagsIndex_).toString().split(",", QString::SkipEmptyParts);
    setRecordTags(supertagIdStrs, r);
}

void QModelBase::setRecordTags(const QStringList &supertagIdStrs, const shared_ptr<QSqlRecord> &r)
{
    QList<int> supertagIds;
    transform(supertagIdStrs.begin(), supertagIdStrs.end(), back_inserter(supertagIds),
              [](const QString &s){return s.toInt();});
    auto superTagModel = new QTagIdFilterProxyModel(tagModel()->idIndex_, this);
    superTagModel->setSourceModel(tagModel());
    superTagModel->setIds(supertagIds);
    r->setValue(tagsIndex_, QVariant::fromValue<QTagIdFilterProxyModel*>(superTagModel));
}

void QModelBase::updateTags(const int &row, const QVariant &value)
{
    int id = idByRow(row);
    QList<int> tagIds = value.value<QTagIdFilterProxyModel*>()->ids();
    dbBackend()->updateTags(id, tagIds);
}

}}
