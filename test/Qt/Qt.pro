TEMPLATE = app
TARGET = tst_test

QT       += testlib
QT       -= gui
QT += core qml quick widgets sql

CONFIG   += console
CONFIG   -= app_bundle
CONFIG += c++14 object_parallel_to_source

INCLUDEPATH += ../../lib

LIBS += -lmylib -L../../lib

SOURCES += \
    DatabaseStoreTest.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"

!include (../../lib/libs.pri){
    message("libs.pri is not included")
}
