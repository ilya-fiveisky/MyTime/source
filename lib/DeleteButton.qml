import QtQuick 2.5
import QtQuick.Controls 2.0
import "."

ToolButton {
    id: deleteButton
    text: "\u2718"

    contentItem: Label {
        text: deleteButton.text
        font: deleteButton.font
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    Component.onCompleted: contentItem.color = Style.red
}
