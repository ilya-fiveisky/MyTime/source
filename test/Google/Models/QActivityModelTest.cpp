#include "gtest/gtest.h"

#include <exception>
#include <memory>

#include <boost/date_time/local_time/local_time.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <QSignalSpy>
#include <QSqlDatabase>

#include "Database/Backend/Editor/Activity.hpp"
#include "Database/Backend/Editor/Tag.hpp"
#include "Database/SqlExecutor.hpp"
#include "Models/QActivityModel.hpp"
#include "Models/QActivityModelImpl.hpp"
#include "Models/QTagIdFilterProxyModel.hpp"
#include "Models/QTagModel.hpp"

#include "Models/QActivityModelBasicTest.hpp"

using namespace std;
using namespace boost::gregorian;
using namespace boost::local_time;
using namespace boost::posix_time;

using namespace MyTime;
using namespace MyTime::Models;

namespace MyTime { namespace Test { namespace Models {

using namespace Models;

class ModelsQActivityModelMethodsTest : public QActivityModelBasicTest {
protected:
    ModelsQActivityModelMethodsTest()
    {
        activityModel_ = make_shared<QActivityModel>(tags(), activityDbBackend());
    }

    shared_ptr<QActivityModel> activityModel() const
    {
        return activityModel_;
    }

    void checkDataIsNull(const QActivityModelImpl::RoleIndex &ind){
        EXPECT_TRUE(activityModel()->data(
                        activityModel()->index(0, ind),
                        Qt::UserRole + 1 + ind).isNull())
                << QActivityModelImpl::roles.at(ind).toStdString();
    }

    template<typename T> void compareData(
            const QActivityModelImpl::RoleIndex &ind, const T &v){
        EXPECT_EQ(activityModel()->data(
                      activityModel()->index(0, ind),
                      Qt::UserRole + 1 + ind), v)
                << QActivityModelImpl::roles.at(ind).toStdString();
    }

    template<typename T> void setData(
            const QActivityModelImpl::RoleIndex &ind, const T &v){
        EXPECT_NO_THROW(activityModel()->setData(
                            activityModel()->index(0, ind),
                            v,
                            Qt::UserRole + 1 + ind))
                << QActivityModelImpl::roles.at(ind).toStdString();
    }

    template<typename T> void setAndCompareData(
            const QActivityModelImpl::RoleIndex &ind, const T &v){
        setData(ind, v);
        compareData(ind, v);
    }

private:
    shared_ptr<QActivityModel> activityModel_;
};

typedef QActivityModelBasicTest ModelsQActivityModelConstructorTest;

TEST_F(ModelsQActivityModelConstructorTest, test) {
    EXPECT_THROW(QActivityModel m(nullptr,  nullptr),               invalid_argument);
    EXPECT_THROW(QActivityModel m(tags(),   nullptr),               invalid_argument);
    EXPECT_THROW(QActivityModel m(nullptr,  activityDbBackend()),   invalid_argument);

    EXPECT_NO_THROW(QActivityModel m(tags(),  activityDbBackend()));
}

TEST_F(ModelsQActivityModelMethodsTest, columnCount) {
    EXPECT_EQ(activityModel()->columnCount(), QActivityModelImpl::roles.size());
}

TEST_F(ModelsQActivityModelMethodsTest, rowCount) {
    EXPECT_EQ(activityModel()->rowCount(), 0);
    activityModel()->makeNew();
    EXPECT_EQ(activityModel()->rowCount(), 1);
}

TEST_F(ModelsQActivityModelMethodsTest, data) {
    activityModel()->makeNew();
    checkDataIsNull(QActivityModelImpl::RoleIndex::description);
    checkDataIsNull(QActivityModelImpl::RoleIndex::start_time);
    checkDataIsNull(QActivityModelImpl::RoleIndex::end_time);
    checkDataIsNull(QActivityModelImpl::RoleIndex::duration);
    EXPECT_TRUE(activityModel()->data(
                    activityModel()->index(0, QActivityModelImpl::RoleIndex::tags),
                    Qt::UserRole + 1 + QActivityModelImpl::RoleIndex::tags).
                value<QTagIdFilterProxyModel*>()->ids().empty());
    compareData(QActivityModelImpl::RoleIndex::id, 1);
    checkDataIsNull(QActivityModelImpl::RoleIndex::running);
    checkDataIsNull(QActivityModelImpl::RoleIndex::duration_num);
    compareData(QActivityModelImpl::RoleIndex::start_date,
                QActivityModelImpl::dateString(second_clock::local_time()));
    checkDataIsNull(QActivityModelImpl::RoleIndex::run_time);
}

TEST_F(ModelsQActivityModelMethodsTest, setDataWithoutTags) {
    activityModel()->makeNew();

    checkDataIsNull(QActivityModelImpl::RoleIndex::description);
    QString description{"some description"};
    setAndCompareData(QActivityModelImpl::RoleIndex::description, description);

    checkDataIsNull(QActivityModelImpl::RoleIndex::start_time);
    QDateTime startTime{QDateTime::currentDateTime().addDays(-1)};
    setAndCompareData(QActivityModelImpl::RoleIndex::start_time, startTime);

    checkDataIsNull(QActivityModelImpl::RoleIndex::end_time);
    QDateTime endTime{QDateTime::currentDateTime().addDays(1)};
    setAndCompareData(QActivityModelImpl::RoleIndex::end_time, endTime);

    checkDataIsNull(QActivityModelImpl::RoleIndex::duration);
    QString duration{"1000"};
    setAndCompareData(QActivityModelImpl::RoleIndex::duration, duration);

    compareData(QActivityModelImpl::RoleIndex::id, 1);
    int newId{1000};
    setAndCompareData(QActivityModelImpl::RoleIndex::id, newId);

    checkDataIsNull(QActivityModelImpl::RoleIndex::running);
    bool running{true};
    setAndCompareData(QActivityModelImpl::RoleIndex::running, running);

    checkDataIsNull(QActivityModelImpl::RoleIndex::duration_num);
    double durationNum{1000};
    setAndCompareData(QActivityModelImpl::RoleIndex::duration_num, durationNum);

    auto currDate = second_clock::local_time();
    compareData(QActivityModelImpl::RoleIndex::start_date,
                QActivityModelImpl::dateString(currDate));
    auto startDate = currDate - days(1);
    setAndCompareData(QActivityModelImpl::RoleIndex::start_date,
                QActivityModelImpl::dateString(startDate));

    checkDataIsNull(QActivityModelImpl::RoleIndex::run_time);
    QDateTime runTime{QDateTime::currentDateTime()};
    setAndCompareData(QActivityModelImpl::RoleIndex::run_time, runTime);
}

TEST_F(ModelsQActivityModelMethodsTest, setDataTags) {
    activityModel()->makeNew();
    tags()->makeNew();
    tags()->makeNew();
    EXPECT_TRUE(activityModel()->data(
                    activityModel()->index(0, QActivityModelImpl::RoleIndex::tags),
                    Qt::UserRole + 1 + QActivityModelImpl::RoleIndex::tags).
                value<QTagIdFilterProxyModel*>()->ids().empty());
    auto superTagModel = new QTagIdFilterProxyModel(QActivityModelImpl::RoleIndex::id, activityModel().get());
    superTagModel->setSourceModel(tags().get());
    superTagModel->setIds({2});
    setData(QActivityModelImpl::RoleIndex::tags,
            QVariant::fromValue<QTagIdFilterProxyModel*>(superTagModel));
    activityModel()->refresh();
    EXPECT_EQ(activityModel()->data(
                  activityModel()->index(0, QActivityModelImpl::RoleIndex::tags),
                  Qt::UserRole + 1 + QActivityModelImpl::RoleIndex::tags).
              value<QTagIdFilterProxyModel*>()->ids(),
              superTagModel->ids());
}

TEST_F(ModelsQActivityModelMethodsTest, deleteRowException) {
    invalidateDbConnection();
    QSignalSpy spy(activityModel().get(), SIGNAL(rowsRemoved(const QModelIndex&,int,int)));
    EXPECT_EQ(spy.count(), 0);
    ASSERT_FALSE(activityModel()->deleteRow(0).isEmpty());
    EXPECT_EQ(spy.count(), 1);
}

TEST_F(ModelsQActivityModelMethodsTest, deleteRow) {
    EXPECT_FALSE(activityModel()->deleteRow(-1).isEmpty());
    EXPECT_EQ(activityModel()->rowCount(), 0);
    activityModel()->makeNew();
    EXPECT_EQ(activityModel()->rowCount(), 1);
    auto result = activityDbBackend()->select("SELECT id FROM activity");
    vector<QSqlRecord> records{begin(result), end(result)};
    ASSERT_EQ(records.size(), 1);
    QSignalSpy spy(activityModel().get(), SIGNAL(rowsRemoved(const QModelIndex&,int,int)));
    EXPECT_EQ(spy.count(), 0);
    QString error = activityModel()->deleteRow(0);
    EXPECT_TRUE(error.isEmpty()) << error.toStdString();
    EXPECT_EQ(spy.count(), 1);
    EXPECT_EQ(activityModel()->rowCount(), 0);
    result = activityDbBackend()->select("SELECT id FROM activity");
    records.assign(begin(result), end(result));
    ASSERT_EQ(records.size(), 0);
}

TEST_F(ModelsQActivityModelMethodsTest, makeNewException) {
    invalidateDbConnection();
    QSignalSpy spy(activityModel().get(), SIGNAL(rowsInserted(const QModelIndex&,int,int)));
    EXPECT_EQ(spy.count(), 0);
    ASSERT_FALSE(activityModel()->makeNew().isEmpty());
    EXPECT_EQ(spy.count(), 1);
}

TEST_F(ModelsQActivityModelMethodsTest, makeNew) {
    QSignalSpy spy(activityModel().get(), SIGNAL(rowsInserted(const QModelIndex&,int,int)));
    EXPECT_EQ(spy.count(), 0);
    EXPECT_EQ(activityModel()->rowCount(), 0);
    auto error = activityModel()->makeNew();
    ASSERT_TRUE(error.isEmpty()) << error.toStdString();
    EXPECT_EQ(spy.count(), 1);
    EXPECT_EQ(activityModel()->rowCount(), 1);
    auto result = activityDbBackend()->select("SELECT id FROM activity");
    vector<QSqlRecord> records{begin(result), end(result)};
    ASSERT_EQ(records.size(), 1);
    EXPECT_EQ(records.at(0).value("id").toInt(), 1);
}

TEST_F(ModelsQActivityModelMethodsTest, refreshException) {
    invalidateDbConnection();
    QSignalSpy spy(activityModel().get(), SIGNAL(modelReset()));
    EXPECT_EQ(spy.count(), 0);
    ASSERT_FALSE(activityModel()->refresh().isEmpty());
    EXPECT_EQ(spy.count(), 1);
}

TEST_F(ModelsQActivityModelMethodsTest, refresh) {
    EXPECT_EQ(activityModel()->rowCount(), 0);
    activityDbBackend()->create();
    EXPECT_EQ(activityModel()->rowCount(), 0);
    QSignalSpy spy(activityModel().get(), SIGNAL(modelReset()));
    EXPECT_EQ(spy.count(), 0);
    auto error = activityModel()->refresh();
    ASSERT_TRUE(error.isEmpty()) << error.toStdString();
    EXPECT_EQ(activityModel()->rowCount(), 1);
    EXPECT_EQ(spy.count(), 1);
}

TEST_F(ModelsQActivityModelMethodsTest, start) {
    EXPECT_FALSE(activityModel()->start(-1).isEmpty());
    activityModel()->makeNew();
    compareData(QActivityModelImpl::RoleIndex::running, false);
    auto error = activityModel()->start(0);
    ASSERT_TRUE(error.isEmpty()) << error.toStdString();
    compareData(QActivityModelImpl::RoleIndex::running, true);
}

TEST_F(ModelsQActivityModelMethodsTest, stop) {
    EXPECT_FALSE(activityModel()->stop(-1).isEmpty());
    activityModel()->makeNew();
    activityModel()->start(0);
    compareData(QActivityModelImpl::RoleIndex::running, true);
    auto error = activityModel()->stop(0);
    ASSERT_TRUE(error.isEmpty()) << error.toStdString();
    compareData(QActivityModelImpl::RoleIndex::running, false);
}

TEST_F(ModelsQActivityModelMethodsTest, roleNames) {
    EXPECT_EQ(activityModel()->roleNames().count(), 16);
    EXPECT_EQ(activityModel()->roleNames()[Qt::UserRole + 1], "description");
    EXPECT_EQ(activityModel()->roleNames()[Qt::UserRole + 2], "start");
    EXPECT_EQ(activityModel()->roleNames()[Qt::UserRole + 3], "end");
    EXPECT_EQ(activityModel()->roleNames()[Qt::UserRole + 4], "duration");
    EXPECT_EQ(activityModel()->roleNames()[Qt::UserRole + 5], "tags");
    EXPECT_EQ(activityModel()->roleNames()[Qt::UserRole + 6], "activity_id");
    EXPECT_EQ(activityModel()->roleNames()[Qt::UserRole + 7], "running");
    EXPECT_EQ(activityModel()->roleNames()[Qt::UserRole + 8], "duration_num");
    EXPECT_EQ(activityModel()->roleNames()[Qt::UserRole + 9], "start_date");
    EXPECT_EQ(activityModel()->roleNames()[Qt::UserRole + 10], "run_time");
}

}}}
