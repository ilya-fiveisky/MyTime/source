#include "Store.hpp"

#include <exception>
#include <string>
#include <vector>

#include <boost/filesystem.hpp>
#include <boost/log/attributes/named_scope.hpp>
#include <boost/log/trivial.hpp>

#include <QDebug>
#include <QSettings>
#include <QSqlError>

#include "Database/db_exception.hpp"
#include "Database/SqlExecutor.hpp"
#include "my_throw.hpp"
#include "StoreImpl.hpp"

using namespace std;
using namespace boost::filesystem;

namespace MyTime { namespace Database {

Store::Store(const std::shared_ptr<SqlExecutor> &sqlExecutor):
    impl_{new StoreImpl{sqlExecutor}}
{}

Store::Store(const std::shared_ptr<SqlExecutor> &sqlExecutor,
             const std::shared_ptr<QSettings> &settings,
             const std::string &tempDirPath):
    impl_{new StoreImpl{sqlExecutor, settings, tempDirPath}}
{
}

Store::Store(const std::shared_ptr<SqlExecutor> &sqlExecutor,
             const std::shared_ptr<QSettings> &settings,
             const string &tempDirPath,
             const int &backupTimerInterval):
    impl_{new StoreImpl{sqlExecutor, settings, tempDirPath, backupTimerInterval}}
{

}

Store::~Store() = default;

void Store::newDb(const QUrl &fileUrl)
{
    BOOST_LOG_FUNCTION();
    if(exists(fileUrl.toLocalFile().toStdString())){
        my_throw<invalid_argument>(fileUrl.toLocalFile().toStdString() + " already exists");
    }
    impl_->sqlExecutor_->setConnectionString(fileUrl.toLocalFile().toStdString());
    open(fileUrl);
}

void Store::open()
{
    auto path = impl_->settings_->value(StoreImpl::LAST_DB_PATH_SETTING_NAME);
    if(path.isNull() || !exists(path.toUrl().toLocalFile().toStdString())){
        newDb(impl_->NEW_DB_PATH_DEFAULT);
    }
    else {
        open(path.toUrl());
    }
}

void Store::open(const QUrl &fileUrl)
{
    BOOST_LOG_FUNCTION();
    if(!exists(fileUrl.toLocalFile().toStdString())){
        my_throw<invalid_argument>(fileUrl.toLocalFile().toStdString() + " does not exist");
    }

    BOOST_LOG_TRIVIAL(info) << "Start opening " << fileUrl.toDisplayString().toStdString();

    impl_->sqlExecutor_->setConnectionString(fileUrl.toLocalFile().toStdString());
    impl_->settings_->setValue(StoreImpl::LAST_DB_PATH_SETTING_NAME, fileUrl);
    impl_->startBackup();

    BOOST_LOG_TRIVIAL(info) << "Opened " << fileUrl.toDisplayString().toStdString();
}

QString Store::path()
{
    return QString::fromStdString(impl_->sqlExecutor_->dbPath());
}

void Store::saveAs(const QUrl &fileUrl)
{
    if(!exists(path().toStdString())){
        my_throw<invalid_argument>(path().toStdString() + " does not exist");
    }
    copy_file(
                path().toStdString(),
                fileUrl.toLocalFile().toStdString(),
                copy_option::overwrite_if_exists);
    open(fileUrl);
}

}}
