#include "QGUIContextImpl.hpp"

#include "try_catch_to_string.hpp"

#include "Database/Store.hpp"
#include "QGUIContext.hpp"

using namespace std;

namespace MyTime {

QGUIContextImpl::QGUIContextImpl(std::shared_ptr<Database::Store> store): store_(store)
{

}

QGUIContextImpl::~QGUIContextImpl() = default;

QString QGUIContextImpl::doActionWithStringResult(const std::function<void ()> &action,
                                                  QGUIContext *backPtr)
{
    auto error = try_catch_to_string(action);
    if(error){
        return QString::fromStdString(*error);
    }
    else{
        backPtr->update_dbPath(store_->path());
        return "";
    }
}

}
