DEFINES += BOOST_ALL_DYN_LINK BOOST_FILESYSTEM_NO_DEPRECATED

# Added in order to resolve the problem with copy_file (https://svn.boost.org/trac/boost/ticket/10038)
DEFINES += BOOST_NO_CXX11_SCOPED_ENUMS

unix:BOOST_LIB_LIST = -lboost_context -lboost_date_time -lboost_filesystem -lboost_log -lboost_log_setup -lboost_regex -lboost_system -lboost_thread

INCLUDEPATH += $$top_srcdir/stacktrace/include
#DEFINES += BOOST_STACKTRACE_USE_BACKTRACE
#LIBS += -ldl -lbacktrace

!android {
    win32 {
        !msvc {
            BOOST_LIB_LIST = -lboost_context-mt.dll -lboost_date_time-mt.dll -lboost_filesystem-mt.dll -lboost_log-mt.dll -lboost_log_setup-mt.dll -lboost_regex-mt.dll -lboost_system-mt.dll -lboost_thread_win32-mt.dll
        } else {
            BOOST_LIB_LIST = \
                -lboost_atomic-vc140-mt-1_63 \
                -lboost_chrono-vc140-mt-1_63 \
                -lboost_context-vc140-mt-1_63 \
                -lboost_coroutine-vc140-mt-1_63 \
                -lboost_date_time-vc140-mt-1_63 \
                -lboost_filesystem-vc140-mt-1_63 \
                -lboost_log-vc140-mt-1_63 \
                -lboost_log_setup-vc140-mt-1_63 \
                -lboost_regex-vc140-mt-1_63 \
                -lboost_system-vc140-mt-1_63 \
                -lboost_thread-vc140-mt-1_63

        }
    }
    LIBS += -L$$(BOOST_LIB) $${BOOST_LIB_LIST}
    INCLUDEPATH += $$(BOOST_INCLUDE)
}

android {
    NDK_SOURCES = $$(NDK_ROOT)/sources

    INCLUDEPATH += \
        $${NDK_SOURCES}/cxx-stl/gnu-libstdc++/4.9/include \
        $${NDK_SOURCES}/boost/1.59.0/include

    equals(ANDROID_TARGET_ARCH, armeabi-v7a) {
        BOOST_LIB_DIR = $${NDK_SOURCES}/boost/1.59.0/libs/armeabi-v7a/gnu-4.9
        STL_LIB_DIR = $${NDK_SOURCES}/cxx-stl/gnu-libstdc++/4.9/libs/armeabi-v7a
        CRYSTAX_LIB_DIR = $${NDK_SOURCES}/crystax/libs/armeabi-v7a
    }

    LIBS += -L$${STL_LIB_DIR} -lgnustl_shared
    LIBS += -L$${BOOST_LIB_DIR} $${BOOST_LIB_LIST}
    LIBS += -L$${CRYSTAX_LIB_DIR} -lcrystax

    ANDROID_EXTRA_LIBS += \
        $${BOOST_LIB_DIR}/libboost_chrono.so \
        $${BOOST_LIB_DIR}/libboost_context.so \
        $${BOOST_LIB_DIR}/libboost_date_time.so \
        $${BOOST_LIB_DIR}/libboost_filesystem.so \
        $${BOOST_LIB_DIR}/libboost_log.so \
        $${BOOST_LIB_DIR}/libboost_log_setup.so \
        $${BOOST_LIB_DIR}/libboost_regex.so \
        $${BOOST_LIB_DIR}/libboost_system.so \
        $${BOOST_LIB_DIR}/libboost_thread.so \
        $${CRYSTAX_LIB_DIR}/libcrystax.so
}
