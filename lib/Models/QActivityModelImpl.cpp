#include "QActivityModelImpl.hpp"

#include <sstream>

#include <boost/date_time/posix_time/posix_time.hpp>

#include <QDebug>

#include "check_ptr_arg.hpp"

#include "../Database/Backend/Editor/Activity.hpp"
#include "QModelBase.hpp"
#include "QTagIdFilterProxyModel.hpp"

namespace MyTime { namespace Models {

using namespace std;
using namespace boost::posix_time;

const QString QActivityModelImpl::QUERY =
        "SELECT "
        "   description, "
        "   start, "
        "   end, "
        "   duration, "
        "   group_concat(tag.id, ',') AS tags, "
        "   temp.id AS activity_id, "
        "   running "
        "FROM "
        "("
        "   SELECT "
        "       activity.id AS id, "
        "       activity.description as description, "
        "       MIN(interval.start) AS start, "
        "       MAX(ifnull(interval.end, strftime('%s', 'now'))) AS end, "
        "       SUM(ifnull(interval.end, strftime('%s', 'now')) - interval.start) AS duration,"
        "       MAX(interval.start NOTNULL AND interval.end ISNULL) AS running"
        "   FROM activity LEFT OUTER JOIN interval "
        "   ON activity.id = interval.activity_id "
        "   GROUP BY activity.id "
        ") AS temp "
        "LEFT OUTER JOIN activity_tag "
        "ON temp.id = activity_tag.activity_id "
        "LEFT OUTER JOIN tag "
        "ON tag.id = activity_tag.tag_id "
        "GROUP BY temp.id "
        "ORDER BY temp.id DESC ";

const vector<QString> QActivityModelImpl::roles =
{"description", "start", "end", "duration", "tags", "activity_id",
 "running", "duration_num", "start_date", "run_time"};

const int QActivityModelImpl::TIMER_INTERVAL = 1000;

QActivityModelImpl::QActivityModelImpl(const std::shared_ptr<QModelBase> &tagModel,
                                       const std::shared_ptr<Database::Backend::Editor::Activity> &dbBackend):
    dbBackend_{dbBackend}, tagModel_{tagModel}
{
    check_ptr_arg(dbBackend, "db backend");
    check_ptr_arg(tagModel, "tag model");
}

QString QActivityModelImpl::dateString(const ptime &d)
{
    time_facet* facet(new time_facet("%A %B %e, %Y"));
    stringstream ss;
    ss.imbue(std::locale(std::locale::classic(), facet));
    ss << d;
    return QString::fromStdString(ss.str());
}

QActivityModelImpl::~QActivityModelImpl() = default;

void QActivityModelImpl::updateDescription(const int &row, const QString &description, const QModelBase *backPointer)
{
    dbBackend_->updateDescription(backPointer->idByRow(row), description);
}

}}
