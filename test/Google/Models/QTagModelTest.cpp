#include "gtest/gtest.h"

#include <exception>
#include <memory>

#include <QSqlDatabase>

#include "Database/Backend/Editor/Tag.hpp"
#include "Database/SqlExecutor.hpp"
#include "Models/QTagIdFilterProxyModel.hpp"
#include "Models/QTagModel.hpp"

#include "../BasicTest.hpp"

using namespace std;

using namespace MyTime;
using namespace MyTime::Models;

namespace MyTime { namespace Test { namespace Models {

using namespace Models;

class ModelsQTagModelBasicTest : public Test::BasicTest {
protected:
    ModelsQTagModelBasicTest()
    {
        tagDbBackend_ = make_shared<Database::Backend::Editor::Tag>(sqlExecutor());
    }

    shared_ptr<Database::Backend::Editor::Tag> tagDbBackend() const
    {
        return tagDbBackend_;
    }

private:
    shared_ptr<Database::Backend::Editor::Tag> tagDbBackend_;
};

class ModelsQTagModelMethodsTest : public ModelsQTagModelBasicTest {
protected:
    ModelsQTagModelMethodsTest()
    {
        sqlExecutor()->setConnectionString(":memory:");
        tagModel_ = make_shared<QTagModel>(tagDbBackend());
    }

    shared_ptr<QTagModel> tagModel() const
    {
        return tagModel_;
    }

private:
    shared_ptr<QTagModel> tagModel_;
};

typedef ModelsQTagModelBasicTest ModelsQTagModelConstructorTest;

TEST_F(ModelsQTagModelConstructorTest, test) {
    EXPECT_THROW(QTagModel m(nullptr), invalid_argument);

    EXPECT_NO_THROW(QTagModel m(tagDbBackend()));
}

TEST_F(ModelsQTagModelMethodsTest, columnCount) {
    EXPECT_EQ(tagModel()->columnCount(), 3);
}

TEST_F(ModelsQTagModelMethodsTest, rowCount) {
    EXPECT_EQ(tagModel()->rowCount(), 0);
    tagModel()->makeNew();
    EXPECT_EQ(tagModel()->rowCount(), 1);
}

TEST_F(ModelsQTagModelMethodsTest, data) {
    tagModel()->makeNew();
    EXPECT_TRUE(tagModel()->data(
                    tagModel()->index(0, QTagModel::RoleIndex::name),
                    Qt::UserRole + 1 + QTagModel::RoleIndex::name).isNull());
    EXPECT_TRUE(tagModel()->data(
                    tagModel()->index(0, QTagModel::RoleIndex::supertags),
                    Qt::UserRole + 1 + QTagModel::RoleIndex::supertags).
                value<QTagIdFilterProxyModel*>()->ids().empty());
    EXPECT_EQ(tagModel()->data(
                  tagModel()->index(0, QTagModel::RoleIndex::id),
                  Qt::UserRole + 1 + QTagModel::RoleIndex::id), 1);
}

TEST_F(ModelsQTagModelMethodsTest, setDataWithoutTags) {
    tagModel()->makeNew();

    EXPECT_TRUE(tagModel()->data(
                    tagModel()->index(0, QTagModel::RoleIndex::name),
                    Qt::UserRole + 1 + QTagModel::RoleIndex::name).isNull());
    QString name{"some name"};
    EXPECT_NO_THROW(tagModel()->setData(
                        tagModel()->index(0, QTagModel::RoleIndex::name),
                        name,
                        Qt::UserRole + 1 + QTagModel::RoleIndex::name));
    EXPECT_EQ(tagModel()->data(
                    tagModel()->index(0, QTagModel::RoleIndex::name),
                    Qt::UserRole + 1 + QTagModel::RoleIndex::name), name);

    EXPECT_EQ(tagModel()->data(
                  tagModel()->index(0, QTagModel::RoleIndex::id),
                  Qt::UserRole + 1 + QTagModel::RoleIndex::id), 1);
    int newId{1000};
    EXPECT_NO_THROW(tagModel()->setData(
                        tagModel()->index(0, QTagModel::RoleIndex::id),
                        newId,
                        Qt::UserRole + 1 + QTagModel::RoleIndex::id));
    EXPECT_EQ(tagModel()->data(
                    tagModel()->index(0, QTagModel::RoleIndex::id),
                    Qt::UserRole + 1 + QTagModel::RoleIndex::id).toInt(), newId);
}

TEST_F(ModelsQTagModelMethodsTest, setDataTags) {
    tagModel()->makeNew();
    tagModel()->makeNew();
    tagModel()->refresh();
    EXPECT_TRUE(tagModel()->data(
                    tagModel()->index(0, QTagModel::RoleIndex::supertags),
                    Qt::UserRole + 1 + QTagModel::RoleIndex::supertags).
                value<QTagIdFilterProxyModel*>()->ids().empty());
    auto superTagModel = new QTagIdFilterProxyModel(QTagModel::RoleIndex::id, tagModel().get());
    superTagModel->setSourceModel(tagModel().get());
    superTagModel->setIds({2});
    EXPECT_NO_THROW(tagModel()->setData(
                        tagModel()->index(0, QTagModel::RoleIndex::supertags),
                        QVariant::fromValue<QTagIdFilterProxyModel*>(superTagModel),
                        Qt::UserRole + 1 + QTagModel::RoleIndex::supertags));
    tagModel()->refresh();
    EXPECT_EQ(tagModel()->data(
                    tagModel()->index(0, QTagModel::RoleIndex::supertags),
                    Qt::UserRole + 1 + QTagModel::RoleIndex::supertags).
              value<QTagIdFilterProxyModel*>()->ids(),
              superTagModel->ids());
}

TEST_F(ModelsQTagModelMethodsTest, deleteRow) {
    EXPECT_FALSE(tagModel()->deleteRow(-1).isEmpty());
    EXPECT_EQ(tagModel()->rowCount(), 0);
    tagModel()->makeNew();
    EXPECT_EQ(tagModel()->rowCount(), 1);
    auto result = tagDbBackend()->select("SELECT id FROM tag");
    vector<QSqlRecord> records{begin(result), end(result)};
    ASSERT_EQ(records.size(), 1);
    QString error = tagModel()->deleteRow(0);
    EXPECT_TRUE(error.isEmpty()) << error.toStdString();
    EXPECT_EQ(tagModel()->rowCount(), 0);
    result = tagDbBackend()->select("SELECT id FROM tag");
    records.assign(begin(result), end(result));
    ASSERT_EQ(records.size(), 0);
}

TEST_F(ModelsQTagModelMethodsTest, makeNew) {
    EXPECT_EQ(tagModel()->rowCount(), 0);
    auto error = tagModel()->makeNew();
    ASSERT_TRUE(error.isEmpty()) << error.toStdString();
    EXPECT_EQ(tagModel()->rowCount(), 1);
    auto result = tagDbBackend()->select("SELECT id FROM tag");
    vector<QSqlRecord> records{begin(result), end(result)};
    ASSERT_EQ(records.size(), 1);
    EXPECT_EQ(records.at(0).value("id").toInt(), 1);
}

TEST_F(ModelsQTagModelMethodsTest, refresh) {
    EXPECT_EQ(tagModel()->rowCount(), 0);
    tagDbBackend()->create();
    EXPECT_EQ(tagModel()->rowCount(), 0);
    auto error = tagModel()->refresh();
    ASSERT_TRUE(error.isEmpty()) << error.toStdString();
    EXPECT_EQ(tagModel()->rowCount(), 1);
}

TEST_F(ModelsQTagModelMethodsTest, roleNames) {
    EXPECT_EQ(tagModel()->roleNames().count(), 9);
    EXPECT_EQ(tagModel()->roleNames()[Qt::UserRole + 1], "id");
    EXPECT_EQ(tagModel()->roleNames()[Qt::UserRole + 2], "name");
    EXPECT_EQ(tagModel()->roleNames()[Qt::UserRole + 3], "supertags");
}

}}}
