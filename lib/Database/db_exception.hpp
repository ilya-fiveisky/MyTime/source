#ifndef GUARD_D05BCC4CE1704BA5994FC12AEDF067BB
#define GUARD_D05BCC4CE1704BA5994FC12AEDF067BB

#include <stdexcept>

namespace MyTime { namespace Database {

class db_exception: public std::runtime_error {
    using std::runtime_error::runtime_error;
};

}}
#endif // GUARD_D05BCC4CE1704BA5994FC12AEDF067BB
