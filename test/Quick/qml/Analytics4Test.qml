import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import QtQuick.Window 2.2
// Just a copy of original Analytics.qml except for this import
import "../../../lib"

Page {
    id: root
    anchors.fill: parent
    padding: 10

    property QtObject context: null
    // Made in order to fix Qt Quick Test crash on ChartView usage.
    property alias durationChartView: chartLoader.sourceComponent

    Flickable {
        id: flickable
        anchors.fill: parent
        contentHeight: 1000
        contentWidth: 1000

        Flow {
            spacing: 10
            width: ApplicationWindow.window.width

            GroupBox {
                title: qsTr("Filter Conditions") + ":"

                ColumnLayout {
                    spacing: 5
                    width: flickable.contentWidth

                    GridLayout {
                        columns: 2
                        rows: 2

                        Label {text: qsTr("Search keywords") + ":"}
                        Label {text: qsTr("Tags") + ":"}

                        TextField {
                            id: searchKeywords
                            objectName: "searchKeywords"
                            placeholderText: "Enter search keywords"
                            Layout.preferredWidth: 200
                        }
                        CompactMultiSelection {
                            id: tags
                            Layout.preferredWidth: 300
                            selectorModel: context ? context.newTagSelectorModel : null
                            selected: context ? context.tagSelectorModel : null
                            elementName: "Tag"
                        }

                    }

                    RowLayout {
                        spacing: 5
                        Label {
                            text: qsTr("From")
                        }
                        DateSelector {
                            id: from
                            objectName: "from"
                            selectedDate: new Date((new Date).setMonth((new Date).getMonth() - 1))
                            MyToolTip {text: qsTr("Select start of filter interval")}
                        }

                        Label {
                            text: qsTr("To")
                        }
                        DateSelector {
                            id: to
                            objectName: "to"
                            selectedDate: new Date((new Date).setDate((new Date).getDate() + 1))
                            MyToolTip {text: qsTr("Select end of filter interval")}
                        }
                    }

                    Button {
                        objectName: "submitButton"
                        text: qsTr("Submit")
                        MyToolTip {text: qsTr("Apply filter conditions")}

                        onClicked: {
                            context.update(
                                        searchKeywords.text,
                                        tags.selected.ids,
                                        from.selectedDate,
                                        to.selectedDate);
                        }
                    }
                }
            }

            GroupBox {
                title: qsTr("General Info") + ":"

                GridLayout {
                    columns: 2

                    Label {text: qsTr("Start (of first activity)") + ": "}
                    Label {text: context ? formatDatetime(context.start) : ""}

                    Label {text: qsTr("End (of last activity)") + ": "}
                    Label {text: context ? formatDatetime(context.end) : ""}
                }
            }
            GroupBox {
                title: qsTr("Durations") + ":"
                GridLayout {
                    columns: 2

                    Label {text: qsTr("Filter (from-to) interval") + ": "}
                    Label {text: context ? context.filterInterval : ""}

                    Label {text: qsTr("Start-End interval") + ": "}
                    Label {text: context ? context.startEndInterval : ""}

                    Label {text: qsTr("Activities Sum") + ": "}
                    Label {text: context ? context.durationSum : ""}

                    Label {text: qsTr("Average (per day in start-end interval)") + ": "}
                    Label {text: context ? context.average : ""}

                    Label {text: qsTr("Average (per day in filter interval)") + ": "}
                    Label {text: context ? context.averageFilter : ""}
                }
            }

            RowLayout {
                height: ApplicationWindow.window.height / 2
                width: ApplicationWindow.window.width
                Loader {
                    id: chartLoader
                    property QtObject context: root.context
                    Layout.minimumHeight: 300
                    Layout.preferredHeight: 400
                    Layout.fillHeight: true
                    Layout.minimumWidth: 600
                    Layout.fillWidth: true
                    sourceComponent: durationChartView
                }
            }
        }
    }

    function formatDatetime(date){
        return date.toLocaleString(Qt.locale(), "dd MMM yyyy HH:mm:ss");
    }
}
