#include "gtest/gtest.h"

#include <exception>
#include <memory>

#include "Database/Backend/Editor/Tag.hpp"
#include "Database/SqlExecutor.hpp"
#include "Models/QTagIdFilterProxyModel.hpp"
#include "Models/QTagModel.hpp"

#include "BasicTest.hpp"

using namespace std;

using namespace MyTime;
using namespace MyTime::Models;

namespace MyTime { namespace Test { namespace Models {

using namespace Models;

typedef BasicTest ModelsQTagIdFilterProxyModelBasicTest;

class ModelsQTagIdFilterProxyModelMethodsTest : public ModelsQTagIdFilterProxyModelBasicTest {
protected:
    ModelsQTagIdFilterProxyModelMethodsTest()
    {
        tagFilterModel_ = make_shared<QTagIdFilterProxyModel>(QTagModel::RoleIndex::id);
        tagFilterModel_->setSourceModel(tags().get());
    }

    shared_ptr<QTagIdFilterProxyModel> tagFilterModel() const
    {
        return tagFilterModel_;
    }

private:
    shared_ptr<QTagIdFilterProxyModel> tagFilterModel_;
};

typedef ModelsQTagIdFilterProxyModelBasicTest ModelsQTagIdFilterProxyModelConstructorTest;

TEST_F(ModelsQTagIdFilterProxyModelConstructorTest, test) {
    EXPECT_THROW(QTagIdFilterProxyModel m(-1), invalid_argument);

    EXPECT_NO_THROW(QTagIdFilterProxyModel m(1));
}

TEST_F(ModelsQTagIdFilterProxyModelMethodsTest, filterAcceptsRow) {
    tags()->makeNew();
    tags()->makeNew();
    tagFilterModel()->setIds({2});
    EXPECT_EQ(tagFilterModel()->rowCount(), 1);
    EXPECT_TRUE(tagFilterModel()->filterAcceptsRow(0));
    EXPECT_FALSE(tagFilterModel()->filterAcceptsRow(1));
}

TEST_F(ModelsQTagIdFilterProxyModelMethodsTest, setIds) {
    QList<int> ids{1, 2};
    EXPECT_EQ(tagFilterModel()->ids().size(), 0);
    tagFilterModel()->setIds(ids);
    EXPECT_EQ(tagFilterModel()->ids().size(), 2);
    EXPECT_EQ(tagFilterModel()->ids(), ids);
}

}}}
