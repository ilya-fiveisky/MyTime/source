#include "StoreMethodsTest.hpp"

#include "Database/Store.hpp"
#include "Database/StoreImpl.hpp"

using namespace std;
using namespace boost::filesystem;

using namespace MyTime;
using namespace MyTime::Database;

namespace MyTime { namespace Test { namespace Database {

void StoreMethodsTest::SetUp()
{
    BasicTest::SetUp();
    store_ = make_shared<Store>(sqlExecutor(), SETTINGS, TEMP_TEST_DIR.string());
}

shared_ptr<Store> StoreMethodsTest::store() const
{
    return store_;
}

void StoreMethodsTest::checkSettings()
{
    checkSettings(dbPath());
}

void StoreMethodsTest::checkSettings(const boost::filesystem::path &dbPath)
{
    SETTINGS->sync();
    ASSERT_TRUE(exists(SETTINGS->fileName().toStdString()))
            << "Settings file " << SETTINGS->fileName().toStdString() << " does not exist.";
    ASSERT_FALSE(SETTINGS->value(StoreImpl::LAST_DB_PATH_SETTING_NAME).isNull());
    EXPECT_EQ(SETTINGS->value(StoreImpl::LAST_DB_PATH_SETTING_NAME).toUrl().toLocalFile().toStdString(),
              dbPath.string());
}

}}}
