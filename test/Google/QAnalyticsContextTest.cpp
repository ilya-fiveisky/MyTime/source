#include "gtest/gtest.h"

#include <QDateTime>
#include <QSqlDatabase>

#include "QAnalyticsContext.hpp"
#include "Database/Backend/Basic.hpp"
#include "Database/Backend/Editor/Activity.hpp"
#include "Database/Backend/Editor/Tag.hpp"
#include "Database/SqlExecutor.hpp"
#include "Models/QDurationChartModel.hpp"
#include "Models/QTagIdFilterProxyModel.hpp"
#include "Models/QTagModel.hpp"
#include "Models/QTagNameFilterProxyModel.hpp"

#include "Models/BasicTest.hpp"

using namespace std;

using namespace MyTime;
using namespace MyTime::Models;

namespace MyTime { namespace Test {

class QAnalyticsContextBasicTest : public MyTime::Test::Models::BasicTest {
protected:
    QAnalyticsContextBasicTest()
    {
        dbBackend_ = make_shared<Database::Backend::Basic>(sqlExecutor());
    }

    shared_ptr<Database::Backend::Basic> dbBackend() const
    {
        return dbBackend_;
    }

private:
    shared_ptr<Database::Backend::Basic> dbBackend_;
};

class QAnalyticsContextMethodsTest : public QAnalyticsContextBasicTest {
protected:
    QAnalyticsContextMethodsTest()
    {
        activityDbBackend_ = make_shared<Database::Backend::Editor::Activity>(sqlExecutor());

        int activity1Id = activityDbBackend()->create();
        activityDbBackend()->updateDescription(activity1Id, "Activity 1");
        int activity2Id = activityDbBackend()->create();
        activityDbBackend()->updateDescription(activity2Id, "Activity 2");
        int activity3Id = activityDbBackend()->create();
        activityDbBackend()->updateDescription(activity3Id, "Activity 3");
        int activity4Id = activityDbBackend()->create();
        activityDbBackend()->updateDescription(activity4Id, "Activity 4");

        sqlExecutor()->executeInTransaction
                ({
                     "INSERT INTO interval (activity_id, start, end) "
                     "VALUES (1, strftime('%s', '2017-01-10T10:00:00'), strftime('%s', '2017-01-10T11:00:00'));",
                     "INSERT INTO interval (activity_id, start, end) "
                     "VALUES (1, strftime('%s', '2017-01-10T15:00:00'), strftime('%s', '2017-01-10T16:00:00'));",
                     "INSERT INTO interval (activity_id, start, end) "
                     "VALUES (2, strftime('%s', '2017-01-11T09:30:00'), strftime('%s', '2017-01-11T10:00:00'));",
                     "INSERT INTO interval (activity_id, start) "
                     "VALUES (3, strftime('%s', '2017-01-13T13:00:00'));"
                 });

        for(int i = 1; i <= 10; ++i){
            QString s = QString::fromStdString(to_string(i));
            tagDbBackend()->updateName(tagDbBackend()->create(), s + s + s);
        }

        activityDbBackend()->updateTags(activity1Id, {1});
        activityDbBackend()->updateTags(activity2Id, {1,2});

        tagDbBackend()->updateTags(1, {3});
        tagDbBackend()->updateTags(2, {1,3});

        analitics_ = make_shared<QAnalyticsContext>(tags(), dbBackend());
    }

    shared_ptr<QAnalyticsContext> analitics() const
    {
        return analitics_;
    }

    std::shared_ptr<MyTime::Database::Backend::Editor::Activity> activityDbBackend() const
    {
        return activityDbBackend_;
    }

    void checkDate(const int &row, const QString &date)
    {
        EXPECT_EQ(analitics()->get_chartModel()->data(analitics()->get_chartModel()->index(row, 0)).toDate(),
                  QDate::fromString(date, DATE_FORMAT))
                << "Date should be " << date.toStdString();
    }

    void checkDuration(const int &row, const double &duration)
    {
        EXPECT_EQ(analitics()->get_chartModel()->data(
                      analitics()->get_chartModel()->index(row, 1)).toDouble(),
                  duration)
                << "Duration should be " << duration << " hours";
    }

    const QString DATE_FORMAT {"yyyy-MM-dd"};

private:
    shared_ptr<QAnalyticsContext> analitics_;
    shared_ptr<Database::Backend::Editor::Activity> activityDbBackend_;
};

typedef QAnalyticsContextBasicTest QAnalyticsContextConstructorTest;

TEST_F(QAnalyticsContextConstructorTest, test) {
    EXPECT_THROW(QAnalyticsContext m(nullptr,   nullptr),       invalid_argument);
    EXPECT_THROW(QAnalyticsContext m(nullptr,   dbBackend()),   invalid_argument);
    EXPECT_THROW(QAnalyticsContext m(tags(),    nullptr),       invalid_argument);

    EXPECT_NO_THROW(QAnalyticsContext m(tags(), dbBackend()));
}

TEST_F(QAnalyticsContextMethodsTest, Properties) {
    analitics()->update("", {},
                        QDateTime::fromString("2017-01-10", DATE_FORMAT),
                        QDateTime::fromString("2017-01-12", DATE_FORMAT));
    EXPECT_EQ(analitics()->get_average().toStdString(), "02:30:00");
    EXPECT_EQ(analitics()->get_averageFilter().toStdString(), "01:15:00");

    EXPECT_TRUE(analitics()->get_chartModel());
    EXPECT_EQ(typeid(*analitics()->get_chartModel()), typeid(QDurationChartModel));

    EXPECT_EQ(analitics()->get_durationSum().toStdString(), "02:30:00");

    QDateTime expectedEnd{QDateTime::fromString("2017-01-11T10:00:00", Qt::ISODate)};
    expectedEnd.setTimeSpec(Qt::UTC);
    EXPECT_EQ(analitics()->get_end().toUTC(), expectedEnd)
            << "End = " << analitics()->get_end().toUTC().toString().toStdString()
            << endl
            << "Expected End: " << expectedEnd.toString().toStdString();

    EXPECT_EQ(analitics()->get_filterInterval().toStdString(), "48:00:00");

    EXPECT_TRUE(analitics()->get_newTagSelectorModel());
    EXPECT_EQ(typeid(*analitics()->get_newTagSelectorModel()), typeid(QTagNameFilterProxyModel));

    QDateTime expectedStart{QDateTime::fromString("2017-01-10T10:00:00", Qt::ISODate)};
    expectedStart.setTimeSpec(Qt::UTC);
    EXPECT_EQ(analitics()->get_start().toUTC(), expectedStart)
            << "Start = " << analitics()->get_start().toUTC().toString().toStdString()
            << endl
            << "Expected Start: " << expectedStart.toString().toStdString();

    EXPECT_EQ(analitics()->get_startEndInterval().toStdString(), "24:00:00");

    EXPECT_TRUE(analitics()->get_tagSelectorModel());
    EXPECT_EQ(typeid(*analitics()->get_tagSelectorModel()), typeid(QTagIdFilterProxyModel));
}

TEST_F(QAnalyticsContextMethodsTest, update) {
    QDateTime from{QDateTime::fromString("2017-01-01T01:00:00", Qt::ISODate)};
    from.setTimeSpec(Qt::UTC);
    QDateTime to{QDateTime::currentDateTimeUtc().addDays(1)};

    analitics()->update("", {}, from, to);
    EXPECT_EQ(analitics()->get_chartModel()->rowCount(), 4);
    checkDate(0, "2017-01-10");
    checkDuration(0, 2);
    checkDate(1, "2017-01-11");
    checkDuration(1, 0.5);
    checkDate(2, "2017-01-12");
    checkDuration(2, 0);
    checkDate(3, "2017-01-13");
    EXPECT_GT(analitics()->get_chartModel()->data(analitics()->get_chartModel()->index(3, 1)).toDouble(), 0)
            << "Duration should be > 0";

    analitics()->update("1", {}, from, to);
    EXPECT_EQ(analitics()->get_chartModel()->rowCount(), 1);
    checkDate(0, "2017-01-10");
    checkDuration(0, 2);

    analitics()->update("", {1}, from, to);
    EXPECT_EQ(analitics()->get_chartModel()->rowCount(), 2);
    checkDate(0, "2017-01-10");
    checkDuration(0, 2);
    checkDate(1, "2017-01-11");
    checkDuration(1, 0.5);

    analitics()->update("", {3}, from, to);
    EXPECT_EQ(analitics()->get_chartModel()->rowCount(), 2);
    checkDate(0, "2017-01-10");
    checkDuration(0, 2);
    checkDate(1, "2017-01-11");
    checkDuration(1, 0.5);

    analitics()->update("", {},
                        QDateTime::fromString("2017-01-11", DATE_FORMAT),
                        QDateTime::fromString("2017-01-12", DATE_FORMAT));
    EXPECT_EQ(analitics()->get_chartModel()->rowCount(), 1);
    checkDate(0, "2017-01-11");
    checkDuration(0, 0.5);
}

}}
