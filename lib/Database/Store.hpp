#ifndef GUARD_48E37BFB63514D048A496B4697A6BD17
#define GUARD_48E37BFB63514D048A496B4697A6BD17

#include <memory>

#include <QDateTime>
#include <QString>
#include <QUrl>

class QSettings;

namespace MyTime { namespace Database {

class SqlExecutor;
struct StoreImpl;

class Store
{
public:
    explicit Store(const std::shared_ptr<SqlExecutor> &sqlExecutor);
    Store(const std::shared_ptr<SqlExecutor> &sqlExecutor,
          const std::shared_ptr<QSettings> &settings,
          const std::string &tempDirPath);
    Store(const std::shared_ptr<SqlExecutor> &sqlExecutor,
              const std::shared_ptr<QSettings> &settings,
              const std::string &tempDirPath,
              const int &backupTimerInterval);
    virtual ~Store();

    void    newDb(const QUrl &fileUrl);
    void    open();
    void    open(const QUrl &fileUrl);
    QString path();
    void    saveAs(const QUrl &fileUrl);

private:
    std::unique_ptr<StoreImpl> impl_;
};

}}

#endif // GUARD_48E37BFB63514D048A496B4697A6BD17
